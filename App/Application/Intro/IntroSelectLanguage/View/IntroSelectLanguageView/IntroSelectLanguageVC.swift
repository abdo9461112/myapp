//
//  IntroSelectLanguageView.swift
//  App
//
//  Created by MGAbouarab on 26/12/2023.
//

import UIKit

class IntroSelectLanguageVC: BaseViewController {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var collectionView: UICollectionView!
    
    //MARK: - Properties -
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    var selectedLang = "ar"
    var languages: [IntroSelectLanguage] = [
        IntroSelectLanguage(
            displayedName: "اللغة العربية",
            localeSymbol: Language.Languages.ar,
            isSelected: false)
        ,
        IntroSelectLanguage(
            displayedName: "English",
            localeSymbol: Language.Languages.en,
            isSelected: false)
    ]
    
    private let padding: CGFloat = 10
    private let spacing: CGFloat = 10 //Xib

    
    //MARK: - Initializer -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        view.backgroundColor = .systemBackground
        setLeading(title: "Language Settings".localized)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        
        if Language.currentLanguage() == "ar" {
            
            self.languages[0].isSelected = true
            self.selectedLang = "ar"
        }else{
            self.languages[1].isSelected = true
            self.selectedLang = "en"
        }
        self.collectionView.reloadData()
    }
    
    //MARK: - IBAction -
    @IBAction private func confirmButtonPressed() {
        Language.setAppLanguage(lang: self.selectedLang)
        callApiData()
        Language.handleViewDirection()

    }
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<BaseResponse<ResponseData>>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress).data
    }
}

//MARK: - CollectionView -

extension IntroSelectLanguageVC {
    func setupCollectionView() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(cellType: IntroSelectLanguageCollectionViewCell.self)
        self.collectionView.contentInset = .init(top: 0, left: padding, bottom: 0, right: padding)
    }
}

extension IntroSelectLanguageVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.languages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: IntroSelectLanguageCollectionViewCell.self, for: indexPath)
        cell.set(languages[indexPath.item])
        return cell
    }
}

extension IntroSelectLanguageVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let selectedIndex = self.languages.firstIndex(where: { $0.isSelected }){
            self.languages[selectedIndex].isSelected = false
            collectionView.reloadItems(at: [IndexPath(item: selectedIndex, section: 0)])
            self.languages[indexPath.row].isSelected = true
            self.selectedLang = self.languages[indexPath.row].localeSymbol
            
            collectionView.reloadItems(at: [indexPath])
        }
    }
}

extension IntroSelectLanguageVC: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width - 2 * padding
        let height = CGFloat(50) // Set the height of each item here
        return CGSize(width: width, height: height)
    }
}

//MARK: -  NewWroking
extension IntroSelectLanguageVC{
    private func callApiData() {
        Task {
            let endpoint = SettingsEndpoints.intro()
            do {
                guard let response = try await self.request(endpoint) else {return}
                let items = response
                let vc = OnBoardingPageController(items: items)
                AppHelper.changeWindowRoot(vc: vc)
                UserDefaults.isFirstTime = true
            } catch {
                print(error.localizedDescription)
            }
        }
    }

}
