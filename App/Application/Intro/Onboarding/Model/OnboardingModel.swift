//
//  OnboardingModel.swift
//  App
//
//  Created by MGAbouarab on 29/12/2023.
//

import Foundation

struct OnboardingData: Decodable {
    let intros: [OnboardingModel]
}

struct OnboardingModel: Decodable {
    let image: String
    let title: String
    let description: String
    
    enum CodingKeys: String, CodingKey {
        case image
        case title
        case description = "desc"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.image = try container.decodeIfPresent(String.self, forKey: .image) ?? ""
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        self.description = try container.decodeIfPresent(String.self, forKey: .description) ?? ""
    }
}
