//
//  OnBoardingPageView.swift
//  App
//
//  Created by MGAbouarab on 28/12/2023.
//

import UIKit

class OnBoardingPageView: UIView {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var pageControl: UIPageControl!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var descriptionTextView: UITextView!
    @IBOutlet weak private var nextButton: UIButton!
    @IBOutlet weak private var skipButton: UIButton!
    
    //MARK: - Properties -
    var onNext: (()->())?
    var onSkip: (()->())?
    
    //MARK: - Initializer -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "OnBoardingPageView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.skipButton.setTitle("Skip_intro_button".onboardingLocalized, for: .normal)
        self.nextButton.setTitle("Next_intro_button".onboardingLocalized, for: .normal)
    }
    
    //MARK: - Data -
    func set(title: String, description: String, pageIndex: Int) {
        self.titleLabel.text = title
        self.descriptionTextView.text = description
        self.pageControl.currentPage = pageIndex
    }
    func setSkipButton(alpha: CGFloat) {
        UIView.animate(withDuration: 0.3) {
            self.skipButton.alpha = alpha
        }
    }
    func setPages(count: Int) {
        self.pageControl.numberOfPages = count
    }
    func setNextButton(title: String) {
        self.nextButton.setTitle(title, for: .normal)
    }
    
    
    //MARK: - IBAction -
    @IBAction private func nextButtonPressed() {
        self.onNext?()
    }
    @IBAction private func skipButtonPressed() {
        self.onSkip?()
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}
