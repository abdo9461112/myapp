//
//  OnboardingViewController.swift
//  App
//
//  Created by MGAbouarab on 29/12/2023.
//

import UIKit

class OnboardingViewController: UIViewController {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var imageView: UIImageView!
    
    //MARK: - Properties -
    let image: String
    
    //MARK: - Initializer -
    init(image: String) {
        self.image = image
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        self.image = ""
        super.init(coder: coder)
    }
    
    //MARK: - Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.setWith(image)
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
}
