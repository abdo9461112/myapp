//
//  Onboarding+Localized.swift
//  App
//
//  Created by MGAbouarab on 30/12/2023.
//

import Foundation

extension String {
    var onboardingLocalized: String {
        return NSLocalizedString(self, tableName: "OnboardingLocalized", bundle: Bundle.main, value: "", comment: "")
    }
}
