//
//  HomeSearchCell.swift
//  App
//
//  Created by Abdo Emad on 31/05/2024.
//

import UIKit
import Cosmos

class ProviderCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var stateLable: UILabel!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var cosmosView: CosmosView!
    @IBOutlet weak var lableView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupDesign()
    }
    //MARK: - setUp
    
    private func setupDesign(){
        containerView.addCornerRadius(radious: 10)
        cosmosView.isUserInteractionEnabled = false 
        self.clipsToBounds = true
    }
    
    //MARK: - DAta
    func setupData(_ data: ProviderModel){
        imageView.setWith(data.image)
        titleLable.text = data.name
        cosmosView.rating = data.rate ?? 0.0
        if data.isAvaliable == true{
            stateLable.text = "available".localized
            stateLable.textColor = .white
            lableView.backgroundColor = .green
        }else{
            stateLable.text = "not available".localized
            stateLable.textColor = .white
            lableView.backgroundColor = .newRed
        }
    }

}
