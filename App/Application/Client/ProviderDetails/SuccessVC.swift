//
//  SuccessAddToCartVC.swift
//  App
//
//  Created by Abdo Emad on 11/06/2024.
//

import UIKit
import SnapKit

protocol SuccessProtocol: AnyObject{
    func popOut()
}

class SuccessVC: BaseViewController {
    
    private let halfView : UIView = {
        let view = UIView()
        view.backgroundColor = .systemBackground
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let image: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(systemName: "checkmark.seal.fill")
        image.contentMode = .scaleAspectFit
        image.tintColor = .main
        return image
    }()
    
    var titleLable : UILabel = {
        let lable = UILabel()
        lable.text = ""
        lable.textAlignment = .center
        lable.font = UIFont.boldSystemFont(ofSize: 18)
        lable.textColor = .label
        lable.numberOfLines = 0
        return lable
    }()
    
    var subtitleLable : UILabel = {
        let lable = UILabel()
        lable.text = ""
        lable.textAlignment = .center
        lable.font = UIFont.systemFont(ofSize: 15)
        lable.textColor = .label
        lable.numberOfLines = 0
        return lable
    }()
    
    
     var button : UIButton = {
        let button = UIButton()
        button.setTitle("Back".localized, for: .normal)
        button.tintColor = .white
        button.backgroundColor = .main
        button.addCornerRadius(radious: 10)
        return button
    }()
    
    private lazy var vStackView : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            vStack,
            button
        ])
        stack.axis = .vertical
        stack.spacing = 45
        stack.alignment = .fill
        stack.distribution = .fill
        return stack
    }()
    
    private lazy var vStack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            image,
            vStackLabels
        ])
        stack.axis = .vertical
        stack.spacing = 20
        stack.alignment = .fill
        stack.distribution = .fill
        return stack
    }()
    private lazy var vStackLabels : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            titleLable,
            subtitleLable
        ])
        stack.axis = .vertical
        stack.spacing = 5
        stack.alignment = .fill
        stack.distribution = .fill
        return stack
    }()
    
   weak var delegate : SuccessProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layout()
        handleTouches()
    }
    
    static func create(delegate:SuccessProtocol, title: String, subtitle: String = "", titleButton: String = "Back".localized,isModalInPresentation:Bool = false) -> SuccessVC{
        let vc = SuccessVC()
        vc.delegate = delegate
        vc.titleLable.text = title
        vc.subtitleLable.text = subtitle
        vc.button.setTitle(titleButton, for: .normal)
        if let sheet = vc.sheetPresentationController{
            if #available(iOS 16.0, *) {
                let customDetent = UISheetPresentationController.Detent.custom { context in
                    return 350
                }
                sheet.detents = [customDetent]
                sheet.preferredCornerRadius = 25
                vc.isModalInPresentation = isModalInPresentation
            }
        }
        return vc
    
    }
    
    private func layout(){
        view.addSubview(halfView)
        view.backgroundColor = .systemBackground
        halfView.addSubview(vStackView)
        halfView.snp.makeConstraints { make in
            make.top.equalTo(view.snp.top).offset(16)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.greaterThanOrEqualTo(view.snp.bottom).offset(-350)
        }
        vStackView.snp.makeConstraints { make in
            make.top.equalTo(halfView.snp.top)
            make.leading.equalTo(halfView.snp.leading).offset(16)
            make.trailing.equalTo(halfView.snp.trailing).offset(-16)
            make.bottom.equalTo(halfView.snp.bottom).offset(-20)
        }
        image.snp.makeConstraints { make in
            make.height.equalTo(150)
        }
        button.snp.makeConstraints { make in
            make.height.equalTo(50)
        }
    }
}


extension SuccessVC {
    
    private func handleTouches(){
        button.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
    }
    
    @objc func backButtonTapped(_ sender: UIButton) {
        dismiss(animated: true) {[weak self] in
            self?.delegate?.popOut()
        }
    }
}

