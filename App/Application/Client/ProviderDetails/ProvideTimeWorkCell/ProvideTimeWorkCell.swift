//
//  ProvideTimeWorkCell.swift
//  App
//
//  Created by Abdo Emad on 03/06/2024.
//

import UIKit

class ProvideTimeWorkCell: UITableViewCell {

    
    @IBOutlet weak var dayLable: UILabel!
    @IBOutlet weak var fromTimeLable: UILabel!
    @IBOutlet weak var toTimeLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    func setdata(data: ProviderTimeWork){
        dayLable.text = data.day
        fromTimeLable.text = data.from
        toTimeLable.text = data.to
    }
    
}
