//
//  ProductsVC.swift
//  App
//
//  Created by Abdo Emad on 10/06/2024.
//

import UIKit


import UIKit
import SnapKit

class ProductsVC: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchField: UITextField!
    var providerId: Int?
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var searchWorkItem: DispatchWorkItem?

    private var productsModel : [ProviderProductsDataModel] = []{
        didSet{
            collectionView.reloadData()
            if productsModel.isEmpty{
                emptyMessage.text = "No Products".localized
                emptyMessage.isHidden = false
            }else{
                emptyMessage.isHidden = true
            }
            
        }
    }

    private let emptyMessage : UILabel = {
        let lable = UILabel()
        lable.textColor = .newRed
        lable.font = UIFont.systemFont(ofSize: 25, weight: .semibold)
        return lable
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

   
    private func setup(){
        addBackButtonWith(title: "Products".localized,color: .white)
        collectionView.delegate = self
        collectionView.dataSource = self
        searchField.delegate = self
        collectionView.register(cellType: ProductCell.self)
        getProviderProducts(id: providerId ?? 0, filter: nil)
        view.addSubview(emptyMessage)
        emptyMessage.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
}


//MARK: - collectionView
extension ProductsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productsModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: ProductCell.self, for: indexPath)
        cell.setData(data: productsModel[indexPath.row])
        cell.addToFav = { [weak self] in
            guard let self = self else {return}
            guard let id = productsModel[indexPath.row].id else {return}
            self.addToFavourite(productId: id)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let id = productsModel[indexPath.row].id
        let vc = ProductDetailsVC()
        vc.productId = id
        push(vc)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.width - 24) / 2, height: 200)
    }
    
    
}


//MARK: -  NetWorking


extension ProductsVC{
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    
    private func callApi(){
        DispatchQueue.main.async {
            self.getProviderProducts(id: self.providerId ?? 0, filter: nil)
        }
    }
    
    private func getProviderProducts(id: Int, filter: String?){
        showIndicator()
        Task{
            let endpoint = ProductEndPoints.getProviderProducts(id: id, filter: filter)
            do{
                guard let response = try await request(endpoint) else {return}
                productsModel = response.data?.data ?? [ ]
                hideIndicator()
            }catch{
                print("error calling products api : \(error.localizedDescription)")
                
            }
        }
    }
    
    private func addToFavourite(productId:Int){
        showIndicator()
        Task{
            let endpoint = ProductEndPoints.addProductToFav(productId: productId)
            do{
                guard let _ = try await request(endpoint)else{return}
                hideIndicator()
            }catch{
                print("error adding products api : \(error.localizedDescription)")

            }
        }
    }
    
}



//MARK: -  Textfield

extension ProductsVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        let newText = (currentText as NSString).replacingCharacters(in: range, with: string)
        searchWorkItem?.cancel()
        searchWorkItem = DispatchWorkItem { [weak self] in
            self?.performSearch(text: newText)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: searchWorkItem!)
        
        return true
    }
    
   private func performSearch(text: String) {
        if !text.isEmpty {
            let matchedProduct = productsModel.contains { $0.nameEn!.lowercased().contains(text.lowercased())
            }
            if !matchedProduct {
                productsModel.removeAll()
                collectionView.reloadData()
                emptyMessage.text = "No Products".localized
            }
        }
        getProviderProducts(id: providerId ?? 0, filter: text)
    }
  
    
}
