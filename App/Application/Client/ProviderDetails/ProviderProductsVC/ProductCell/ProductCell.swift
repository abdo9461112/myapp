//
//  ProductCell.swift
//  App
//
//  Created by Abdo Emad on 04/06/2024.
//

import UIKit

class ProductCell: UICollectionViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var favButton: UIButton!
    var addToFav: (() -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        favButton.setImage(UIImage(resource: .fav), for: .selected)
        favButton.setImage(UIImage(resource: .unFav), for: .normal)
    }

    
    func setData(data: ProviderProductsDataModel){
        if Language.isRTL(){
            productName.text = data.nameAr
        }else{
            productName.text = data.nameEn
        }
        productPrice.text = "\(data.price ?? 0.0) " + "SAR".localized
        productImage.setWith(data.attachments?.first?.path)
        favButton.isSelected = data.isFavorite ?? false
    }

    @IBAction func favButtonTaped(_ sender: UIButton) {
        sender.isSelected.toggle()
        addToFav?()
        
    }
}
