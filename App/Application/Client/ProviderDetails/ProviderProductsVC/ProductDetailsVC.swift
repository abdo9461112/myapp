//
//  ProductDetailsVC.swift
//  App
//
//  Created by Abdo Emad on 09/06/2024.
//

import UIKit
import ImageSlideshow



class ProductDetailsVC: BaseViewController{
    
    @IBOutlet weak var productImage: ImageSlideshow!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productDescription: UITextView!
    @IBOutlet weak var ProductQuantity: UILabel!
    @IBOutlet weak var ProducttotalPrice: UILabel!
    
    //MARK: - Properties -
    var productId: Int?
    private var sliders: [Attachment] = []
    private var quantity: Int = 1
    private var itemPrice: Double = 1.0
    private var calculatedPrice: Double = 1.0
    private var isAvailable: Bool = false
    private var maxPurchases: Int = 1
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButtonWith(title: "Details".localized, color: .white)
        confgImageSlider()
        getProduct(id: productId!)
    }

    private func confgImageSlider(){
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = .main
        pageControl.pageIndicatorTintColor = .lightGray
        productImage.pageIndicator = pageControl
        productImage.slideshowInterval = 3
        productImage.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        productImage.contentScaleMode = .scaleAspectFill
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        productImage.addGestureRecognizer(recognizer)
    }

    @objc func didTap() {
        productImage.presentFullScreenController(from: self)
    }
    
    private func configureSlider(_ models: [Attachment]) {
        let kingfisherSources: [KingfisherSource] = models.compactMap { attachment in
            guard let path = attachment.path, !path.isEmpty,
                  let encodedURLString = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                  let url = URL(string: encodedURLString) else {
                return nil
            }
            return KingfisherSource(url: url, placeholder: UIImage(named: "placeholder"))
        }

        self.productImage.setImageInputs(kingfisherSources)
    }
    
    
    @IBAction func increaseButtonTapped(_ sender: UIButton) {
        checkQuantity(id: productId!)
    }
    @IBAction func dcreaseButtonTapped(_ sender: UIButton) {
        if quantity > 1 {
            self.quantity -= 1
            self.calculatedPrice = calculatedPrice - itemPrice
            self.ProducttotalPrice.text = "\(calculatedPrice)"
            self.ProductQuantity.text = "\(self.quantity)"
        }
    }
    
    @IBAction func AddToCartButtonTapped(_ sender: UIButton) {
    addProductToCart(productId: productId!, quantity: quantity)
    }
    
}
//MARK: - Networking

extension ProductDetailsVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getProduct(id:Int){
        showIndicator()
        Task{
            let endpoint = ProductEndPoints.clientProduct(id: id)
            do{
                guard let response = try await request(endpoint) else{return}
                guard let slider = response.data?.attachments else{return}
                configureSlider(slider)
                productName.text = response.data?.name
                productPrice.text = "\(response.data?.price ?? 0) \("SAR".localized)"
                productDescription.text = response.data?.description
                ProductQuantity.text = "1"
                ProducttotalPrice.text = "\(response.data?.price ?? 0) " + " " + "\("SAR".localized)"
                itemPrice = response.data?.price ?? 0
                maxPurchases = response.data?.maxPurchases ?? 1
                isAvailable = response.data?.isAvaliable ?? false
                hideIndicator()
            }catch{
                print("error getting product : \(error.localizedDescription)")
            }
        }
    }
    private func checkQuantity(id:Int){
        Task{
            let endpoint = CartEndpoints.clientCartCheckQuantity(id: id)
            do{
                guard let _ = try await request(endpoint)else{return}
                if quantity == maxPurchases{
                    show(errorMessage: "You have exceeded your maximum purchase limit".localized)
                }else if quantity < maxPurchases{
                    quantity += 1
                    calculatedPrice = itemPrice * Double(quantity)
                    ProductQuantity.text = "\(quantity)"
                    ProducttotalPrice.text = "\(calculatedPrice)"
                }
                
            }catch{
                print("error checking on quantity : \(error.localizedDescription)")
            }
        }
    }
    private func addProductToCart(productId: Int, quantity: Int){
        Task{
            showIndicator()
            let endpoint = CartEndpoints.addProdcutToCart(id: productId, quantity: quantity)
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                if response.key == .success{
                    presentSuccessVC()
                }else{
                    show(errorMessage: response.message)
                }
            }catch{
                print("error adding product to cart : \(error.localizedDescription)")
            }
        }
    }
}


//MARK: - pop backe to product view

extension ProductDetailsVC: SuccessProtocol{
    func popOut() {
        pop()
    }
    private func presentSuccessVC(){
        let vc = SuccessVC.create(delegate: self, title: "The product has been successfully added to the cart".localized, isModalInPresentation: true)
        present(vc, animated: true)
    }
}



