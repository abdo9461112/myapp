//
//  ProviderDetailsVC.swift
//  App
//
//  Created by Abdo Emad on 03/06/2024.
//

import UIKit
import Cosmos

class ProviderDetailsVC: BaseViewController {
    
    
    @IBOutlet weak var providerImageView: UIImageView!
    @IBOutlet weak var providerName: UILabel!
    
    @IBOutlet weak var providerState: UILabel!
    @IBOutlet weak var providerStateView: UIView!
    @IBOutlet weak var providerRate: CosmosView!
    @IBOutlet weak var providerRateValue: UILabel!
    @IBOutlet weak var providerDescription: UITextView!
    
    @IBOutlet weak var productsView: UIView!
    @IBOutlet weak var servicesView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var arrowImage: UIImageView!
    
    @IBOutlet weak var wokingTimeButton: UIButton!
    
    var providerId: Int?
    private let responseHandler: ResponseHandler = DefaultResponseHandler()
    private var timeWorkModel : [ProviderTimeWork] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getProvider(id: providerId ?? 0)
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as? ColoredNav)?.changeApperance(to: .colored)
    }
    

    private func setup(){
        addBackButtonWith(title: "Description".localized,color: .white)
        providerRate.settings.updateOnTouch = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isHidden = true
        tableView.register(cellType: ProvideTimeWorkCell.self)
        productsView.addTapGesture {
            self.goToProductsVC()
        }
        
        servicesView.addTapGesture {
            self.goToServicesVC()
        }
        
    }

    
   private  func goToProductsVC(){
       if !UserDefaults.isLogin{
           showRequiredLoginAlert()
       }else{
           let vc = ProductsVC()
           vc.providerId = providerId
           push(vc)

       }
    }
    
   private func goToServicesVC(){
       if !UserDefaults.isLogin{
           showRequiredLoginAlert()
       }else{
           let vc = ServicesVC()
           vc.providerId = providerId
           push(vc)
       }
    }
    
    @IBAction func weeklyWorkingHoursBttTaped(_ sender: UIButton) {
        sender.isSelected.toggle()
        if wokingTimeButton.isSelected {
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let self = self else {return}
                self.tableView.isHidden = false
                self.arrowImage.image = UIImage(resource: .moveArrowUp)
            }
        }else {
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let self = self else {return}
                self.tableView.isHidden = true
                if Language.isRTL() {
                    self.arrowImage.image = UIImage(resource: .moveArrowAr)
                }else {
                    self.arrowImage.image = UIImage(resource: .moveArrowEn)
                }
            }
        }

        
    }
    
}

//MARK: - TapleView

extension ProviderDetailsVC: UITableViewDataSource, UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if timeWorkModel.isEmpty {
            return 0
        }else{
            return timeWorkModel.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: ProvideTimeWorkCell.self, for: indexPath)
        cell.selectionStyle = .none
        cell.setdata(data: timeWorkModel[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
}




//MARK: - Networking

extension ProviderDetailsVC{
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getProvider(id:Int){
        showIndicator()
        Task{
            let endpoint = HomeEndPoints.getProvider(id: id)
            do{
                guard let response = try await request(endpoint) else {return}
                if let data = response.data{
                    providerName.text = data.name
                    providerImageView.setWith(data.image)
                    providerRateValue.text = "(\(data.rate?.value ?? 0.0))"
                    providerRate.rating = Double(data.rate?.value ?? 0.0)
                    providerDescription.text = data.description
                    timeWorkModel = data.timeWorks ?? []
                    tableView.reloadData()
                    if data.isAvaliable ?? true {
                        providerState.text = "available".localized
                        providerStateView.backgroundColor = .green
                    }else{
                        providerState.text = "not available".localized
                        providerStateView.backgroundColor = .newRed
                    }
                }
                hideIndicator()
            }catch{
                
                print("error calling provider api \(error.localizedDescription)")
                
            }
        }
    }
}



