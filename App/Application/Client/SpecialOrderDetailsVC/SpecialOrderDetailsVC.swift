//
//  SpecialOrderDetailsVC.swift
//  App
//
//  Created by Abdo Emad on 14/06/2024.
//

import UIKit

class SpecialOrderDetailsVC: BaseViewController {
    
    
    @IBOutlet weak var stepperView: StepperView!
    @IBOutlet weak var orderName: UILabel!
    @IBOutlet weak var catgory: UILabel!
    @IBOutlet weak var deliveryData: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var descriptoin: UILabel!
    @IBOutlet weak var waitingLable: UILabel!
    @IBOutlet weak var QuotationsButton: UIButton!
    
    @IBOutlet weak var offerView: UIView!
    @IBOutlet weak var offerPrice: UILabel!
    @IBOutlet weak var offerNotes: UILabel!
    
    @IBOutlet weak var providerImage: UIImageView!
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var providerNumber: UILabel!
    @IBOutlet weak var providerView: UIView!
    
    @IBOutlet weak var delegateView: UIView!
    @IBOutlet weak var delegateImageView: UIImageView!
    @IBOutlet weak var delegateName: UILabel!
    @IBOutlet weak var delegateNumber: UILabel!
    
    @IBOutlet weak var chatButton: UIButton!
    
    @IBOutlet weak var receivedFromProviderBt: UIButton!
    
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var orderId: Int?
    private var inHouseStepperSteps: [String] = [
        "In progress".localized,
        "Delegate on the way to you".localized,
        "Delivered".localized,
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        configureStepperView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        (self.navigationController as? ColoredNav)?.changeApperance(to: .colored)
        getSpecialOrderDetails(orderId: orderId!)
    }
    
    private func setUp(){
        addBackButtonWith(title: "Details".localized, color: .white)
        QuotationsButton.isHidden = true
        offerView.isHidden = true
        chatButton.isHidden = true
        providerView.isHidden = true
        delegateView.isHidden = true
        waitingLable.isHidden = true
        receivedFromProviderBt.isHidden = true
        
    }
    private func configureStepperView() {
        stepperView.setStepperLinesColor(activeColor: .main, notActiceColor: .lightGray)
        stepperView.setStepperLabelsColor(activeColor: .black, notActiceColor: .black)
        stepperView.setStepperViewImages(nextStateImage: UIImage(named: "unStageStepper"), currentStateImage: UIImage(named: "stageStepper"), finishedStateImage: UIImage(named: "stageStepper"))
        stepperView.setStepperTitlesForItems(titles: inHouseStepperSteps)
    }
    
    static func create(orderId:Int) ->SpecialOrderDetailsVC{
        let vc = SpecialOrderDetailsVC()
        vc.orderId = orderId
        return vc
    }
    
    private func setData(_ data : OrderDetailsModel?){
        guard let data = data else { return }
        
        orderName.text = data.name
        //        catgory.text = data?.name
        price.text = "\(data.total) " + "SAR".localized
        deliveryData.text = data.receivingTime
        address.text = data.userRecevingAddress
        descriptoin.text = data.notes
        providerName.text = data.provider.name
        providerImage.setWith(data.provider.image)
        providerNumber.text = data.provider.phone
        offerNotes.text = data.notes
        offerPrice.text = String(data.offerPriceInfo?.price ?? 0)
        delegateName.text = data.delegate.name
        delegateNumber.text = data.delegate.phone
        delegateImageView.setWith(data.delegate.image)
       
        
        if data.status.client == .wattingAccept {
            if data.hasOffer == true {
                handleWaitingForAcceptanceStatus(hasOffer: true)
            }else{
                handleWaitingForAcceptanceStatus(hasOffer: false)
            }
        }else if data.status.client == .receivedFromDelegate {
                handleClientReceived()
        }else if data.status.client == .paid {
            if data.status.provider == .accepted {
                handleAcceptedStatus()
            }else if data.status.provider == .preparing{
                handlePreparedStatus()
            }else if data.status.provider == .delegateHasArrived {
                handleOnTheWayToCient()
            }else if data.status.provider == .rejected{
                handleRejectedStatus()
            }
        }
    }
  
    @IBAction func QuotationsButtonTapped(_ sender: UIButton) {
        let vc = OfferPriceVC.create(orderId: orderId!)
        push(vc)
    }
    
    @IBAction func chatButtonTapped(_ sender: UIButton) {
    }
    
    @IBAction func receivedSuccessfullyButtonTapped(_ sender: UIButton) {
        clientChangeStatus(orderId: orderId ?? 0 )
    }
}

//MARK: - handle Views
extension SpecialOrderDetailsVC{
    private func handleWaitingForAcceptanceStatus(hasOffer: Bool) {
        if hasOffer {
            stepperView.setStepperCurrentIndex(toIndex: 0)
            QuotationsButton.isHidden = false
        } else {
            waitingLable.isHidden = false
            waitingLable.text = "Waiting for price offers".localized
            stepperView.setStepperCurrentIndex(toIndex: 0)
        }
    }
    private func handleAcceptedStatus() {
        offerView.isHidden = false
        providerView.isHidden = false
        waitingLable.isHidden = false
        waitingLable.text = "Wating to be prepared".localized
        stepperView.setStepperCurrentIndex(toIndex:0)
    }
   
    private func handlePreparedStatus() {
        offerView.isHidden = false
        chatButton.isHidden = false
        providerView.isHidden = false
        stepperView.isHidden = false
        stepperView.setStepperCurrentIndex(toIndex: 0)
    }

    private func handleOnTheWayToCient() {
        providerView.isHidden = false
        offerView.isHidden = false
        delegateView.isHidden = false
        stepperView.isHidden = false
        chatButton.isHidden = false
        receivedFromProviderBt.isHidden = false
        stepperView.setStepperCurrentIndex(toIndex: 1)
        }
    private func handleClientReceived() {
        providerView.isHidden = false
        offerView.isHidden = false
        delegateView.isHidden = false
        stepperView.isHidden = false
        waitingLable.isHidden = false
        waitingLable.text = "Order has been received".localized
        stepperView.setStepperCurrentIndex(toIndex: 2)
    }
    private func handleRejectedStatus() {
        providerView.isHidden = false
        offerView.isHidden = false
        waitingLable.isHidden = false
        stepperView.isHidden = false
        waitingLable.text = "This order has been rejected".localized
        stepperView.setStepperCurrentIndex(toIndex: 2)
    }
          
}



//MARK: - Netwoking

extension SpecialOrderDetailsVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getSpecialOrderDetails(orderId:Int){
        Task{
            showIndicator()
            let endpoint = OrdersEndpoint.getOrderDetials(orderId: orderId)
            do{
                guard let response = try await request(endpoint) else{return}
                hideIndicator()
                if let data = response.data{
                    self.setData(data)
                }
            }catch{
                print("error calling order details api : \(error.localizedDescription)")
            }
        }
    }
    private func clientChangeStatus(orderId:Int){
        Task{
            let endpoint = OrdersEndpoint.clientOrderStatusChange(orderId: orderId, status:  "received_from_delegate")
            do{
                guard let _ = try await request(endpoint)else{return}
                getSpecialOrderDetails(orderId: orderId)
                pop(animated: true)
            }catch{
                print("error calling order Client Change Status api : \(error.localizedDescription)")
            }
        }
    }
}

