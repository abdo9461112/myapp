//
//  ReceivingMethodMoodel.swift
//  App
//
//  Created by Abdo Emad on 12/06/2024.
//

import Foundation

struct ReceivingMethodModel: Codable {
    var key: String?
    var name: String?
}
