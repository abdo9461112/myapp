//
//  Orders.swift
//  App
//
//  Created by Abdo Emad on 13/06/2024.
//

// MARK: - OrdersModel
struct OrdersModel: Codable {
    var pagination: Paginate?
    var data: [OrdersDataModel]?
}

// MARK: - OrdersDataModel
struct OrdersDataModel: Codable {
    var id: Int?
    var number, type, createdAt: String?
    var provider: Provider?
    var user: User?
    var status: Status?
    var clientStatusText: String?
    
     func createStatus() -> String{
        switch status?.client {
        case .wattingAccept:
            return "watting for Acceptance".localized
        case .receivedFromDelegate:
            return "Received from delegate".localized
        case .prepareForPay:
            return "prepare For Pay".localized
        case .paid:
            return "Paid".localized
        case .delegateHasArrived:
            return "Delegate has arrived".localized
        case .accepted:
            return "Order accepted".localized
        case .preparing:
            return "In progress".localized
        case .onMyWay:
            return "Delegate on the way to you".localized
        case .receivedFromProvider:
            return "Received from provider".localized
        case .receivedToClient:
            return "Received".localized
        case nil:
            return ""
        }
    }

    enum CodingKeys: String, CodingKey {
        case id, number, type
        case createdAt = "created_at"
        case provider, status, user
    }
}

// MARK: - Status
struct Status: Codable {
    var user: OrderStatus?
    var provider: ProviderOrderStatus?
    var delegate: DelegateOrderStatus?
    var client: OrderStatus?
}

enum OrderStatus: String, Codable {
    case wattingAccept = "watting_accept"
    case receivedFromDelegate = "received_from_delegate"
    case prepareForPay = "prepare_for_pay"
    case paid = "paid"
    case delegateHasArrived = "delegate_has_arrived"
    case accepted = "accepted"
    case preparing = "preparing"
    case onMyWay = "on_my_way"
    case receivedFromProvider = "received_from_provider"
    case receivedToClient = "received_to_client"
}


enum ProviderOrderStatus: String, Codable {
    case wattingAccept = "watting_accept"
    case delegateHasArrived = "delegate_has_arrived"
    case accepted = "accepted"
    case preparing = "preparing"
    case onMyWay = "on_my_way"
    case rejected = "rejected"
    case receivedToClient = "received_to_client"
}


enum DelegateOrderStatus: String, Codable {
    case wattingAccept = "watting_accept"
    case receivedFromDelegate = "received_from_delegate"
    case delegateHasArrived = "delegate_has_arrived"
    case accepted = "accepted"
    case onMyWay = "on_my_way"
    case receivedFromProvider = "received_from_provider"
    case receivedToClient = "received_to_client"
}


struct User: Codable {
    var id: Int?
    var name: String?
    var image: String
    var phone: String?
}
