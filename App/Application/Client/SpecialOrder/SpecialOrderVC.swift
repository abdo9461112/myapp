//
//  SpecialOrderVC.swift
//  App
//
//  Created by Abdo Emad on 14/06/2024.
//

import UIKit

class SpecialOrderVC: BaseViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var orderName: UITextField!
    @IBOutlet weak var catgoryField: DropDownTextField!
    @IBOutlet weak var deliveryDataField: UITextField!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var priceField: UITextField!
    
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var categoriesModel : [CategoriesModel] = []
    private var categoryId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        getCategories()
    }
    override func viewWillAppear(_ animated: Bool) {
        (self.navigationController as? ColoredNav)?.changeApperance(to: .colored)
        if UserDefaults.isLogin{
            scrollView.isHidden = false
            bottomView.isHidden = false
        }else{
            scrollView.isHidden = true
            bottomView.isHidden = true
        }
    }
    
    private func setUp(){
        setLeading(title: "Special order".localized, color: .white)
        catgoryField.dropDownDelegate = self
        deliveryDataField.setDatePickerAsInputViewFor(target: self, selector: #selector(dateFieldTaped), mode: .dateAndTime)
    }
    
    
    @objc func dateFieldTaped() {
        if let datePicker = self.deliveryDataField.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.locale = Locale(identifier: "en")
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            self.deliveryDataField.text = dateFormatter.string(from: datePicker.date)
            deliveryDataField.resignFirstResponder()
        }
    }
    @IBAction func sendButtonTapped(_ sender: UIButton) {
        do{
            let name = try ValidationService.validate(orderName: orderName.text)
            let data = try ValidationService.validate(receiveTime: deliveryDataField.text)
            let address = try ValidationService.validate(receiveAddress: addressField.text)
            let price = try  ValidationService.validate(price: priceField.text)
            let notes = try ValidationService.validate(description: descriptionField.text)
            let category = try ValidationService.validate(categoryId: categoryId)
            
            storePrivateOrder(name: name, receivingTime: data, address: address, price: price, notes: notes, categoryId: category)
        }catch{
            
            show(errorMessage: error.localizedDescription)
        }
    }
    
    private func presentSuccessView(){
        let vc = SuccessVC.create(delegate: self,
                                  title: "The order has sent successfully.".localized, 
                                  subtitle: "You can follow your order from the order list you have.".localized)
        present(vc, animated: true)
        
    }
}

//MARK: - Neworking
extension SpecialOrderVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func storePrivateOrder(name: String, receivingTime: String, address: String, price: String, notes: String, categoryId: Int){
        Task{
            let endpoint = OrdersEndpoint.storePrivateOrder(name: name, receivingTime: receivingTime, address: address, price: price, notes: notes, categoryId: categoryId)
            do{
                guard let response = try await request(endpoint)else{return}
                if response.key == .success{
                    presentSuccessView()
                }else{
                    show(errorMessage: response.message)
                }
            }catch{
                print("error trying to send privater order api requset : \(error.localizedDescription)")
            }
        }
    }
    
    private func getCategories(){
        Task{
            let endpoint = HomeEndPoints.getCategories()
            do{
                guard let response = try await request(endpoint)else{return}
                categoriesModel = response.data ?? []
            }catch{
                print("error trying to call catgories api requset : \(error.localizedDescription)")
            }
        }
    }
   
}

//MARK: - Go To Home

extension SpecialOrderVC: SuccessProtocol{
    func popOut() {
        let vc = UserTabBarController()
        AppHelper.changeWindowRoot(vc: vc)
    }
}

//MARK: - DropDown
extension SpecialOrderVC: DropDownTextFieldDelegate{
    func dropDownList(for textField: UITextField) -> [any DropDownItem] {
        return categoriesModel
    }
    
    func didSelect(item: any DropDownItem, for textField: UITextField) {
        catgoryField.text = item.name
        categoryId = item.id
    }
}



