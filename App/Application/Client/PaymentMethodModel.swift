//
//  PaymentMethodModel.swift
//  App
//
//  Created by Abdo Emad on 12/06/2024.
//


// MARK: - PaymentMethodsModel
struct PaymentMethodModel: Codable {
    var id: Int?
    var name: String?
    var image: String?
    var status: Int?
    var createdAt, updatedAt, deletedAt: String?
    var key: String?
    var isSelected: Bool = false
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.key = try container.decodeIfPresent(String.self, forKey: .key)
        self.image = try container.decodeIfPresent(String.self, forKey: .image)
        self.status = try container.decodeIfPresent(Int.self, forKey: .status)
        self.createdAt = try container.decodeIfPresent(String.self, forKey: .createdAt)
        self.updatedAt = try container.decodeIfPresent(String.self, forKey: .updatedAt)
        self.deletedAt = try container.decodeIfPresent(String.self, forKey: .deletedAt)
        self.isSelected = false
    }

    enum CodingKeys: String, CodingKey {
        case id, name, image, status, key
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
    }
}
