//
//  OrdersDetailsModel.swift
//  App
//
//  Created by Abdo Emad on 14/06/2024.
//

// MARK: - OrderDetailsModel
struct OrderDetailsModel: Codable {
    var id: Int
    var number: String
    var name: String
    var createdAt: String
    var user: Provider
    var provider: Provider
    var total: Double
    var taxValue: Double
    var appRate: Double
    var contactPhone: String
    var finalTotal: Double
    var deliveryPrice: Double
    var userRecevingAddress: String
    var receivingTime: String
    var paymentMethod: String
    var isRated: Bool
    var notes: String
    var deliveryMethod: String
    var items: Items
    var status: Status
    var roomID: Int
    var delegate: DelegateData
//    var category: Category?
    var offerPriceInfo: OfferPriceInfo?
    var hasOffer:Bool?

    enum CodingKeys: String, CodingKey {
        case id
        case delegate
        case number
        case name
        case createdAt = "created_at"
        case user
        case isRated = "is_rated"
        case provider
        case total
        case taxValue = "tax_value"
        case appRate = "app_rate"
        case finalTotal = "final_total"
        case deliveryPrice = "delivery_price"
        case userRecevingAddress = "user_receving_address"
        case receivingTime = "receiving_time"
        case paymentMethod = "payment_method"
        case notes
        case deliveryMethod = "delivery_method"
        case items
        case status
//        case category
        case roomID = "room_id"
        case offerPriceInfo = "offer_price_info"
        case hasOffer = "has_offer_prices"
        case contactPhone = "contact_phone"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.number = try container.decodeIfPresent(String.self, forKey: .number) ?? ""
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.createdAt = try container.decodeIfPresent(String.self, forKey: .createdAt) ?? ""
        self.user = try container.decodeIfPresent(Provider.self, forKey: .user) ?? .init()
        self.provider = try container.decodeIfPresent(Provider.self, forKey: .provider) ?? .init()
        self.delegate = try container.decodeIfPresent(DelegateData.self, forKey: .delegate) ?? .init()
        self.total = try container.decodeIfPresent(Double.self, forKey: .total) ?? 0
        self.taxValue = try container.decodeIfPresent(Double.self, forKey: .taxValue) ?? 0
        self.appRate = try container.decodeIfPresent(Double.self, forKey: .appRate) ?? 0
        self.contactPhone = try container.decodeIfPresent(String.self, forKey: .contactPhone) ?? ""
        self.finalTotal = try container.decodeIfPresent(Double.self, forKey: .finalTotal) ?? 0
        self.deliveryPrice = try container.decodeIfPresent(Double.self, forKey: .deliveryPrice) ?? 0
        self.isRated = try container.decodeIfPresent(Bool.self, forKey: .isRated) ?? false
        self.userRecevingAddress = try container.decodeIfPresent(String.self, forKey: .userRecevingAddress) ?? ""
        self.receivingTime = try container.decodeIfPresent(String.self, forKey: .receivingTime) ?? ""
        self.paymentMethod = try container.decodeIfPresent(String.self, forKey: .paymentMethod) ?? ""
        self.notes = try container.decodeIfPresent(String.self, forKey: .notes) ?? ""
        self.deliveryMethod = try container.decodeIfPresent(String.self, forKey: .deliveryMethod) ?? ""
        self.items = try container.decodeIfPresent(Items.self, forKey: .items) ?? .init()
        self.status = try container.decode(Status.self, forKey: .status)
        self.roomID = try container.decodeIfPresent(Int.self, forKey: .roomID) ?? 0
        self.offerPriceInfo = try container.decodeIfPresent(OfferPriceInfo.self, forKey: .offerPriceInfo)
        self.hasOffer = try container.decodeIfPresent(Bool.self, forKey: .hasOffer)
//        self.category = try container.decodeIfPresent(Category.self, forKey: .category)
    }
    
    
}
struct OfferPriceInfo: Codable {
    var id: Int?
    var notes: String?
    var price: Int?
    var status: String?
}



struct DelegateData: Codable {
    var id: Int?
    var image: String?
    var name: String?
    var phone: String?
}
