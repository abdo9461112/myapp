//
//  MoreViewController.swift
//  App
//
//  Created by MGAbouarab on 16/01/2024.
//

import UIKit

class MoreViewController: BaseViewController {

    //MARK: - IBOutlets -
    @IBOutlet weak private var tableView: UITableView!
    
    //MARK: - Properties -
    private lazy var navigationUserInfoView: NavigationUserInfoView = NavigationUserInfoView()
    private var items: [MoreSection] = []
    private lazy var authItems: [MoreSection] = [
        .init(
            items: [
                .init(
                    image: .init(systemName: "person.fill"),
                    name: "Personal_Profile_More_Title".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = ClientProfileVC()
                        vc.hidesBottomBarWhenPushed = true
                        push(vc)
                },
            ]
        ),
            .init(
                items: [
                    .init(
                        image: .init(systemName: "heart.fill"),
                        name: "Favourite".localized) { [weak self] in
                            guard let self = self else {return}
                            let vc = FavoriteVC()
                            vc.hidesBottomBarWhenPushed = true
                            push(vc)
                    },
                ]
            ),
        .init(
            items: [
                .init(
                    image: .init(systemName: "wallet.pass.fill"),
                    name: "Wallet_More_Title".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = WalletVC()
                        vc.hidesBottomBarWhenPushed = true
                        push(vc)
                },
            ]
        ),
        .init(
            items: [
                .init(
                    image: .init(systemName:"message.fill"),
                    name: "Chats".localized) { [weak self] in
                        guard let self = self else {return}
                },
            ]
        ),
        .init(
            items: [
                .init(
                    image: .init(systemName: "questionmark.circle.fill"),
                    name: "About_Us_More_Title".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = About_TermsVC()
                        vc.hidesBottomBarWhenPushed = true
                        vc.type = .about
                        self.push(vc)
                },
            ]
        ),
        .init(
            items: [
                .init(
                    image: .init(systemName: "list.bullet.clipboard.fill"),
                    name: "Terms And Conditions".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = About_TermsVC()
                        vc.hidesBottomBarWhenPushed = true
                        vc.type = .terms
                        self.push(vc)
                },
            ]
        ),
        .init(
            items: [
                .init(
                    image: .init(systemName: "phone.fill"),
                    name: "Contact_Us_More_Title".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = ContactUsVC()
                        vc.hidesBottomBarWhenPushed = true
                        push(vc)
                },
            ]
        ),
        .init(
            items: [
                .init(
                    image: .init(systemName: "questionmark.circle.fill"),
                    name: "Common Questions".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = FAQuestionsVC()
                        vc.hidesBottomBarWhenPushed = true
                        push(vc)
                },
            ]
        ),
        .init(
            items: [
                .init(
                    image: .init(systemName: "gearshape.fill"),
                    name: "General Settings".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = GeneralSettingsVC()
                        vc.hidesBottomBarWhenPushed = true
                        push(vc)
                },
            ]
        ),
        .init(
            items: [
                .init(
                    image: UIImage(resource: .logout),
                    name: "Log Out".localized) { [weak self] in
                        guard let self = self else {return}
                        presentLogOutVC()
                },
            ]
        ),
    ]
    

    private lazy var visitorItems: [MoreSection] = [
        .init(
            items: [
                .init(
                    image: .init(systemName: "translate"),
                    name: "Language".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = ChangeLangVC()
                        vc.hidesBottomBarWhenPushed = true
                        push(vc)
                    },
            ]
        ),
        .init(
            items: [
                .init(
                    image: .init(systemName: "questionmark.circle.fill"),
                    name: "About_Us_More_Title".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = About_TermsVC()
                        vc.hidesBottomBarWhenPushed = true
                        vc.type = .about
                        self.push(vc)
                },
            ]
        ),
        .init(
            items: [
                .init(
                    image: .init(systemName: "list.bullet.clipboard.fill"),
                    name: "Terms and Condtions".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = About_TermsVC()
                        vc.hidesBottomBarWhenPushed = true
                        vc.type = .terms
                        self.push(vc)
                },
            ]
        ),
        .init(
            items: [
                .init(
                    image: .init(systemName: "questionmark.circle.fill"),
                    name: "Frequently asked questions".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = FAQuestionsVC()
                        vc.hidesBottomBarWhenPushed = true
                        push(vc)
                },
            ]
        ),
        .init(
            items: [
                .init(
                    image: UIImage(resource: .logout),
                    name: "Log In".localized) { [weak self] in
                        guard let self = self else {return}
                        presentLogOutVC()
                },
            ]
        ),
       
    ]
        
    
    
    
    //MARK: - Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationDesign()
        self.items = !UserDefaults.isLogin ? visitorItems : authItems
        self.navigationUserInfoView.set(
            status: UserDefaults.isLogin ? .authorized(
                image: UserDefaults.user?.image
            ) : .unauthorized
        )
        
        self.tableView.reloadData()
    }
    
    private func setupNavigationDesign() {
        setLeading(title: "My account".localized,color: .white)
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationItem.leftBarButtonItem = .init(customView: self.navigationUserInfoView)
        tabBarController?.tabBar.isHidden = false
        (self.navigationController as? ColoredNav)?.changeApperance(to: .colored)
        navigationUserInfoView.viewTapped.addTapGesture {
            let vc = ClientProfileVC()
            vc.hidesBottomBarWhenPushed = true
            self.push(vc)
        }
    }
    
}

//MARK: - UITableView -
private extension MoreViewController {
    func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.contentInset = .init(top: 20, left: 0, bottom: 20, right: 0)
    }
}

extension MoreViewController: UITableViewDataSource , UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items[section].items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = self.items[indexPath.section].items[indexPath.row]
        
        let cell = UITableViewCell()
        cell.contentConfiguration = UIListContentConfiguration.defaultSubtitleCell(
            image: item.image,
            title: item.name
        )
        cell.accessoryType = item.accessoryType
        cell.backgroundColor = item.backgroundColor
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.items[indexPath.section].items[indexPath.row].action()
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}

//MARK: - PresentionVC

extension MoreViewController {
    private func presentLogOutVC(){
        let vc = LogOutAndLogInVC()
        if UserDefaults.isLogin{
            vc.lableText = "Do you really want to logout ?".localized
        }else{
            vc.lableText = "Do you really want to login ?".localized
        }
        vc.hidesBottomBarWhenPushed = true
        if let sheet = vc.sheetPresentationController{
            if #available(iOS 16.0, *){
                let customDetent = UISheetPresentationController.Detent.custom { context in
                    return 300
                }
                sheet.detents = [customDetent]
                sheet.preferredCornerRadius = 25
                
            }
        }
        present(vc, animated: true)

    }
    
}

