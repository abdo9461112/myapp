//
//  WalletVC.swift
//  App
//
//  Created by Abdo Emad on 16/06/2024.
//

import UIKit

class WalletVC: BaseViewController {

    @IBOutlet weak var myBalanceLable: UILabel!
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    override func viewDidLoad() {
        super.viewDidLoad()
        getWalletBalance()
        addBackButtonWith(title: "Wallet".localized, color: .white)
    }


    @IBAction func chagreWalletButtonTapped(_ sender: UIButton) {
    }
    
}

extension WalletVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getWalletBalance(){
        Task{
            showIndicator()
            let endpoint = SettingsEndpoints.getWalletBalance()
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                myBalanceLable.text = String(response.data?.walletBalance ?? 0)
            }catch{
                print("error trying to call wallet api \(error.localizedDescription)")
            }
        }
    }
}
