//
//  FAQuestionsModel.swift
//  App
//
//  Created by Abdo Emad on 20/05/2024.
//

import Foundation

struct FaqsModel: Codable {
    
    var id: Int
    var title : String
    var answer: String
    var isSelected: Bool? = false
    
    enum CodingKeys: String, CodingKey {
        case id, title, answer
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        title = try values.decode(String.self, forKey: .title)
        answer = try values.decode(String.self, forKey: .answer)
        isSelected = false
    }
}
