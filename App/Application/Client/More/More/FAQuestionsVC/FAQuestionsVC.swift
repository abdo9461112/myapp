//
//  FAQuestionsVC.swift
//  App
//
//  Created by Abdo Emad on 09/06/2024.
//

import UIKit

class FAQuestionsVC: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    
    private var questionsModel: [FaqsModel] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButtonWith(title: "Common Questions", color: .white)
        setupTableView()
        callApi()
    }
}

//MARK: - TableVeiw

extension FAQuestionsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        questionsModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: FAQuestionsCell.self, for: indexPath)
        cell.setData(model: questionsModel[indexPath.row])
        cell.cliking = {[weak self] in
            guard let self = self else{return}
            self.questionsModel[indexPath.row].isSelected = (self.questionsModel[indexPath.row].isSelected == true) ? false : true
            self.tableView.reloadRows(at: [indexPath], with: .fade)
        }
        return cell
    }
    
    private func setupTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: FAQuestionsCell.self)
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
        
    }
    
}



extension FAQuestionsVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    private func callApi(){
        showIndicator()
        Task{
            let endpoint = SettingsEndpoints.getQuestions()
            do{
                guard let response = try await request(endpoint)else{return}
                questionsModel = response.data ?? [ ]
                for index in self.questionsModel.indices {
                    self.questionsModel[index].isSelected = false
                    hideIndicator()
                }
            }catch{
                
                print("error calling api \(error.localizedDescription)")
            }
        }
    }
}
