//
//  FAQuestionsCell.swift
//  App
//
//  Created by Abdo Emad on 09/06/2024.
//
import UIKit

class FAQuestionsCell: UITableViewCell {
    
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var decribtionLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var dropDownBt: UIButton!
    var cliking : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        containerView.layer.cornerRadius = 8
        containerView.layer.borderColor = UIColor.lightGray.cgColor
        containerView.layer.borderWidth = 0.3
    }

    
    
    func setData(model: FaqsModel){
        titleLable.text = model.title
        decribtionLabel.text = model.answer
        decribtionLabel.isHidden = !(model.isSelected ?? true)
        dropDownBt.isSelected = model.isSelected ?? false
        dropDownBt.isSelected ? dropDownBt.setImage(UIImage(systemName: "chevron.up"), for: .normal) : dropDownBt.setImage(UIImage(systemName: "chevron.down"), for: .normal)
    }
    
    @IBAction func buttonTaped(_ sender: UIButton) {
        cliking?()
    }
}

