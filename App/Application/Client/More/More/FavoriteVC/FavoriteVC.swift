//
//  FavoriteVC.swift
//  App
//
//  Created by Abdo Emad on 04/06/2024.
//

import UIKit
import SnapKit

class FavoriteVC: BaseViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var productsFavouritesModel: [ProviderProductsDataModel] = []{
        didSet{
            collectionView.reloadData()
            if productsFavouritesModel.isEmpty{
                emptyMessage.text = "No Favorite Products".localized
                emptyMessage.isHidden = false
            }else{
                emptyMessage.isHidden = true
            }
        }
    }
    
    private var servicesFavouritesModel: [ProviderProductsDataModel] = []{
        didSet{
            collectionView.reloadData()
            if servicesFavouritesModel.isEmpty{
                emptyMessage.text = "No Favorite Services".localized
                emptyMessage.isHidden = false
            }else{
                emptyMessage.isHidden = true
            }
        }
    }
    
    private let responseHandler: ResponseHandler = DefaultResponseHandler()
    private var emptyMessage: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 28, weight: .semibold)
        label.textColor = .newRed
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        configuCollectionView()
        configureSegmentedControl()
    }
   
    private func setUp(){
        view.addSubview(emptyMessage)
        emptyMessage.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        addBackButtonWith(title:"Favourite".localized, color: .white)
        getClietntProducts()
    }
    
    private func configuCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(cellType: ProductCell.self)
        collectionView.register(cellType: ServicesCollectionViewCell.self)
    }
    
    private func configureSegmentedControl() {
        let normalAttributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.gray
        ]
        let selectedAttributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.white
        ]
        segmentedControl.setTitle("Products".localized, forSegmentAt: 0)
        segmentedControl.setTitle("Services".localized, forSegmentAt: 1)
        segmentedControl.setTitleTextAttributes(normalAttributes, for: .normal)
        segmentedControl.setTitleTextAttributes(selectedAttributes, for: .selected)
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(changeIndex(_:)), for: .valueChanged)
    }
    @objc func changeIndex(_ segController : UISegmentedControl){
        switch segController.selectedSegmentIndex{
        case 0:
            getClietntProducts()
        case 1:
            getClietntService()
        default:
            break
        }
    }
 
}


extension FavoriteVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            return productsFavouritesModel.count
        case 1:
            return servicesFavouritesModel.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if segmentedControl.selectedSegmentIndex == 0 {
            let cell = collectionView.dequeueReusableCell(with: ProductCell.self, for: indexPath)
            let product = productsFavouritesModel[indexPath.row]
            cell.setData(data: product)
            cell.addToFav = { [weak self] in
                guard let self = self else { return }
                guard let id = product.id else { return }
                self.removeProductFav(productId: id)
            }
            return cell
        }else  {
            let cell = collectionView.dequeueReusableCell(with: ServicesCollectionViewCell.self, for: indexPath)
            let service = servicesFavouritesModel[indexPath.row]
            cell.setData(data: service)
            cell.addToFav = { [weak self] in
                guard let self = self else { return }
                guard let id = service.id else { return }
                self.removeServiceFav(serviceId: id)
            }
            return cell
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.width - 24) / 2, height: 170)
    }
}
//MARK: - NetWorking


extension FavoriteVC{
    
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<BaseResponse<ResponseData>>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> BaseResponse<ResponseData>? {
          return try await self.responseHandler.get(endPoint, progress: progress)
      }
    
    private func getClietntProducts(){
        showIndicator()
        Task{
            let endpoint = SettingsEndpoints.getClientFavorites(type: "product")
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                productsFavouritesModel = response.data?.data ?? []
            }catch{
                print("error trying to call client products api : \(error.localizedDescription)")
            }
        }
    }
    private func removeProductFav(productId : Int){
        showIndicator()
        Task{
            let endpoint = ProductEndPoints.addProductToFav(productId: productId)
            do{
                guard let _ = try await request(endpoint)else{return}
                hideIndicator()
                getClietntProducts()
            }catch{
                print("error removing products api : \(error.localizedDescription)")
            }
        }
    }
    
    private func getClietntService(){
        showIndicator()
        Task{
            let endpoint = SettingsEndpoints.getClientFavorites(type: "service")
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                servicesFavouritesModel = response.data?.data ?? []
            }catch{
                print("error trying to call client services api : \(error.localizedDescription)")
            }
        }
    }
    private func removeServiceFav(serviceId : Int){
        showIndicator()
        Task{
            let endpoint = ServicesEndpoints.addServiceToFav(serviceId: serviceId)
            do{
                guard let _ = try await request(endpoint)else{return}
                hideIndicator()
                getClietntService()
            }catch{
                print("error removing products api : \(error.localizedDescription)")
            }
        }
    }
}



