//
//  ClientProfileVC.swift
//  App
//
//  Created by Abdo Emad on 09/06/2024.
//

import UIKit

class ClientProfileVC: BaseViewController {

    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userCity: UILabel!
    
    let responseHandler: ResponseHandler = DefaultResponseHandler()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        getProfile()
    }
  
    private func setUp(){
        addBackButtonWith(title: "Personal Profile".localized, color: .white)
        userImage.layer.cornerRadius = userImage.frame.height / 2
        userImage.layer.borderWidth = 0.3
        userImage.layer.borderColor = .mainCGColor
    }
}


extension ClientProfileVC{
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getProfile(){
        showIndicator()
        Task{
            let endpoint = SettingsEndpoints.getProfile()
            do{
                guard let response = try await self.request(endpoint)else{return}
                self.userImage.setWith(response.data?.image)
                self.userName.text = response.data?.name
                self.userCity.text = response.data?.city?.name
                self.userEmail.text = response.data?.email
                hideIndicator()
            }catch{
                print("error getting profile data : \(error.localizedDescription)")
            }
        }
    }
}
