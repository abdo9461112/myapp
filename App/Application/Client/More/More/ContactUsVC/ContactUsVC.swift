//
//  ContactUsVC.swift
//  App
//
//  Created by Abdo Emad on 19/05/2024.
//

import UIKit

class ContactUsVC: BaseViewController {

    @IBOutlet weak var nameText: NormalTextFieldView!
    @IBOutlet weak var phoneText: PhoneTextFieldView!
    @IBOutlet weak var messageSubject: NormalTextFieldView!
    @IBOutlet weak var messageText: AppTextView!
    
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var countriesModel : [CountryCodeModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDeign()
    }
    
    private func setupDeign(){
        addBackButtonWith(title: "Contact Us".localized,color: .white)
        (navigationController as? ColoredNav)?.changeApperance(to: .colored)
        
        phoneText.stakView.addTapGesture {
            self.countryCodesActions()
        }
    }

    private func countryCodesActions(){
        getCountryApi()
    }

    @IBAction func sendButtonTaped(_ sender: UIButton) {
        do {
            let name = try ValidationService.validate(name: nameText.textValue())
            let phone = try phoneText.phoneText()
            let subject = try ValidationService.validate(message: messageSubject.textValue())
            let message = try ValidationService.validate(message: messageText.textValue())
            guard let countryCodeLabel = phoneText.countryCodeLabel.text else{return}
            contactUs(name: name, phone: phone, subject: subject, message: message, countryCode: countryCodeLabel)
            
        }catch{
            
            show(errorMessage: error.localizedDescription)
        }
    }
    
   
}


extension ContactUsVC {
    
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<BaseResponse<ResponseData>>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> BaseResponse<ResponseData>? {
          return try await self.responseHandler.get(endPoint, progress: progress)
      }
    
    private func contactUs(name: String, phone: String, subject: String, message: String, countryCode: String) {
        Task {
            showIndicator()
            let endpoint = SettingsEndpoints.contactUs(name: name, phone: phone, subject: subject, messgae: message, country_code: countryCode)
            do {
                guard let response = try await request(endpoint) else { return }
                show(successMessage: response.message)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                    guard let self = self else { return }
                    self.pop()
                }
                hideIndicator()
            } catch {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    private func getCountryApi() {
        Task {
            let endpoint = AuthEndpoints.getCountryCode()
            do {
                guard let response = try await request(endpoint) else { return }
                self.countriesModel = response.data ?? []
                let vc = CountriesVC(countries: countriesModel, delegate: self)
                let nav = BaseNavigationController(rootViewController: vc)
                self.present(nav, animated: true)
            } catch {
                print("Error calling countries API: \(error.localizedDescription)")
            }
        }
    }
}

extension ContactUsVC: CountryCodeDelegate {
    func didSelectCountry(_ item: CountryCodeModel) {
        phoneText.flagImage.setWith(item.image)
        phoneText.countryCodeLabel.text = item.key
    }
}
