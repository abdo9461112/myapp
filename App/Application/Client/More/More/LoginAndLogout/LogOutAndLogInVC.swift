//
//  LogOut&LogInVC.swift
//  App
//
//  Created by Abdo Emad on 01/06/2024.
//

import UIKit

class LogOutAndLogInVC: BaseViewController {
    @IBOutlet weak var titleLabel: UILabel!
    var lableText : String!
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = lableText
    }
  
    
    @IBAction func yesButton(_ sender: UIButton) {
        if UserDefaults.isLogin{
            clientLogout()
        }else{
            UserDefaults.isFirstTime = true
            goToLogin()
        }
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
}

//MARK: - NetWorking

extension LogOutAndLogInVC{
    
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<BaseResponse<ResponseData>>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> BaseResponse<ResponseData>? {
          return try await self.responseHandler.get(endPoint, progress: progress)
      }
      
    private func clientLogout(){
        showIndicator()
        Task{
            let endpoint = AuthEndpoints.clientLogout()
            do{
                guard let _ = try await request(endpoint) else{return}
                hideIndicator()
                UserDefaults.user = nil
                UserDefaults.accessToken = nil
                UserDefaults.isLogin = false
                UserDefaults.isFirstTime = true
                goToLogin()
            }catch{
                print("error trying to log out : \(error.localizedDescription)")
            }
        }
    }
    
    private func goToLogin(){
        let vc = CheckUserTypeVC()
        let navController = UINavigationController(rootViewController: vc)
        AppHelper.changeWindowRoot(vc: navController)
    }
    
}
