//
//  MoreSection.swift
//  App
//
//  Created by MGAbouarab on 16/01/2024.
//

import Foundation

struct MoreSection {
    let items: [MoreItem]
}
