//
//  ConfirmDeleteVC.swift
//  App
//
//  Created by Abdo Emad on 22/05/2024.
//

import UIKit
import SnapKit

class ConfirmDeleteVC: UIViewController {

    private let halfView : UIView = {
        let view = UIView()
        view.backgroundColor = .systemBackground
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let image: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(systemName: "checkmark.seal.fill")
        image.contentMode = .scaleAspectFit
        image.tintColor = .main
        return image
    }()
    
    private let titleLable : UILabel = {
        let lable = UILabel()
        lable.text = "This account has been deleted successfully".localized
        lable.textAlignment = .center
        lable.font = UIFont.boldSystemFont(ofSize: 20)
        lable.textColor = .label
        lable.numberOfLines = 2
        return lable
    }()
     
    private let button : UIButton = {
        let button = UIButton()
        button.setTitle("Login".localized, for: .normal)
        button.tintColor = .white
        button.backgroundColor = .main
        button.addCornerRadius(radious: 10)
        return button
    }()
    
    private lazy var vStackView : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            vStack,
            button
        ])
        stack.axis = .vertical
        stack.spacing = 35
        stack.alignment = .fill
        stack.distribution = .fill
        return stack
    }()
    
    private lazy var vStack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            image,
            titleLable,
        ])
        stack.axis = .vertical
        stack.spacing = 20
        stack.alignment = .fill
        stack.distribution = .fill
        return stack
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layout()
        handleTouches()
    }
  
   
    private func layout(){
        view.addSubview(halfView)
        view.backgroundColor = .systemBackground
        halfView.addSubview(vStackView)
        halfView.snp.makeConstraints { make in
            make.top.equalTo(view.snp.top).offset(16)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.greaterThanOrEqualTo(view.snp.bottom).offset(-350)
        }
        vStackView.snp.makeConstraints { make in
            make.top.equalTo(halfView.snp.top)
            make.leading.equalTo(halfView.snp.leading).offset(16)
            make.trailing.equalTo(halfView.snp.trailing).offset(-16)
            make.bottom.equalTo(halfView.snp.bottom).offset(-20)
        }
        image.snp.makeConstraints { make in
            make.height.equalTo(190)
        }
        button.snp.makeConstraints { make in
            make.height.equalTo(50)
        }
    }
}


extension ConfirmDeleteVC {
    
    private func handleTouches(){
        button.addTarget(self, action: #selector(goToLoginView), for: .touchUpInside)
    }
    
       @objc func goToLoginView(_ sender: UIButton) {
           let vc = CheckUserTypeVC()
           let navController = UINavigationController(rootViewController: vc)
           AppHelper.changeWindowRoot(vc: navController)
           UserDefaults.isFirstTime = true
           UserDefaults.accessToken = nil
           UserDefaults.isLogin = false 
       }
}
