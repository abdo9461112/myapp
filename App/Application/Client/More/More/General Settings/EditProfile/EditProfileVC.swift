//
//  EditProfileVC.swift
//  App
//
//  Created by Abdo Emad on 09/06/2024.
//

import UIKit

class EditProfileVC: BaseViewController {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: NormalTextFieldView!
    @IBOutlet weak var userEmail: EmailTextFieldView!
    @IBOutlet weak var userCity: DropDownTextFieldView!
    
    private var imageData:Data?
    private var cityId : Int?
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    private var cityModel : [City] = [ ]
    private var delegate : ViewControllerTransitionDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        getProfile()
        getCities()
    }
    
    private func setUp(){
        addBackButtonWith(title: "My Account".localized, color: .white)
        userImage.layer.cornerRadius = userImage.frame.height / 2
        userCity.delegate = self
        delegate = self
    }
    @IBAction func pickImageButtTaped(_ sender: UIButton) {
        ImagePicker().pickImage { image, imageData in
            self.userImage.image = image
            self.imageData = imageData
        }
    }
    
    @IBAction func saveButton(_ sender: UIButton) {
        do{
            let name = try ValidationService.validate(name: userName.textValue())
            
            let email = try ValidationService.validate(email: userEmail.emailTextValue())
            updateProfile(name: name, email: email, image: imageData, cityId: cityId ?? 0)
        }catch{
            
            show(errorMessage: error.localizedDescription)
        }
    }
    
    @IBAction func deleteAccountButton(_ sender: UIButton) {
        presentDeleteVC()
    }
    
    
}

//MARK: - NetWorkin


extension EditProfileVC{
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<BaseResponse<ResponseData>>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> BaseResponse<ResponseData>? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getProfile(){
        showIndicator()
        Task{
            let endpoint = SettingsEndpoints.getProfile()
            do{
                guard let response = try await self.request(endpoint)else{return}
                hideIndicator()
                userImage.setWith(response.data?.image)
                userName.set(text: response.data?.name)
                userCity.set(value:  response.data?.city)
                userEmail.set(text: response.data?.email)
                cityId = response.data?.city?.id
            }catch{
                print("error getting profile data : \(error.localizedDescription)")
            }
        }
    }
    
    private func updateProfile(name: String, email: String, image: Data?, cityId: Int){
        showIndicator()
        Task{
            let endpoint = SettingsEndpoints.updateProfile(name: name, email: email, image: image, cityId: cityId)
            do{
                guard let respone = try await request(endpoint)else{return}
                hideIndicator()
                show(successMessage: respone.message)
                pop(animated: true)
            }catch{
                print("error updateting profile data : \(error.localizedDescription)")
                
            }
        }
    }
    private func getCities(){
        Task{
            let endpoint = AuthEndpoints.getCities()
            do{
                guard let response = try await request(endpoint)else{return}
                cityModel = response.data ?? [ ]
            }catch{
                
                print("error getting cities data : \(error.localizedDescription)")
            }
        }
    }
}
extension EditProfileVC: DropDownTextFieldViewDelegate{
    func dropDownList(for textFieldView: DropDownTextFieldView) -> [any DropDownItem] {
        cityModel
    }
    
    func didSelect(item: any DropDownItem, for textFieldView: DropDownTextFieldView) {
        cityId = item.id
    }
    
    
}


extension EditProfileVC : ViewControllerTransitionDelegate{
    func presentConfirmDeleteVC() {
        let vc = ConfirmDeleteVC()
        if let sheet = vc.sheetPresentationController{
            if #available(iOS 16.0, *){
                let customDetent = UISheetPresentationController.Detent.custom { context in
                    return 350
                }
                sheet.detents = [customDetent]
                sheet.preferredCornerRadius = 25
                vc.isModalInPresentation = true
            }
        }
        present(vc, animated: true)
    }
    private func presentDeleteVC(){
        let vc = DeleteVC()
        vc.transitionDelegate = self
        if let sheet = vc.sheetPresentationController{
            if #available(iOS 16.0, *){
                let customDetent = UISheetPresentationController.Detent.custom { context in
                    return 350
                }
                sheet.detents = [customDetent]
                sheet.preferredCornerRadius = 25
                sheet.prefersGrabberVisible = true
            }
        }
        present(vc, animated: true)
    }
}
