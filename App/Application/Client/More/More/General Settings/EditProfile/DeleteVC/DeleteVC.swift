import UIKit

protocol ViewControllerTransitionDelegate: AnyObject {
    func presentConfirmDeleteVC()
}

class DeleteVC: BaseViewController {
        
    @IBOutlet weak var viewContainer: UIView!
    
    var initialCenter: CGPoint = .zero
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    weak var transitionDelegate: ViewControllerTransitionDelegate?
    
    private let halfView : UIView = {
        let view = UIView()
        view.backgroundColor = .systemBackground
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        return view
    }()
    
    private let image: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(systemName: "exclamationmark.triangle")
        image.contentMode = .scaleAspectFit
        image.tintColor = .main
        return image
    }()
    
    private let titleLable : UILabel = {
        let lable = UILabel()
        lable.text = "Are you sure you want to delete your account ?".localized
        lable.textAlignment = .center
        lable.font = UIFont.boldSystemFont(ofSize: 17)
        lable.textColor = .label
        lable.numberOfLines = 2
        return lable
    }()
    
    
    private let subtitleLable : UILabel = {
        let lable = UILabel()
        lable.text = "In the event of deletion, all data will be deleted And you can't recover it again".localized
        lable.textAlignment = .center
        lable.font = UIFont(name: "System", size: 14)
        lable.textColor = .lightGray
        lable.numberOfLines = 0
        return lable
    }()
    
    private let yesbutton : UIButton = {
        let button = UIButton()
        button.setTitle("Yes".localized, for: .normal)
        button.backgroundColor = .green
        button.tintColor = .white
        button.layer.cornerRadius = 10
        return button
    }()
    
    private let backbutton : UIButton = {
        let button = UIButton()
        button.setTitle("Back".localized, for: .normal)
        button.backgroundColor = .main
        button.tintColor = .white
        button.layer.cornerRadius = 10
        return button
    }()
    
    private lazy var hStackView : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            backbutton,
            yesbutton
        ])
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fill
        stack.spacing = 10
        return stack
    }()
    
    private lazy var vStackView : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            titleLable,
            subtitleLable
        ])
        stack.axis = .vertical
        stack.spacing = 0
        stack.alignment = .center
        stack.distribution = .fillEqually
        return stack
    }()
    
    private lazy var vStack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            image,
            vStackView,
            hStackView
        ])
        stack.axis = .vertical
        stack.spacing = 20
        stack.alignment = .fill
        stack.distribution = .fill
        return stack
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handleTouches()
        layout()
    }
    
    private func layout(){
        
        view.addSubview(halfView)
        halfView.snp.makeConstraints { make in
            make.top.equalTo(view.snp.top).offset(16)
            make.leading.equalTo(view.snp.leading).offset(16)
            make.trailing.equalTo(view.snp.trailing).offset(-16)
            make.bottom.greaterThanOrEqualTo(view.snp.bottom).offset(-350)
        }
        halfView.addSubview(vStack)
        vStack.snp.makeConstraints { make in
            make.top.equalTo(halfView.snp.top)
            make.leading.equalTo(halfView.snp.leading)
            make.trailing.equalTo(halfView.snp.trailing)
            make.bottom.equalTo(halfView.snp.bottom)

            
        }
        backbutton.snp.makeConstraints { make in
            make.height.equalTo(50)
            make.width.equalTo(yesbutton.snp.width)
        }
        yesbutton.snp.makeConstraints { make in
            make.height.equalTo(50)
        }
        image.snp.makeConstraints { make in
            make.height.equalTo(160)
        }
        
        
    }
    private func handleTouches(){
        
        backbutton.addTarget(self, action: #selector(closeButtonTapped), for: .touchUpInside)
        
        yesbutton.addTarget(self, action: #selector(yesButtonTapped), for: .touchUpInside)
    }
    
    @objc func yesButtonTapped() {
        dismiss(animated: true) { [weak self] in
            self?.deleteAccount()
            self?.transitionDelegate?.presentConfirmDeleteVC()
        }
    }
    
    @objc func closeButtonTapped() {
        dismiss(animated: true, completion: nil)
    }
    
}


extension DeleteVC{

    func request<ResponseData: Decodable>(_ endPoint: Endpoint<BaseResponse<ResponseData>>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress).data
    }

    private func deleteAccount(){
        Task{
            let endpoint = SettingsEndpoints.delete()
            do{
                guard let _ = try await request(endpoint) else {return}
            }catch{
                print("error Deleting The Account : \(error.localizedDescription)")
            }
        }
    }
}

