//
//  EditNotificationVC.swift
//  App
//
//  Created by Abdo Emad on 09/06/2024.
//

import UIKit

class EditNotificationVC: BaseViewController {

    @IBOutlet weak var switchButton: UISwitch!
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    override func viewDidLoad() {
        super.viewDidLoad()
        getProfile()
        addBackButtonWith(title: "Notification".localized, color: .white)
        switchButton.transform = CGAffineTransform(scaleX: 0.90, y: 0.90)
    }


    @IBAction func switchButtonTaped(_ sender: UISwitch) {
        changeNotification()
    }
    
}

extension EditNotificationVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getProfile(){
        showIndicator()
        Task{
            let endpoint = SettingsEndpoints.getProfile()
            do{
                guard let response = try await request(endpoint)else{return}
                switchButton.isOn = response.data?.isNotify ?? false
                hideIndicator()
            }catch{
                
            }
        }
    }
    private func changeNotification(){
        showIndicator()
        Task{
            let endpoint = SettingsEndpoints.changeNotification()
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                show(successMessage: response.message)
                pop()
            }catch{
                
            }
        }
    }
}

