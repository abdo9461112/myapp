//
//  GeneralSettingsVC.swift
//  App
//
//  Created by Abdo Emad on 20/05/2024.
//

import UIKit

class GeneralSettingsVC: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private var items: [MoreSection] = []
    private lazy var settingsItems: [MoreSection] = [
        .init(
            items: [
                .init(
                    image: .init(systemName: "person.fill"),
                    name: "Personal_Profile_More_Title".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = EditProfileVC()
                        push(vc)
                    },
            ]
        ),
        .init(
            items: [
                .init(
                    image: .init(systemName: "translate"),
                    name: "Language".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = ChangeLangVC()
                        push(vc)
                    },
            ]
        ),
        .init(
            items: [
                .init(
                    image: .init(systemName: "phone.fill"),
                    name: "Change Phone Number".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = VerificationCodeVC()
                        push(vc)
                    },
            ]
        ),
        .init(
            items: [
                .init(
                    image: .init(systemName:"bell.fill"),
                    name: "Notification".localized) { [weak self] in
                        guard let self = self else {return}
                        let vc = EditNotificationVC()
                        push(vc)
                    },
            ]
        ),
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        
    }
    
    func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.contentInset = .init(top: 20, left: 0, bottom: 20, right: 0)
        addBackButtonWith(title: "General Settings".localized,color: .white)
    }
    override func viewWillAppear(_ animated: Bool) {
        items = settingsItems
        tableView.reloadData()
    }
    
}


extension GeneralSettingsVC: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items[section].items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = self.items[indexPath.section].items[indexPath.row]
        
        let cell = UITableViewCell()
        cell.contentConfiguration = UIListContentConfiguration.defaultSubtitleCell(
            image: item.image,
            title: item.name
        )
        cell.accessoryType = item.accessoryType
        cell.backgroundColor = item.backgroundColor
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.items[indexPath.section].items[indexPath.row].action()
        tableView.reloadData()
    }
}


