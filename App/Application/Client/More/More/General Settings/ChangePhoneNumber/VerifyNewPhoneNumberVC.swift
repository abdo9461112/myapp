//
//  VerifyNewPhoneNumberVC.swift
//  App
//
//  Created by Abdo Emad on 09/06/2024.
//

import UIKit

class VerifyNewPhoneNumberVC: BaseViewController {
    @IBOutlet weak var codeField: OTPTextField!
    
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    var code = ""
    var oldCountryCode : String?
    var oldPhone: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigne()
    }

    private func setupDesigne(){
        addBackButtonWith(title: "Change phone".localized,color: .white)
        codeOTPConfigure()
        codeField.didEnterLastDigit = { code in
            self.code = code
        }
        codeField.becomeFirstResponder()
    }
    private func codeOTPConfigure(){
        codeField.configure(with: 4)
        codeField.didEnterLastDigit = { [weak self] code in
            guard let self = self else {return}
            self.codeField.resignFirstResponder()
        }
    }
    

    @IBAction func confirmButtonTaped(_ sender: UIButton) {
        do{
            let code = try ValidationService.validate(verificationCode: code)
            verifyPhone(phone: oldPhone ?? " ", code: code , countryCode: oldCountryCode ?? "")
        }catch{
            show(errorMessage: error.localizedDescription)
        }
    }
    
    @IBAction func resendButtonTaped(_ sender: UIButton) {
        resendCode(phone: oldPhone ?? " ", countryCode: oldCountryCode ?? "")
    }
    
}

extension VerifyNewPhoneNumberVC{
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
  private  func resendCode(phone: String, countryCode: String) {
        Task {
            let endpoint = AuthEndpoints.resendCode(phone: phone, countryCode: countryCode, type: "change_phone")
            do {
                guard let response = try await request(endpoint) else { return }
                show(successMessage: response.message)
            } catch {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    private func verifyPhone(phone: String, code: String, countryCode: String){
        showIndicator()
        Task{
            let endpoint = AuthEndpoints.verifyPhone(phone: phone, code: code, countryCode: countryCode, type: "change_phone")
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                if response.key == .success{
                    show(successMessage: response.message)
                    popToRoot()
                }else{
                    show(errorMessage: response.message)
                }
            }catch{
                
            }
        }
    }
}


