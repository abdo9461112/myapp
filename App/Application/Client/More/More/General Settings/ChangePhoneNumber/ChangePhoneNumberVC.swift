//
//  ChangePhoneNumberVC.swift
//  App
//
//  Created by Abdo Emad on 21/05/2024.
//

import UIKit

class ChangePhoneNumberVC: BaseViewController {

    @IBOutlet weak var newPhoneNumber: PhoneTextFieldView!
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var countryCodesModel: [CountryCodeModel] =  []
    var oldPhone = " "
    var oldCountryCode =  ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesign()
    }
    
    private func setupDesign(){
        addBackButtonWith(title: "Change phone".localized,color: .white)
        
        newPhoneNumber.stakView.addTapGesture {
            self.getCountryApi()
        }
    }
   

    @IBAction func confirmButtonTaped(_ sender: UIButton) {
        do {
            let phone = try newPhoneNumber.phoneText()
            guard newPhoneNumber.phoneTextValue() != oldPhone else{
                show(errorMessage: "You must enter a new phone number".localized)
                return
            }
            changePhone(phone: phone, countryCode: newPhoneNumber.countryCodeLabel.text!)
        }catch{
            show(errorMessage: error.localizedDescription)
        }
        
    }
    
    
}

extension ChangePhoneNumberVC{
    
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
   
    private func changePhone(phone: String, countryCode: String){
        showIndicator()
        Task{
            let endpoint = AuthEndpoints.clientRegisterPhone(countryCode: countryCode, newPhone: phone)
            do{
                guard let response = try await request(endpoint) else{return}
                hideIndicator()
                if response.key == .success{
                    show(successMessage: response.message)
                    UserDefaults.user?.phone = phone
                    UserDefaults.user?.countryCode =  countryCode
                    let vc = VerifyNewPhoneNumberVC()
                    vc.oldCountryCode = oldCountryCode
                    vc.oldPhone = oldPhone
                    push(vc)
                }else{
                    show(errorMessage: response.message)
                }
               
            }catch{
                print("error trying to change phone \(error.localizedDescription)")
                
            }
        }
    }
    private func getCountryApi(){
        Task{
            let endpoint = AuthEndpoints.getCountryCode()
            do{
                guard let response = try await self.request(endpoint) else {return}
                self.countryCodesModel = response.data ?? [ ]
                let vc = CountriesVC(countries: countryCodesModel, delegate: self)
                let nav = BaseNavigationController(rootViewController: vc)
                self.present(nav, animated: true)
            }catch{
                print("Error calling countries api : \(error.localizedDescription)")
            }
        }
    }
}


extension ChangePhoneNumberVC: CountryCodeDelegate{
    func didSelectCountry(_ item:  CountryCodeModel) {
        newPhoneNumber.flagImage.setWith(item.image)
        newPhoneNumber.countryCodeLabel.text = item.key
    }
}
