//
//  ChangePhoneNumberVC.swift
//  App
//
//  Created by Abdo Emad on 21/05/2024.
//

import UIKit

class VerificationCodeVC: BaseViewController {
    
    @IBOutlet weak var textField: OTPTextField!
    @IBOutlet weak var confirmButton: UIButton!
    
    
    //MARK: - Properties -
    private var countryCode = UserDefaults.user?.countryCode
    private var phoneNumber = UserDefaults.user?.phone
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    var code = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigne()
    }
    override func viewDidAppear(_ animated: Bool) {
        clientRequestApi()
    }
    
    private func setupDesigne(){
        addBackButtonWith(title: "Change phone".localized,color: .white)
        codeOTPConfigure()
        textField.didEnterLastDigit = { code in
            self.code = code
        }
    }
    private func codeOTPConfigure(){
        textField.configure(with: 4)
        textField.didEnterLastDigit = { [weak self] code in
            guard let self = self else {return}
            self.textField.resignFirstResponder()
        }
    }
    
    
    
    @IBAction func confirmButtanTaped(_ sender: UIButton) {
        do{
            let code = try ValidationService.validate(verificationCode: code)
            changePhoneNumber(phone: phoneNumber ?? " ", code: code, countryCode: countryCode!)
        }catch{
            show(errorMessage: error.localizedDescription)
        }
        
    }
    
    
    @IBAction func resendButtonTaped(_ sender: UIButton) {
        resendCode(phone: phoneNumber ?? " ", countryCode: countryCode!)
    }
    
    
}


extension VerificationCodeVC{
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    func resendCode(phone: String, countryCode: String) {
        showIndicator()
        Task {
            let endpoint = AuthEndpoints.resendCode(phone: phone, countryCode: countryCode, type: "change_phone")
            do {
                guard let response = try await request(endpoint) else { return }
                hideIndicator()
                show(successMessage: response.message)
            } catch {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    
    func changePhoneNumber(phone:String,code:String, countryCode: String){
        Task{
            showIndicator()
            
            let endpoint = AuthEndpoints.verifyPhone(phone: phone, code: code, countryCode: countryCode, type: "change_phone")
            do{
                guard let response = try await self.request(endpoint)else {return}
                if response.key == .success {
                    show(successMessage: response.message)
                    let vc = ChangePhoneNumberVC()
                    vc.oldPhone = phone
                    vc.oldCountryCode = countryCode
                    push(vc)
                } else {
                    show(errorMessage: response.message)
                }
                hideIndicator()
            }catch{
                
                print("error trying to change phonne number \(error.localizedDescription)")
            }
            
        }
    }
    
    func clientRequestApi(){
        Task{
            let endpoint = AuthEndpoints.clientRequestChangePhone()
            do{
                guard let response = try await request(endpoint)else{return}
                show(successMessage: response.message)
            }catch{
                
                print("error trying to call request to change client phone number : \(error.localizedDescription) ")
            }
        }
    }
}


