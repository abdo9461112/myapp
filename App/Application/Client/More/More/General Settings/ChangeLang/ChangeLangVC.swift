//
//  ChangeLangVC.swift
//  App
//
//  Created by Abdo Emad on 09/06/2024.
//

import UIKit


class ChangeLangVC: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var languages: [ChangeLangModel] = [
        .init(title: "اللغة العربية",
              localeSymbol: Language.Languages.ar,
              image: "ArabicLogo",
              isSelected: false),
        .init(title: "English",
              localeSymbol:  Language.Languages.en,
              image: "EnglishLogo",
              isSelected: false)
    ]
    private let padding: CGFloat = 16
    private let spacing: CGFloat = 10
    var selectedLang = "en"
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        self.addBackButtonWith(title: "Language".localized,color: .white)
        if Language.currentLanguage() == "ar" {
            
            self.languages[0].isSelected = true
        }else{
            self.languages[1].isSelected = true

        }
    }

    @IBAction func saveButtonTaped(_ sender: UIButton) {
        Language.handleViewDirection()
        Language.setAppLanguage(lang: selectedLang)
        chagnLang(lang: selectedLang)
        let vc = UserTabBarController()
        AppHelper.changeWindowRoot(vc: vc)
    }
    
}

//MARK: - collectionView

extension ChangeLangVC {
    func setupCollectionView() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(cellType: ChangeLangCell.self)
        self.collectionView.contentInset = .init(top: 0, left: padding, bottom: 0, right: padding)
    }
}

extension ChangeLangVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.languages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: ChangeLangCell.self, for: indexPath)
        cell.set(language: languages[indexPath.row])
        return cell
    }
}

extension ChangeLangVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let selectedIndex = self.languages.firstIndex(where: {$0.isSelected}) {
            self.languages[selectedIndex].isSelected = false
            collectionView.reloadItems(at: [IndexPath(item: selectedIndex, section: 0)])
            self.languages[indexPath.row].isSelected = true
            self.selectedLang = self.languages[indexPath.row].localeSymbol
            
            collectionView.reloadItems(at: [indexPath])
        }
    }
}

extension ChangeLangVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let languageCount: CGFloat = 2//CGFloat(languages.count)
        let allSpacing = spacing*languageCount-1
        let length = (collectionView.bounds.width - 2*padding - allSpacing)/languageCount
        return .init(
            width: length,
            height: length
        )
    }
}


extension ChangeLangVC{
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func chagnLang(lang:String){
        Task{
            let endpoint = SettingsEndpoints.changeLang(lang: lang)
            do{
                guard let response = try await request(endpoint)else{return}
                show(successMessage: response.message)
            }catch{
                print("error changing lang api : \(error.localizedDescription)")
            }
        }
    }
}
