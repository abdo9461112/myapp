//
//  ChangeLangModel.swift
//  App
//
//  Created by Abdo Emad on 21/05/2024.
//

import Foundation

struct ChangeLangModel {
    let title: String
    let localeSymbol: String
    let image : String
    var isSelected: Bool = false
}
