//
//  ChangeLangCell.swift
//  App
//
//  Created by Abdo Emad on 09/06/2024.
//

import UIKit

class ChangeLangCell: UICollectionViewCell {
    
    @IBOutlet weak var flagContainerView: UIView!
    @IBOutlet weak var languageNameLabel: UILabel!
    @IBOutlet weak var flagImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupInitialDesign()
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.layer.cornerRadius = .cardCorner
        self.clipsToBounds = true
        self.layer.borderWidth = .defaultBorderWidth
    }
    
    //MARK: - Data -
    func set(language data: ChangeLangModel) {
        self.flagImageView.image = UIImage(named: data.image)
        self.languageNameLabel.text = data.title
        self.layer.borderColor = data.isSelected ? UIColor.white.cgColor : UIColor.separator.cgColor
        self.backgroundColor = data.isSelected ? .main : .clear
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
}
