//
//  CartCell.swift
//  App
//
//  Created by Abdo Emad on 11/06/2024.
//

import UIKit

class CartCell: UITableViewCell {
    @IBOutlet weak var providerImage: UIImageView!
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var productsCount: UILabel!
    var onDelete : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none
    }

    func setData(data: CartDataModel){
        providerImage.setWith(data.provider?.image)
        providerName.text = data.provider?.name
        productsCount.text = "\(data.countOfItems ?? 0)"
    }
    @IBAction func deleteButton(_ sender: UIButton) {
        onDelete?()
    }
    
}
