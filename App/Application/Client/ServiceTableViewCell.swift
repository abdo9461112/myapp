//
//  ServiceTableViewCell.swift
//  App
//
//  Created by Abdo Emad on 12/06/2024.
//

import UIKit

class ServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var servicePrice: UILabel!    
    @IBOutlet weak var deleteButton: UIButton!
    
    var deleteAction : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        serviceImage.layer.cornerRadius = serviceImage.frame.height / 2
        selectionStyle = .none
    }

    func setData(_ data:Product){
        serviceImage.setWith(data.attachments?.first?.path)
        serviceName.text = data.name
        servicePrice.text = "\(data.price ?? 0.0) " + "SAR".localized
    }
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        deleteAction?()
    }
    
}
