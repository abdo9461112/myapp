//
//  ProductTableViewCell.swift
//  App
//
//  Created by Abdo Emad on 12/06/2024.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productCount: UILabel!
    
    @IBOutlet weak var countingView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var quantityStackView: UIStackView!
    
    @IBOutlet weak var quantityLable: UILabel!
    
    var onIncreased: (()->())?
    var onDcreased: (()->())?
    var onDeleted: (()->())?

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        productImage.layer.cornerRadius = productImage.frame.height / 2
    }

  
     func setData(_ data: Product){
        productImage.setWith(data.attachments?.first?.path)
        productPrice.text = "\(data.price ?? 0.0)" + " " + "SAR".localized
        productName.text = data.name
        productCount.text = "\(data.quantity ?? 1)"
        quantityLable.text = "\(data.quantity ?? 1)"
    }
    
    //MARK: - Actions
    
    @IBAction func plusButton(_ sender: UIButton) {
        onIncreased?()
    }
    
    @IBAction func munsButton(_ sender: UIButton) {
        onDcreased?()
    }
    
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        onDeleted?()

    }
    
}
