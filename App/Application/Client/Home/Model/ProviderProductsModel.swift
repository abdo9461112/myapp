//
//  ProductCell.swift
//  App
//
//  Created by Abdo Emad on 04/06/2024.
//

import Foundation

// MARK: - ProviderProductsModel
struct ProviderProductsModel: Codable {
    var pagination: ProvidersPagination?
    var data: [ProviderProductsDataModel]?
}

// MARK: - ProviderProductsDataModel
struct ProviderProductsDataModel: Codable {
    var id: Int?
    var nameAr, nameEn: String?
    var price: Double?
    var description: String?
    var quantity, maxPurchases: Int?
    var isAvaliable: Bool?
    var createdAt: String?
    var attachments: [Attachment]?
    var name: String?
    var isFavorite: Bool?
    var stockStatus: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case nameAr = "name_ar"
        case nameEn = "name_en"
        case price, description, quantity
        case maxPurchases = "max_purchases"
        case isAvaliable = "is_avaliable"
        case createdAt = "created_at"
        case attachments
        case isFavorite = "is_favorite"
        case stockStatus = "stock_status"
    }
}
