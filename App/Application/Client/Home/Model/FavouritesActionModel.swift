//
//  ProductCell.swift
//  App
//
//  Created by Abdo Emad on 04/06/2024.
//

import Foundation

struct FavouritesActionModel: Codable {
    var isFavorite: Bool?
    
    enum CodingKeys: String, CodingKey {
        case isFavorite = "is_favorite"
    }
}
