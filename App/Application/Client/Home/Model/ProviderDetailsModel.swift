//
//  ProductCell.swift
//  App
//
//  Created by Abdo Emad on 04/06/2024.
//

import Foundation

// MARK: - ProviderDetailsModel
struct ProviderDetailsModel: Codable {
    var id: Int?
    var name: String?
    var image: String?
    var rate: ProviderRate?
    var description: String?
    var isAvaliable: Bool?
    var products: [ProviderProduct]?
    var services: [ProviderService]?
    var timeWorks: [ProviderTimeWork]?

    enum CodingKeys: String, CodingKey {
        case id, name, image, rate, description
        case isAvaliable = "is_avaliable"
        case products, services
        case timeWorks = "time_works"
    }
}

// MARK: - Product
struct ProviderProduct: Codable {
    var id: Int?
    var nameAr, nameEn: String?
    var price: Double?
    var description: String?
    var quantity, maxPurchases: Int?
    var isAvaliable: Bool?
    var createdAt: String?
    var attachments: [Attachment]?
    var isFavorite: Bool?
    var stockStatus: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case nameAr = "name_ar"
        case nameEn = "name_en"
        case price, description, quantity
        case maxPurchases = "max_purchases"
        case isAvaliable = "is_avaliable"
        case createdAt = "created_at"
        case attachments
        case isFavorite = "is_favorite"
        case stockStatus = "stock_status"
    }
}

// MARK: - Attachment
struct ProviderAttachments: Codable {
    var id: Int?
    var name: String?
    var path: String?
}

// MARK: - Rate
struct ProviderRate: Codable {
    var value, count: Double?
}

// MARK: - Service
struct ProviderService: Codable {
    var id: Int?
    var nameAr, nameEn: String?
    var price: Double?
    var description: String?
    var isAvaliable: Bool?
    var attachments: [Attachment]?
    var isFavorite: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id
        case nameAr = "name_ar"
        case nameEn = "name_en"
        case price, description
        case isAvaliable = "is_avaliable"
        case attachments
        case isFavorite = "is_favorite"
    }
}

// MARK: - TimeWork
struct ProviderTimeWork: Codable {
    var id: Int?
    var from, to, day: String?
    var status: Bool?
}
