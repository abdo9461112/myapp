//
//  ProductCell.swift
//  App
//
//  Created by Abdo Emad on 04/06/2024.
//

import Foundation

// MARK: - ProviderDataModel
struct ProviderDataModel: Codable {
    var pagination: ProvidersPagination?
    var data: [ProviderModel]?
}

// MARK: - ProviderModel
struct ProviderModel: Codable {
    var id: Int?
    var name: String?
    var image: String?
    var isAvaliable: Bool?
    var rate: Double?

    enum CodingKeys: String, CodingKey {
        case id, name, image , rate
        case isAvaliable = "is_avaliable"
    }
}

// MARK: - Pagination
struct ProvidersPagination: Codable {
    var totalItems, countItems, perPage, totalPages: Int?
    var currentPage: Int?
    var nextPageURL: String?
    var pervPageURL: String?

    enum CodingKeys: String, CodingKey {
        case totalItems = "total_items"
        case countItems = "count_items"
        case perPage = "per_page"
        case totalPages = "total_pages"
        case currentPage = "current_page"
        case nextPageURL = "next_page_url"
        case pervPageURL = "perv_page_url"
    }
}
