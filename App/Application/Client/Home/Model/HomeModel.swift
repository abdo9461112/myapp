//
//  HomeCategoryModel.swift
//  App
//
//  Created by Abdo Emad on 29/05/2024.
//

import Foundation

struct HomeModel: Codable {
    let sliders: [Slider]
    let categories, mostRates: [Category]

    enum CodingKeys: String, CodingKey {
        case sliders, categories
        case mostRates = "most_rates"
    }
}

// MARK: - Category
struct Category: Codable {
    let id: Int?
    let name: String?
    let image: String
    let isAvaliable: Bool?

    init(from decoder: any Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.image = try container.decode(String.self, forKey: .image)
        self.isAvaliable = try container.decodeIfPresent(Bool.self, forKey: .isAvaliable)
    }
    
    enum CodingKeys: String, CodingKey {
        case id, name, image
        case isAvaliable = "is_avaliable"
    }
}

// MARK: - Slider
struct Slider: Codable {
    let id: Int
    let image: String?
    let link: String?
}


struct CategoriesModel: Codable ,DropDownItem{
    let id : Int
    let name: String
}
