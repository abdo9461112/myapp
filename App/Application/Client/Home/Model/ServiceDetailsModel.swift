//
//  ProductCell.swift
//  App
//
//  Created by Abdo Emad on 04/06/2024.
//

import Foundation

// MARK: - ServiceDetailsModel
struct ServiceDetailsModel: Codable {
    var id: Int?
    var name: String?
    var price: Int?
    var description: String?
    var isAvaliable: Bool?
    var category: Category?
    var nameAr: String?
    var nameEn: String?
    var createdAt: String?
    var provider: Provider
    var attachments: [Attachment]?

    enum CodingKeys: String, CodingKey {
        case id, name, price, description
        case isAvaliable = "is_avaliable"
        case category
        case createdAt = "created_at"
        case provider, attachments
        case nameAr = "name_ar"
        case nameEn = "name_en"
    }
}
