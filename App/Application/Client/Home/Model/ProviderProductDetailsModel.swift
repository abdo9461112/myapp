//
//  ProductCell.swift
//  App
//
//  Created by Abdo Emad on 04/06/2024.
//

import Foundation

// MARK: - ProviderProductDetailsModel
struct ProviderProductDetailsModel: Codable {
    var id: Int?
    var name: String? {
        return Language.isRTL() ? nameAr : nameEn
    }
    var price: Double?
    var description: String?
    var quantity, maxPurchases: Int?
    var isAvaliable: Bool?
    var category: Category?
    var createdAt: String?
    var provider: Provider?
    var attachments: [Attachment]?
    var isFavorite: Bool?
    var stockStatus: String?
    var nameAr, nameEn: String?

    enum CodingKeys: String, CodingKey {
        case id, price, description, quantity
        case maxPurchases = "max_purchases"
        case isAvaliable = "is_avaliable"
        case category
        case createdAt = "created_at"
        case provider, attachments
        case isFavorite = "is_favorite"
        case stockStatus = "stock_status"
        case nameAr = "name_ar"
        case nameEn = "name_en"
    }
}

// MARK: - Attachment
struct Attachment: Codable {
    var id: Int?
    var name: String?
    var path: String?
}

// MARK: - Provider
struct Provider: Codable {
    var id: Int?
    var name, fullPhone, commercialRegister, email: String?
    var countryCode, phone, token: String?
    var image: String?
    var description: String?
    var isNotify, isActive, isApproved, isRequestDelegate: Bool?
    var lang: String?
    var walletBalance: Double?
    var type: String?
    var location: Location?
    var isAvaliable: Bool?
    var rate: Double?
    var totalKilometers: String?
    var city: City?
    var bank: Bank?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case fullPhone = "full_phone"
        case commercialRegister = "commercial_register"
        case email
        case countryCode = "country_code"
        case phone, token, image, description
        case isNotify = "is_notify"
        case isActive = "is_active"
        case isApproved = "is_approved"
        case isRequestDelegate = "is_request_delegate"
        case lang
        case totalKilometers = "total_kilometers"
        case walletBalance = "wallet_balance"
        case type, location, city, bank
        case isAvaliable = "is_avaliable"
        case rate
    }
}

// MARK: - Bank
struct Bank: Codable {
    var name, user, account, iban: String?
}

// MARK: - Location
struct Location: Codable {
    var lat, lng: String?
    var address: String?
}
