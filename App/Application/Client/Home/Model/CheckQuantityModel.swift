//
//  ProductCell.swift
//  App
//
//  Created by Abdo Emad on 04/06/2024.
//

import Foundation

struct CheckQuantityModel: Codable {
    
    var quantity: Int?
    var maxPurchases: Int?
    
    
    enum CodingKeys: String, CodingKey {
        case quantity
        case maxPurchases = "max_purchases"
    }
    
}
