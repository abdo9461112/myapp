//
//  ServicesDetailsCell.swift
//  App
//
//  Created by Abdo Emad on 12/06/2024.
//

import UIKit

class ServicesDetailsCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(_ data : Attachment){
        imageView.setWith(data.path)
    }
}
