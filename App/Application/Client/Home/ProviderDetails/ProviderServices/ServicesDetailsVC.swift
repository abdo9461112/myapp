//
//  ServicesDetailsVC.swift
//  App
//
//  Created by Abdo Emad on 09/06/2024.
//

import UIKit



class ServicesDetailsVC: BaseViewController {
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var serviceDescription: UITextView!
    @IBOutlet weak var collectionView: UICollectionView!
    private var imageModel : [Attachment] = []
    private var isAvaliable: Bool?
    var serviceId : Int?
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        getServices(id: serviceId ?? 0 )
    }
    
    private func setUp(){
        addBackButtonWith(title: "Description".localized, color: .white)
        registerCollectionView()
    }
    
    private func registerCollectionView(){
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellType: ServicesDetailsCell.self)
    }
    @IBAction func bookButtonTapped(_ sender: UIButton) {
        addToCart(serviceId: serviceId ?? 0 )
    }
    
}



//MARK: - CollectionVeiw

extension ServicesDetailsVC:  UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        imageModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: ServicesDetailsCell.self, for: indexPath)
        cell.setData(imageModel[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
    
}

//MARK: - Networking


extension ServicesDetailsVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    private func getServices(id:Int){
        Task{
            showIndicator()
            let endpoint = ServicesEndpoints.clientService(serviceId:id)
            do{
                guard let response = try await request(endpoint)else{return}
                imageModel = response.data?.attachments ?? []
                if Language.isRTL(){
                    serviceName.text = response.data?.nameEn
                }else{
                    serviceName.text = response.data?.nameAr
                }
                serviceImage.setWith(response.data?.attachments?.first?.path)
                servicePrice.text = "\(response.data?.price ?? 0)"
                serviceDescription.text = response.data?.description
                isAvaliable = response.data?.isAvaliable
                collectionView.reloadData()
                hideIndicator()
            }catch{
                
            }
        }
    }
    private func addToCart(serviceId:Int){
        Task{
            showIndicator()
            let endpoint = CartEndpoints.addServiceToCart(id: serviceId)
            do{
                guard let response = try await request(endpoint)else{return}
                if response.key == .success{
                    presentSuccessVC()
                }else{
                    show(errorMessage: response.message)
                }
                hideIndicator()
            }catch{
                
            }
        }
    }
}

//MARK: -  pop back to services view
extension ServicesDetailsVC: SuccessProtocol{
    func popOut() {
        pop()
    }
    
    private func presentSuccessVC(){
        let vc = SuccessVC.create(delegate: self, 
                                  title: "The service has been successfully added to the cart.".localized,isModalInPresentation: true)
        present(vc, animated: true)
    }
}


