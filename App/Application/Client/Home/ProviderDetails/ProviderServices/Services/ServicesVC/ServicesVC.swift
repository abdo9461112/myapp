//
//  ProviderServices.swift
//  App
//
//  Created by Abdo Emad on 03/06/2024.
//

import UIKit
import SnapKit

class ServicesVC: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchField: UITextField!
    
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    var providerId: Int?
    var searchWorkItem : DispatchWorkItem?
    private var serviceModle : [ProviderProductsDataModel] = [ ]{
        didSet{
            collectionView.reloadData()
            if serviceModle.isEmpty{
                emptyMessage.text = "No Services"
                emptyMessage.isHidden = false
            }else{
                emptyMessage.isHidden = true
            }
        }
    }
    private var emptyMessage: UILabel = {
        let lable = UILabel()
        lable.font = UIFont.systemFont(ofSize: 28, weight: .semibold)
        lable.textColor = .newRed
        return lable
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    
    private func setUp(){
        view.addSubview(emptyMessage)
        emptyMessage.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        addBackButtonWith(title: "Services".localized,color: .white)
        collectionView.delegate = self
        collectionView.dataSource = self
        searchField.delegate = self
        collectionView.register(cellType: ServicesCollectionViewCell.self)
        getProviderServices(id: providerId!, filter: nil)
    }
    
    
}

//MARK: - CollectionView

extension ServicesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return serviceModle.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: ServicesCollectionViewCell.self, for: indexPath)
        let item = serviceModle[indexPath.row]
        cell.configureServiceCell(data: item)
        cell.addToFav = {[weak self] in
            guard let self = self else{return}
            guard let id = item.id else {return}
            addToFavourite(serviceId: id)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let id = serviceModle[indexPath.row].id
        let vc = ServicesDetailsVC()
        vc.serviceId = id
        push(vc)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.width - 24) / 2, height: 170)
    }
}

//MARK: - NetWorking

extension ServicesVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getProviderServices(id:Int,filter:String?){
        showIndicator()
        Task{
            let endpoint = ServicesEndpoints.getProviderServices(id: id, filter: filter)
            do{
                guard let response = try await request(endpoint)else{return}
                serviceModle = response.data?.data ?? []
                hideIndicator()
            }catch{
                print("error getting provider services api : \(error.localizedDescription)")
                
            }
        }
    }
    
    private func addToFavourite(serviceId:Int){
        Task{
            let endpoint = ServicesEndpoints.addServiceToFav(serviceId: serviceId)
            do{
                guard let _ = try await request(endpoint)else{return}
                print("added to fav")
            }catch{
                print("error adding products api : \(error.localizedDescription)")

            }
        }
    }
    
}

//MARK: - Textfield

extension ServicesVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        let newText = (currentText as NSString).replacingCharacters(in: range, with: string)
        searchWorkItem?.cancel()
        searchWorkItem = DispatchWorkItem { [weak self] in
            self?.performSearch(text: newText)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: searchWorkItem!)
        
        return true
    }
    
   private func performSearch(text: String) {
        if !text.isEmpty {
            let matchedService = serviceModle.contains { $0.name!.lowercased().contains(text.lowercased())
            }
            if !matchedService {
                serviceModle.removeAll()
                collectionView.reloadData()
                emptyMessage.text = "No Services".localized
            }
        }
        getProviderServices(id: providerId!, filter: text)
    }
  
    
}
