//
//  ServicesCollectionViewCell.swift
//  App
//
//  Created by Abdo Emad on 17/06/2024.
//


import UIKit


class ServicesCollectionViewCell:
    UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var servicePrice: UILabel!
    var addToFav: (() -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        favButton.setImage(UIImage(resource: .fav), for: .selected)
        favButton.setImage(UIImage(resource: .unFav), for: .normal)
    }
    
    func setData(data : ProviderProductsDataModel){
        if Language.isRTL(){
            title.text = data.nameAr
        }else{
            title.text = data.nameEn
        }
        imageView.setWith(data.attachments?.first?.path)
        favButton.isSelected = data.isFavorite ?? false
    }
    
    func configureServiceCell(data: ProviderProductsDataModel) {
        title.text = data.name
        favButton.isSelected = data.isFavorite ?? false
        imageView.setWith(data.attachments?.first?.path)
    }
    

    @IBAction func favButtonTapped(_ sender: UIButton) {
        sender.isSelected.toggle()
        addToFav?()

    }
}
