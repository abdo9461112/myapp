//
//  HomeProvidersCollectionViewCell.swift
//  App
//
//  Created by MGAbouarab on 06/01/2024.
//

import UIKit
import Cosmos
class HomeProvidersCollectionViewCell: UICollectionViewCell {

    //MARK: - IBOutlets -
    @IBOutlet weak var providerImage: UIImageView!
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var condtionLable: UILabel!
    @IBOutlet weak var providerRate: CosmosView!
    
    //MARK: - Lifecycle -
    override func awakeFromNib() {
        super.awakeFromNib()
        providerRate.settings.updateOnTouch = false 
    }
    
  
    
    //MARK: - Data -
   
    func setupData(_ data: Category){
        providerImage.setWith(data.image)
        providerName.text = data.name
        if data.isAvaliable == true{
            condtionLable.text = "Avaliable".localized
            condtionLable.textColor = .green
        }else{
            condtionLable.text = "Not Avaliable".localized
            condtionLable.textColor = .red
        }
    }
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}
