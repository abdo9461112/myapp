//
//  HomeTopSliderCollectionViewCell.swift
//  App
//
//  Created by Abdo Emad on 29/05/2024.
//

import UIKit

class HomeTopSliderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        layer.cornerRadius = .cardCorner
    }

    func setupData(_ data: Slider){
        imageView.setWith(data.image)
    }
}
