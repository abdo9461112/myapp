//
//  HomeCategoryCollectionViewCell.swift
//  App
//
//  Created by Abdo Emad on 10/06/2024.
//

import UIKit

class HomeCategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLable: UILabel!
    
    
    //MARK: - Lifecycle -
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupInitialDesign()
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.clipsToBounds = true
    }
    
    //MARK: - Data -
   
    func setupData(_ data: Category){
        imageView.setWith(data.image)
        titleLable.text = data.name
    }
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}
