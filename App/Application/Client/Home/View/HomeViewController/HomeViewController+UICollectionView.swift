import UIKit

// MARK: - CollectionView -
extension HomeViewController {
    // MARK: - Properties -
    private var headerKind: String { UICollectionView.elementKindSectionHeader }
    private var headerHeight: CGFloat { 30 }
    
    func setupCollectionView() {
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.showsVerticalScrollIndicator = false
        self.collectionView.backgroundColor = .clear
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
//        self.collectionView.register(cellType: HomeTopSliderCollectionViewCell.self)
        self.collectionView.register(cellType: HomeCategoryCollectionViewCell.self)
        self.collectionView.register(cellType: HomeProvidersCollectionViewCell.self)
        self.collectionView.register(CollectionViewHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: CollectionViewHeader.className)
        self.collectionView.contentInset = .init(top: 20, left: 0, bottom: 20, right: 0)
        
        let layout = UICollectionViewCompositionalLayout { sectionIndex, environment in
            switch sectionIndex {
            case 0:
                return self.categoriesSection()
            default:
                return self.serviceProvidersSection()
            }
        }
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    private func createHeader() -> NSCollectionLayoutBoundarySupplementaryItem {
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(headerHeight))
        let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: headerKind, alignment: .top)
        return header
    }
    
    
    private func categoriesSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .estimated(100), heightDimension: .estimated(120))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .estimated(14), heightDimension: .estimated(14))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        group.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 15)
        
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 15, bottom: 10, trailing: 0)
        section.orthogonalScrollingBehavior = .continuous
        section.boundarySupplementaryItems = [createHeader()]
        
        return section
    }
    
    private func serviceProvidersSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.85), heightDimension: .absolute(225))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        group.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 15)
        
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 15, bottom: 10, trailing: 0)
        section.orthogonalScrollingBehavior = .continuous
        section.boundarySupplementaryItems = [createHeader()]
        
        return section
    }
}

extension HomeViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return categories.count
        case 1:
            return mostRates.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(with: HomeCategoryCollectionViewCell.self, for: indexPath)
            cell.setupData(categories[indexPath.row])
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(with: HomeProvidersCollectionViewCell.self, for: indexPath)
            cell.setupData(mostRates[indexPath.row])
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard kind == UICollectionView.elementKindSectionHeader else { return UICollectionReusableView() }
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: CollectionViewHeader.className, for: indexPath) as! CollectionViewHeader
        
        if indexPath.section == 0 {
            header.set(title: "Categories".localized)
        } else if indexPath.section == 1 {
            header.set(title: "Service Providers".localized)
        }
        
        return header
    }
}

extension HomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            guard let id = categories[indexPath.row].id else { return }
            guard let name = categories[indexPath.row].name else { return }
           let vc = ProviderCategoriesSearchVC()
            vc.categoryId = id
            vc.categoryName = name
            vc.hidesBottomBarWhenPushed = true
            push(vc)
        case 1:
            guard let id = mostRates[indexPath.row].id else { return }
            let vc = ProviderDetailsVC()
            vc.providerId = id
            vc.hidesBottomBarWhenPushed = true
            push(vc)
        default:
            let vc = HomeSearchVC()
            vc.hidesBottomBarWhenPushed = true
            push(vc)
        }
    }
}
