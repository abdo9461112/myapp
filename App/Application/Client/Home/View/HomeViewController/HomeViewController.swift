//
//  HomeViewController.swift
//  App
//
//  Created by MGAbouarab on 30/12/2023.
//


import UIKit
import ImageSlideshow

class HomeViewController: BaseViewController {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private(set) var collectionView: UICollectionView!
    @IBOutlet weak var imageSlider: ImageSlideshow!
     
    //MARK: - Properties -
    private lazy var navigationTitleView: HomeNavigationView = HomeNavigationView()
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    var sliders: [Slider] = []
    var categories: [Category] = []
    var mostRates: [Category] = []
    private var notificationModel: [NotificationsDataModel] = []

    //MARK: - Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationDesign()
        self.setupCollectionView()
        callApi()
        configImageSlider()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationTitleView.set(address: UserDefaults.locationPicked ?? "")
        (self.navigationController as? ColoredNav)?.changeApperance(to: .light)
        checkNotifications()
    }
    
    //MARK: - Design -
    private func setupNavigationDesign() {
        self.addNavigationTitleView()
    }

    private func addNavigationTitleView() {
        self.navigationItem.titleView = self.navigationTitleView
        self.navigationTitleView.selectAddressAction = { [weak self] in
            guard let self = self else { return }
            let vc = GoogleMapVC.create(type: .home)
            vc.hidesBottomBarWhenPushed = true
            self.push(vc)
        }
        self.navigationTitleView.selectNotificationAction = { [weak self] in
            guard let self = self else { return }
            let vc = NotificationsVC()
            vc.hidesBottomBarWhenPushed = true
            self.push(vc)
        }
        self.navigationTitleView.selectSearchAction = { [weak self] in
            guard let self = self else { return }
            let vc = HomeSearchVC()
            vc.hidesBottomBarWhenPushed = true
            self.push(vc)
        }
        
        if !UserDefaults.isLogin {
            navigationTitleView.addressContainerView.isHidden = true
            navigationTitleView.setNotificationButton(isHidden: true)
        }
    }
     
    private func configImageSlider() {
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = .main
        pageControl.pageIndicatorTintColor = .lightGray
        imageSlider.pageIndicator = pageControl
        imageSlider.slideshowInterval = 3
        imageSlider.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        imageSlider.contentScaleMode = .scaleAspectFill
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        imageSlider.addGestureRecognizer(recognizer)
    }

    @objc func didTap() {
        imageSlider.presentFullScreenController(from: self)
    }
     
    private func configureSlider(_ models: [Slider]) {
        let kingfisherSources: [KingfisherSource] = models.compactMap { slider in
            guard let image = slider.image, !image.isEmpty,
                  let encodedURLString = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                  let url = URL(string: encodedURLString) else {
                return nil
            }
            return KingfisherSource(url: url, placeholder: UIImage(named: "placeholder"))
        }

        self.imageSlider.setImageInputs(kingfisherSources)
    }
    
}

//MARK: - API Calls -
extension HomeViewController {
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }

    private func callApi() {
        showIndicator()
        let endpoint = HomeEndPoints.getHomeSliders()
        Task {
            do {
                guard let response = try await request(endpoint) else { return }
                sliders = response.data?.sliders ?? []
                configureSlider(sliders)
                categories = response.data?.categories ?? []
                mostRates = response.data?.mostRates ?? []
                collectionView.reloadData()
                hideIndicator()
            } catch {
                print("error calling home sliders API: \(error.localizedDescription)")
            }
        }
    }
    //MARK: - Notifications -
    private func checkNotifications() {
        getNotifications { [weak self] hasNotifications in
            guard let self = self else { return }
            self.navigationTitleView.setNotificationButton(hasNotification: hasNotifications)
        }
    }

    private func getNotifications(completion: @escaping (Bool) -> Void) {
        Task {
            let endpoint = HomeEndPoints.getNotifications()
            do {
                guard let response = try await request(endpoint) else { return }
                notificationModel = response.data?.notifications?.data ?? []
                let hasNotifications = !notificationModel.isEmpty
                completion(hasNotifications)
            } catch {
                print("Error calling notifications API: \(error.localizedDescription)")
                completion(false)
            }
        }
    }

}
