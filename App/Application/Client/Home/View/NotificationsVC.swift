//
//  NotificationsVC.swift
//  App
//
//  Created by Abdo Emad on 10/06/2024.
//

import UIKit
import SnapKit

class NotificationsVC: BaseViewController {
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    private var notificationModel : [NotificationsDataModel] = [] {
        didSet {
            tableView.reloadData()
            if notificationModel.isEmpty{
                emptyMessage.isHidden = false
            }else{
                emptyMessage.isHidden = true
            }
        }
    }
    
    private let emptyMessage: UILabel = {
        let label = UILabel()
        label.textColor = .main
        label.font = UIFont.systemFont(ofSize: 20)
        label.text = "No Notifications".localized
        label.isHidden = true
        return label
    }()
    
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        getNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as? ColoredNav)?.changeApperance(to: .colored)
    }
    
    private func setUp() {
        addBackButtonWith(title: "Notifications".localized, color: .white)
        view.addSubview(emptyMessage)
        emptyMessage.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: NotificationsCell.self)
    }
    
    @IBAction func deleteAllNotifcButtTapped(_ sender: UIButton) {
        deleteAllNotifications()
    }
}

//MARK: - TableView
extension NotificationsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: NotificationsCell.self, for: indexPath)
        cell.confgData(data: notificationModel[indexPath.row])
        cell.onDelete = { [weak self] in
            guard let self = self else { return }
            guard let id = self.notificationModel[indexPath.row].id else { return }
            self.deleteOneNotification(id: id)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notfication = self.notificationModel[indexPath.row]
        if notfication.data?.orderType == "normal" {
            guard let id = notfication.data?.orderID  else {return}
            let vc = OrdersDetailsVC.create(orderId: id)
            push(vc)
        } else{
            guard let id = notfication.data?.orderID  else {return}
            let vc = SpecialOrderDetailsVC.create(orderId: id)
            push(vc)
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

//MARK: - Networking
extension NotificationsVC {
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getNotifications() {
        Task {
            showIndicator()
            let endpoint = HomeEndPoints.getNotifications()
            do {
                guard let response = try await request(endpoint) else { return }
                hideIndicator()
                notificationModel = response.data?.notifications?.data ?? []
            } catch {
                print("Error calling notifications API: \(error.localizedDescription)")
            }
        }
    }
    
    // MARK: - Networking
    private func deleteOneNotification(id: String) {
        Task {
            showIndicator()
            let endpoint = HomeEndPoints.deleteOneNotifications(id: id)
            do {
                guard let response = try await request(endpoint) else { return }
                hideIndicator()
                show(successMessage: response.message)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.getNotifications()
                }
            } catch {
                print("Error calling delete notification API: \(error.localizedDescription)")
            }
        }
    }
    
    private func deleteAllNotifications() {
        Task {
            let endpoint = HomeEndPoints.deleteNotifications()
            do {
                guard let response = try await request(endpoint) else { return }
                if !notificationModel.isEmpty{
                    show(successMessage: response.message)
                    tableView.reloadData()
                    pop()
                }
                
            } catch {
                print("Error calling delete notification API: \(error.localizedDescription)")
            }
        }
    }
}
