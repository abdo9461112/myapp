//
//  HomeFilterVC.swift
//  App
//
//  Created by Abdo Emad on 10/06/2024.
//

import UIKit
import SnapKit

class HomeSearchVC: BaseViewController {

    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var searchWorkItem: DispatchWorkItem?
    var providerModel: [ProviderModel] = [ ] {
        didSet{
            collectionView.reloadData()
            if providerModel.isEmpty{
                emptyLable.text = "NO PROVIDERS"
                emptyLable.isHidden = false
            }else{
                emptyLable.isHidden = true
            }
        }
    }
    
   private let emptyLable : UILabel = {
        let lable = UILabel()
       lable.textColor = .newRed
       lable.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        return lable
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        (self.navigationController as? ColoredNav)?.changeApperance(to: .colored)
    }
    private func setUp(){
        addBackButtonWith(title: "Search".localized, color: .white)
        collectionView.delegate = self
        collectionView.dataSource = self
        searchField.delegate = self
        collectionView.register(cellType: ProviderCell.self)
        view.addSubview(emptyLable)
        emptyLable.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
       callApi()
    }
    
    
    @IBAction func filterButtonTapped(_ sender: UIButton) {
        let vc = FilterVC()
        vc.delegate = self
        guard let sheet = vc.sheetPresentationController else{return}
        if #available(iOS 16.0, *) {
            let customDetent = UISheetPresentationController.Detent.custom { context in
                return 400
            }
            sheet.detents = [customDetent]
            sheet.preferredCornerRadius = 25
            present(vc, animated: true)
        }
    }
}

//MARK: - CollectionView

extension HomeSearchVC: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        providerModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: ProviderCell.self, for: indexPath)
        cell.setupData(providerModel[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let id = providerModel[indexPath.row].id
        let vc = ProviderDetailsVC()
        vc.providerId = id
        push(vc)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width - 30) / 2
        return CGSize(width: width, height: 200)
    }
    
    
    
}

//MARK: - Networking

extension HomeSearchVC{
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    private func callApi(){
        DispatchQueue.main.async {
            self.getProviders(category_id: nil, name: nil, bestSell: nil, distance: nil, new: nil, rate: nil)
        }

    }
    
    private func getProviders(category_id: Int?, name: String?, bestSell: Int?, distance: Int?, new: Int?, rate: String?){
        showIndicator()
        Task{
            let endpoint = HomeEndPoints.clientSearch(category_id: category_id, name: name, bestSell: bestSell, distance: distance, new: new, rate: rate)
            do{
                guard let response = try await request(endpoint)else{return}
                providerModel = response.data?.data ?? [ ]
                hideIndicator()
            }catch{
                print("error calling providers api : \(error.localizedDescription)")
            }
        }
    }
}




extension HomeSearchVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        let newText = (currentText as NSString).replacingCharacters(in: range, with: string)
        searchWorkItem?.cancel()
        searchWorkItem = DispatchWorkItem { [weak self] in
            self?.performSearch(text: newText)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: searchWorkItem!)
        
        return true
    }
    
    func performSearch(text: String) {
        if !text.isEmpty {
            let matchedProvider = providerModel.contains { $0.name!.lowercased().contains(text.lowercased())
            }
            if !matchedProvider {
                providerModel.removeAll()
                collectionView.reloadData()
                emptyLable.text = "NO PROVIDERS".localized
            }
        }else {
            callApi()
        }
        self.getProviders(category_id: nil, name: text, bestSell: nil, distance: nil, new: nil, rate: nil)
    }
}


extension HomeSearchVC: FilterProvidersProtocol{
    func filterProvidersProtocol(categoriID: Int?, close: Int?, bestSeller: Int?, new: Int?) {
        providerModel = []
        getProviders(category_id: categoriID, name: nil, bestSell: bestSeller, distance: close, new: new, rate: nil)
    }
}
