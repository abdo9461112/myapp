//
//  FilterVC.swift
//  App
//
//  Created by Abdo Emad on 10/06/2024.
//

import UIKit

class FilterVC: BaseViewController {

    
    @IBOutlet weak var closesetButton: UIButton!
    @IBOutlet weak var mostRatesButton: UIButton!
    @IBOutlet weak var newbutton: UIButton!
    @IBOutlet weak var categoriesField: DropDownTextField!
    
    private var categoriID: Int?
    private var closeKey: Int?
    private var mostRatesKey: Int?
    private var newKey: Int?
    var categoriesModel : [CategoriesModel] = []
    var delegate: FilterProvidersProtocol?
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCategories()
        categoriesField.dropDownDelegate = self
    }

    @IBAction func closestButtonTaped(_ sender: UIButton) {
        sender.isSelected.toggle()
        if sender.isSelected {
            closeKey = 1
            closesetButton.setImage(.selectedFilter, for: .normal)
        }else{
            closeKey = nil
            closesetButton.setImage(.unSelectedFilter, for: .normal)

        }
    }
    
    @IBAction func mostRatesButtonaped(_ sender: UIButton) {
        sender.isSelected.toggle()
        if sender.isSelected {
            mostRatesKey = 1
            mostRatesButton.setImage(.selectedFilter, for: .normal)
        }else{
            mostRatesKey = nil
            mostRatesButton.setImage(.unSelectedFilter, for: .normal)

        }
    }
    
    @IBAction func newButtonaped(_ sender: UIButton) {
        sender.isSelected.toggle()
        if sender.isSelected {
            newKey = 1
            newbutton.setImage(.selectedFilter, for: .normal)
        }else{
            newKey = nil
            newbutton.setImage(.unSelectedFilter, for: .normal)

        }
    }
    
    @IBAction func searchButtonTaped(_ sender: UIButton) {
        guard let category = categoriesField.text, !category.isEmpty else {
            show(errorMessage: "You must enter the category first".localized)
            return
        }
        dismiss(animated: true) { [weak self] in
            guard let self = self else{return}
            delegate?.filterProvidersProtocol(categoriID: categoriID, close: closeKey, bestSeller: mostRatesKey, new: newKey)
        }
    }
}


//MARK: - Networking

extension FilterVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getCategories(){
        showIndicator()
        Task{
            let endpoint = HomeEndPoints.getCategories()
            do{
                guard let response = try await request(endpoint)else{return}
                categoriesModel = response.data ?? []
                hideIndicator()
            }catch{
                print("error calling categories api \(error.localizedDescription)")
            }
        }
    }
}

//MARK: - Drop down

extension FilterVC: DropDownTextFieldDelegate{
    func dropDownList(for textField: UITextField) -> [any DropDownItem] {
        categoriesModel
    }
    
    func didSelect(item: any DropDownItem, for textField: UITextField) {
        categoriID = item.id
        categoriesField.text = item.name
    }
    
}





