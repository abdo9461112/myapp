//
//  NotificationsCell.swift
//  App
//
//  Created by Abdo Emad on 13/06/2024.
//

import UIKit

class NotificationsCell: UITableViewCell {

    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var messageLable: UILabel!
    @IBOutlet weak var createdAtLable: UILabel!
    var onDelete :(()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    
    func confgData(data : NotificationsDataModel){
        titleLable.text = data.title
        messageLable.text = data.body
        createdAtLable.text = data.createdAt
    }
    @IBAction func deleteButton(_ sender: UIButton) {
        onDelete?()
    }
}
