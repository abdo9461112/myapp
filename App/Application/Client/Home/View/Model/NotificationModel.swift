//
//  NotificationModel.swift
//  App
//
//  Created by Abdo Emad on 13/06/2024.
//


// MARK: - NotificationModel
struct NotificationModel: Codable {
    var notifications: NotificationsData?
}

// MARK: - Notifications
struct NotificationsData: Codable {
    var pagination: Pagination?
    var data: [NotificationsDataModel]?
}

// MARK: - NotificationsDataModel
struct NotificationsDataModel: Codable {
    var id, type: String?
    var title: String?
    var body: String?
    var data: NotificationContent?
    var createdAt: String?

    enum CodingKeys: String, CodingKey {
        case id, type, title, body, data
        case createdAt = "created_at"
    }
}

// MARK: - NotificationContent
struct NotificationContent: Codable {
    var type: String?
    var orderID: Int?
    var orderType :String?

    enum CodingKeys: String, CodingKey {
        case type
        case orderID = "order_id"
        case orderType = "order_type"
    }
}


// MARK: - Pagination
struct Pagination: Codable {
    var totalItems, countItems, perPage, totalPages: Int?
    var currentPage: Int?
    var nextPageURL, pervPageURL: String?

    enum CodingKeys: String, CodingKey {
        case totalItems = "total_items"
        case countItems = "count_items"
        case perPage = "per_page"
        case totalPages = "total_pages"
        case currentPage = "current_page"
        case nextPageURL = "next_page_url"
        case pervPageURL = "perv_page_url"
    }
}
