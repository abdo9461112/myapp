//
//  HomeNavigationView.swift
//  App
//
//  Created by MGAbouarab on 06/01/2024.
//

import UIKit

class HomeNavigationView: UIView {
    
    //MARK: - IBOutlets -
    @IBOutlet weak  var addressContainerView: UIView!
    @IBOutlet weak private var addressLabel: UILabel!
    @IBOutlet weak private var notificationButton: UIButton!
    
    //MARK: - Properties -
    var selectAddressAction: (()->())?
    var selectNotificationAction: (()->())?
    var selectSearchAction: (()->())?

    //MARK: - Initializer -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "HomeNavigationView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.tintColor = .main
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.addressViewTapped))
        self.addressContainerView.addGestureRecognizer(tap)
    }
    
    //MARK: - Data -
    func set(address: String) {
        self.addressLabel.text = address
    }
    func setNotificationButton(isHidden: Bool) {
        self.notificationButton.isHidden = isHidden
    }
    func setNotificationButton(hasNotification: Bool) {
        let image = hasNotification ? UIImage(named: "notificationBadge") : UIImage(named: "notificationLogo")
        self.notificationButton.setImage(image, for: .normal)
    }
    
    //MARK: - Actions -
    @objc private func addressViewTapped() {
        self.selectAddressAction?()
    }
    @IBAction private func notificationButtonPressed() {
        self.selectNotificationAction?()
    }
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        self.selectSearchAction?()
    }
    
    
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
}
