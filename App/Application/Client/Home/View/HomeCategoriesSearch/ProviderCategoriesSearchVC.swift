
//
//  ProviderCategoriesVC.swift
//  App
//
//  Created by Abdo Emad on 05/06/2024.
//

import UIKit
import SnapKit

protocol FilterProvidersProtocol {
    func filterProvidersProtocol(categoriID: Int?, close: Int?, bestSeller: Int?, new: Int?)
}

class ProviderCategoriesSearchVC: BaseViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchField: UITextField!
    
    var categoryName: String?
    var categoryId: Int?
    var searchWorkItem: DispatchWorkItem?
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    private var providersModel: [ProviderModel] = [] {
        didSet{
            if providersModel.count != 0 {
                emptyProviderLabel.isHidden = true
            }else{
                emptyProviderLabel.isHidden = false
                emptyProviderLabel.text = "NO PROVIDERS".localized
            }
            collectionView.reloadData()
        }
    }
    
    private var emptyProviderLabel: UILabel = {
        let lable = UILabel()
        lable.textColor = .newRed
        lable.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        return lable
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        confgCollectionView()
    }

    override func viewWillAppear(_ animated: Bool) {
        (self.navigationController as? ColoredNav)?.changeApperance(to: .colored)
    }
    
    private func setUp(){
        addBackButtonWith(title: categoryName ?? "Back".localized, color: .white)
        searchField.delegate = self
        view.addSubview(emptyProviderLabel)
        emptyProviderLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        callApi()
    }
    
    private func confgCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(cellType: ProviderCell.self)
    }

    @IBAction func filterButtonTaped(_ sender: Any) {
        let vc = ProviderFilterVC()
        vc.delegate = self
        guard let sheet = vc.sheetPresentationController else{return}
        if #available(iOS 16.0, *) {
            let customDetent = UISheetPresentationController.Detent.custom { context in
                return 350
            }
            sheet.detents = [customDetent]
            sheet.preferredCornerRadius = 25
            sheet.prefersGrabberVisible = true
            present(vc, animated: true)
        }
    }
}

//MARK: - CollectionVeiw
extension ProviderCategoriesSearchVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        providersModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: ProviderCell.self, for: indexPath)
        cell.setupData(providersModel[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let id = providersModel[indexPath.row].id
        let vc = ProviderDetailsVC()
        vc.providerId = id
        push(vc)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width - 30) / 2
        return CGSize(width: width, height: 200)
    }
}


//MARK: -  NetWorking

extension ProviderCategoriesSearchVC{
    
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<BaseResponse<ResponseData>>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> BaseResponse<ResponseData>? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    private func callApi() {
        DispatchQueue.main.async {
            self.getProviders(category_id: self.categoryId , name: nil, bestSell: nil, distance: nil, new: nil, rate: nil)
        }
    }
    
    private func getProviders(category_id: Int?, name: String?, bestSell: Int?, distance: Int?, new: Int?, rate: String?){
        showIndicator()
        Task{
            let endpoint = HomeEndPoints.clientSearch(category_id: category_id, name: name, bestSell: bestSell, distance: distance, new: new, rate: rate)
            do{
                guard let response = try await request(endpoint) else{return}
                providersModel = response.data?.data ?? [ ]
                hideIndicator()
            }catch{
                
                print("Error calling providers api : \(error.localizedDescription)")
            }
        }
    }
}


//MARK: - TextField

extension ProviderCategoriesSearchVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        let newText = (currentText as NSString).replacingCharacters(in: range, with: string)
        searchWorkItem?.cancel()
        searchWorkItem = DispatchWorkItem { [weak self] in
            self?.performSearch(text: newText)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: searchWorkItem!)
        
        return true
    }
    
    func performSearch(text: String) {
        if !text.isEmpty {
            let matchedProvider = providersModel.contains { $0.name!.lowercased().contains(text.lowercased())
            }
            if !matchedProvider {
                providersModel.removeAll()
                collectionView.reloadData()
                emptyProviderLabel.text = "NO PROVIDERS".localized
            }
        }else {
            callApi()
        }
        self.getProviders(category_id: categoryId ?? nil, name: text, bestSell: nil, distance: nil, new: nil, rate: nil)
    }
}


extension ProviderCategoriesSearchVC: FilterProvidersProtocol{
    func filterProvidersProtocol(categoriID: Int?, close: Int?, bestSeller: Int?, new: Int?) {
        providersModel = []
        getProviders(category_id: self.categoryId, name: nil, bestSell: bestSeller, distance: close, new: new, rate: nil)
    }
}

