//
//  ProviderFilterVC.swift
//  App
//
//  Created by Abdo Emad on 05/06/2024.
//

import UIKit



class ProviderFilterVC: BaseViewController {

    @IBOutlet weak var closesetButton: UIButton!
    @IBOutlet weak var bestSellerButton: UIButton!
    @IBOutlet weak var newButton: UIButton!
    
    private var closeKey: Int?
    private var bestSellerKey: Int?
    private var newKey: Int?
    var delegate: FilterProvidersProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func ClosestButtonTaped(_ sender: UIButton) {
        sender.isSelected.toggle()
        if sender.isSelected {
            closeKey = 1
            closesetButton.setImage(.selectedWay, for: .normal)
        }else{
            closeKey = nil
            closesetButton.setImage(.unSelectedFilter, for: .normal)

        }
    }
    
    @IBAction func bestSellerButtonTaped(_ sender: UIButton) {
        sender.isSelected.toggle()
        if sender.isSelected {
            bestSellerKey = 1
            bestSellerButton.setImage(.selectedWay, for: .normal)
        }else{
            bestSellerKey = nil
            bestSellerButton.setImage(.unSelectedFilter, for: .normal)

        }
    }
    
    @IBAction func newButtonTaped(_ sender: UIButton) {
        sender.isSelected.toggle()
        if sender.isSelected {
            newKey = 1
            newButton.setImage(.selectedWay, for: .normal)
        }else{
            newKey = nil
            newButton.setImage(.unSelectedFilter, for: .normal)

        }
    }
    
    @IBAction func searchButtonTaped(_ sender: UIButton) {
        guard newKey == 1 || closeKey == 1 || bestSellerKey == 1 else{
            show(errorMessage: "You must selcet a key first".localized)
            return
        }
        dismiss(animated: true) { [weak self] in
            guard let self = self else{return}
            delegate?.filterProvidersProtocol(categoriID: nil, close: closeKey, bestSeller: bestSellerKey, new: newKey)
        }
    }
}
