//
//  PaymentTableViewCell.swift
//  App
//
//  Created by Abdo Emad on 12/06/2024.
//

import UIKit

class PaymentTableViewCell: UITableViewCell {

    @IBOutlet weak var paymentImage: UIImageView!
    @IBOutlet weak var paymentName: UILabel!
    @IBOutlet weak var selcted: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setData(_ data : PaymentMethodModel){
        paymentImage.setWith(data.image)
        paymentName.text = data.name
        if data.isSelected{
            selcted.image = UIImage(resource: .selectedFilter)
        }else{
            selcted.image = UIImage(resource: .unSelectedFilter)
        }
    }
  
}
