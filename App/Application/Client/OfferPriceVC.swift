//
//  OfferPriceVC.swift
//  App
//
//  Created by Abdo Emad on 13/06/2024.
//

import UIKit

class OfferPriceVC: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private let responseHandler: ResponseHandler = DefaultResponseHandler()
    private var orderId: Int?
    private var selectedOfferId: Int?
    private var selectedOfferPrice: String?
    private var offerModel: [OfferPricesDataModel] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        if let orderId = orderId {
            getOffers(orderId: orderId)
        }
    }
    
    private func setUp() {
        addBackButtonWith(title: "Quotations".localized, color: .white)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: OfferPriceCell.self)
    }
    
    static func create(orderId: Int) -> OfferPriceVC {
        let vc = OfferPriceVC()
        vc.orderId = orderId
        return vc
    }
}

// MARK: - TableView

extension OfferPriceVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = offerModel.count
        print("Number of rows: \(count)")
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: OfferPriceCell.self, for: indexPath)
        let offer = offerModel[indexPath.row]
        cell.setData(data: offer)
        cell.onAcceptingOffer = { [weak self] in
            guard let self = self else { return }
            self.selectedOfferId = offer.id
            self.selectedOfferPrice = String(offer.price ?? 0)
            self.presentSuccessView()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

// MARK: - Networking

extension OfferPriceVC {
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getOffers(orderId: Int) {
        Task {
            showIndicator()
            let endpoint = OrdersEndpoint.getOffers(orderId: orderId)
            do {
                guard let response = try await request(endpoint) else { return }
                hideIndicator()
                if let offers = response.data?.data {
                    print("Fetched offers: \(offers)")
                    DispatchQueue.main.async {
                        self.offerModel = offers
                    }
                } else {
                    print("No offers found.")
                }
            } catch {
                print("Error calling offers API: \(error.localizedDescription)")
                hideIndicator()
            }
        }
    }
}

// MARK: - Present Success View

extension OfferPriceVC: SuccessProtocol {
    func popOut() {
        let vc = PaymentVC.create(orderId: orderId!, totalPrice: selectedOfferPrice!, offerId: selectedOfferId ?? 0, type: .offer)
        push(vc)
    }
    
    func presentSuccessView() {
        let vc = SuccessVC.create(delegate: self, title: "Your quote has been successfully accepted".localized, titleButton: "Please pay the order value".localized)
        present(vc, animated: true)
    }
}
