//
//  OrdersDetailsModel.swift
//  App
//
//  Created by Abdo Emad on 14/06/2024.
//

import Foundation

// MARK: - OfferPricesModel
struct OfferPricesModel: Codable {
    var pagination: Pagination?
    var data: [OfferPricesDataModel]?
}

// MARK: - OfferPricesDataModel
struct OfferPricesDataModel: Codable {
    var id, price: Int?
    var notes: String?
    var provider: Provider?
    var status: String?
}


