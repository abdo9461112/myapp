//
//  OrdersCell.swift
//  App
//
//  Created by Abdo Emad on 13/06/2024.
//

import UIKit

class OrdersCell: UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var orderState: UILabel!
    @IBOutlet weak var timeLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configur(data: OrdersDataModel){
        userName.text = data.provider?.name
        userImage.setWith(data.provider?.image)
        timeLable.text = data.createdAt
        orderNumber.text = "Order number:" + "\(data.number ?? " ")"
        if data.status?.client?.rawValue == "received_from_delegate" {
            orderState.text = "Finished".localized
        }else {
            orderState.text = data.createStatus().localized
        }
    }
    func setData(_ data: OrdersDataModel){
        userName.text = data.user?.name
        userImage.setWith(data.user?.image)
        timeLable.text = data.createdAt
        orderNumber.text = "Order number:" + "\(data.number ?? " ")"
        orderState.text = data.createStatus().localized
    }
    
}
