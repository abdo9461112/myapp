//
//  OfferPriceCell.swift
//  App
//
//  Created by Abdo Emad on 15/06/2024.
//

import UIKit

class OfferPriceCell: UITableViewCell {

    @IBOutlet weak var providerImage: UIImageView!
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var acceptOfferBt: UIButton!
    
    var onAcceptingOffer : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        providerImage.layer.cornerRadius = providerImage.frame.height / 2
        selectionStyle = .none
        let underlineAttribute: [NSAttributedString.Key : Any] = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue, NSAttributedString.Key.foregroundColor: UIColor.main]
        
        let underLine = NSAttributedString(string: "Accept offer".localized, attributes: underlineAttribute)
        acceptOfferBt.setAttributedTitle(underLine, for: .normal)
    }

    func setData(data: OfferPricesDataModel){
        providerImage.setWith(data.provider?.image)
        providerName.text = data.provider?.name
        price.text = "\(data.price ?? 0)"
    }
    
    
    @IBAction func acceptOfferButtonTapped(_ sender: UIButton) {
        onAcceptingOffer?()
    }
}
