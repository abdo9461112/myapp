//
//  CartVC.swift
//  App
//
//  Created by Abdo Emad on 11/06/2024.
//

import UIKit
import SnapKit

class CartVC: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    
   private var cartModle : [CartDataModel] = [ ] {
        didSet{
            tableView.reloadData()
            if cartModle.isEmpty{
                emptyMessage.text = "Empty Cart".localized
                emptyMessage.isHidden = false
            }else{
                emptyMessage.isHidden = true
            }
        }
    }
    private let emptyMessage: UILabel = {
        let lable = UILabel()
        lable.textColor = .main
        lable.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        return lable
    }()
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    override func viewWillAppear(_ animated: Bool) {
        (self.navigationController as? ColoredNav)?.changeApperance(to: .colored)
        if UserDefaults.isLogin{
            getCarts()
        }else{
            showRequiredLoginAlert()
            emptyMessage.text = "Empty Cart".localized
        }
    }
    
    private func setUp(){
        view.addSubview(emptyMessage)
        setLeading(title: "Cart".localized, color: .white)
        emptyMessage.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        registerTableView()
    }
    private func registerTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: CartCell.self)
    }

}

//MARK: - TableView

extension CartVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        cartModle.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: CartCell.self, for: indexPath)
        cell.setData(data: cartModle[indexPath.row])
        cell.onDelete = {[ weak self] in
            guard let self = self else{return}
            guard let id = cartModle[indexPath.row].id else{return}
            self.deleteCart(cartId: id)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = cartModle[indexPath.row].id else{return}
        let vc = CartDetailsVC()
        vc.cartId = id
        vc.hidesBottomBarWhenPushed = true
        push(vc)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

//MARK: - NetWorking

extension CartVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<BaseResponse<ResponseData>>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> BaseResponse<ResponseData>? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getCarts(){
        showIndicator()
        Task{
            let endpoint = CartEndpoints.getClientCart()
            do{
                guard let response = try await request(endpoint)else{return}
                cartModle = response.data?.data ?? []
                hideIndicator()
            }catch{
                print("error calling carts api \(error.localizedDescription)")
            }
        }
    }
    private func deleteCart(cartId: Int){
        showIndicator()
        Task{
            let endpoint = CartEndpoints.deleteClientCart(cartId: cartId)
            do{
                guard let response = try await request(endpoint)else{return}
                show(successMessage: response.message)
                hideIndicator()
                getCarts()
            }catch{
                print("error deleting carts api \(error.localizedDescription)")
            }
        }
    }
}



