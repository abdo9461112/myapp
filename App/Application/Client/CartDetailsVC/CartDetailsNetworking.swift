//
//  CartDetailsNetworking.swift
//  App
//
//  Created by Abdo Emad on 12/06/2024.
//

import Foundation
import UIKit

//MARK: - Netwokring



extension CartDetailsVC{
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    func getCartDetails(id : Int){
        Task{
            showIndicator()
            let endpoint = CartEndpoints.getClientCartDetails(cartId: id)
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                proiderId = response.data?.cart?.provider?.id
                productModle = response.data?.cart?.items?.products ?? []
                serviceModle = response.data?.cart?.items?.services ?? []
                providerName.text = response.data?.cart?.provider?.name
                providerImage.setWith(response.data?.cart?.provider?.image)
                ordersNum.text = "\(response.data?.cart?.countOfItems ?? 0)"
                totalLable.text = "\(response.data?.financial?.total ?? 0) " + "SAR".localized
                taxLable.text = "\(response.data?.financial?.tax ?? 0) " + "SAR".localized
                deliveryPriceLable.text = "\(response.data?.financial?.deliveryCost ?? 0.0) " + "SAR".localized
                finalTotal.text = "\(response.data?.financial?.finalTotal ?? 0.0) " + "SAR".localized
                financialTotalValue = "\(response.data?.financial?.total ?? 0)"
                financialTaxValue = "\(response.data?.financial?.tax ?? 0)"
                appPrice = "\(response.data?.financial?.app ?? 0)"
                deliveryPriceValue = "\(response.data?.financial?.deliveryPrice ?? 0)"
                finalTotalPrice = "\(response.data?.financial?.finalTotal ?? 0)"
                deliveryCost = response.data?.financial?.deliveryCost
                delegateRate = response.data?.financial?.delegateRate
                getPaymentMethod()
            }catch{
                print("error calling cart details api \(error.localizedDescription)")
            }
        }
    }
    
    func increaseProductQuantity(cartId: Int, itemId: Int){
        Task{
            let endpoint = CartEndpoints.increaseProductQuantity(cartId: cartId, itemId: itemId)
            do{
                guard let response = try await request(endpoint)else{return}
                getCartDetails(id: self.cartId!)
                if response.key == .fail{
                    show(errorMessage: response.message)
                }
            }catch{
                
            }
        }
    }
    func decreaseProductQuantity(cartId: Int, itemId: Int){
        Task{
            let endpoint = CartEndpoints.decreaseProductQuantity(cartId: cartId, itemId: itemId)
            do{
                guard let _ = try await request(endpoint)else{return}
                getCartDetails(id: self.cartId!)
            }catch{
                
            }
        }
    }
    
    func deleteClientCart(cartId: Int){
        showIndicator()
        Task{
            let endpoint = CartEndpoints.deleteClientCart(cartId:cartId)
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                show(successMessage: response.message)
                pop()
            }catch{
                
            }
        }
    }
    func deleteClientItem(itemId: Int){
        showIndicator()
        Task{
            let endpoint = CartEndpoints.deleteClientItem(itemId: itemId)
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                getCartDetails(id: self.cartId!)
                show(successMessage: response.message)
                pop()
            }catch{
                
            }
        }
    }
    
    func getPaymentMethod(){
        Task{
            let endpoint = CartEndpoints.getPaymentMethod()
            do{
                guard let response = try await request(endpoint)else{return}
                paymentMethodModel = response.data ?? []
            }catch{
                print("error calling payment methods api : \(error.localizedDescription)")
            }
        }
    }
    
    func clientOrdersStore(providerId: Int, cartId: Int, paymentMethod: String, note: String?, contactPhone: String, deliveryMethod: String, userReceiveLat: String?, userRecieveLng: String?, userReceivingAddress: String?, receivingTime: String, financialTotal: String, financialTax: String, financialApp: String, financialDeliveryPrice: String, financialFinalTotal: String, delegateRate: Double, deliveryCost: Double){
        Task{
            let endpoint = CartEndpoints.clientOrdersStore(providerId: providerId, cartId: cartId, paymentMethod: paymentMethod, note: note ?? "", contactPhone: contactPhone, deliveryMethod: deliveryMethod, userReceiveLat: userReceiveLat, userRecieveLng: userRecieveLng, userReceivingAddress: userReceivingAddress, receivingTime: receivingTime, financialTotal: financialTotal, financialTax: financialTax, financialApp: financialApp, financialDeliveryPrice: financialDeliveryPrice, financialFinalTotal: financialFinalTotal, delegateRate: delegateRate, deliveryCost: deliveryCost)
            do{
                guard let response = try await request(endpoint)else{return}
                if response.key == .success{
                    presentSuccessView()
                }else{
                    show(errorMessage: response.message)
                }
            }catch{
                print("error storing order api : \(error.localizedDescription)")
            }
        }
    }
    
    
}

