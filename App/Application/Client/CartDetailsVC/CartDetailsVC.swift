//
//  CartDetailsVC.swift
//  App
//
//  Created by Abdo Emad on 12/06/2024.
//

import UIKit

class CartDetailsVC: BaseViewController {
    
    
    @IBOutlet weak var providerImage: UIImageView!
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var ordersNum: UILabel!
    
    
    @IBOutlet weak var productTableView: UITableView!
    @IBOutlet weak var serviceTableView: UITableView!
    
    @IBOutlet weak var totalLable: UILabel!
    @IBOutlet weak var taxLable: UILabel!
    @IBOutlet weak var deliveryPriceLable: UILabel!
    @IBOutlet weak var finalTotal: UILabel!
    
    @IBOutlet weak var receivingMethodField: UITextField!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var timeField: UITextField!
    @IBOutlet weak var userPhoneField: UITextField!
    @IBOutlet weak var notesField: UITextView!
    @IBOutlet weak var paymentTableView: UITableView!
    @IBOutlet weak var receiveAddressStackView: UIStackView!
    
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    var cartId: Int?
    var productModle : [Product] = []{
        didSet{
            productTableView.reloadData()
            if productModle.isEmpty{
                productTableView.isHidden = true
            }else{
                productTableView.isHidden = false
            }
        }
    }
    var serviceModle : [Product] = []{
        didSet{
            serviceTableView.reloadData()
            if serviceModle.isEmpty{
                serviceTableView.isHidden = true
            }else{
                serviceTableView.isHidden = false
            }
        }
    }
    var paymentMethodModel : [PaymentMethodModel] = []{
        didSet{
            paymentTableView.reloadData()
        }
    }
    var deliveryMethodKey: String?
    var receiveLat: Double?
    var receiveLng: Double?
    var selectedPaymentMethod : String?
    var appPrice: String?
    var deliveryPriceValue: String?
    var finalTotalPrice: String?
    var financialTotalValue: String?
    var financialTaxValue: String?
    var deliveryCost: Double?
    var selectdDate: String?
    var proiderId: Int?
    var delegateRate : Double?
    private var pickerView = UIPickerView()
    private var receivingMethod: [ReceivingMethodModel] = [ReceivingMethodModel(key: "from_home", name: "In home".localized), ReceivingMethodModel(key: "from_store", name: "From store".localized)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        registerTableViews()
        getCartDetails(id: cartId!)
    }
    
    private func setUp(){
        addBackButtonWith(title: "Details".localized, color: .white)
        receiveAddressStackView.isHidden = false
        deliveryMethodKey = "from_home"
        receivingMethodField.text = "In home".localized
        pickerView.delegate = self
        pickerView.dataSource = self
        receivingMethodField.inputView = pickerView
        addressField.addTapGesture {
            self.goToMapAction()
        }
        setupTimeField()
    }
    private func registerTableViews(){
        [productTableView,serviceTableView,paymentTableView].forEach { tableView in
            tableView?.delegate = self
            tableView?.dataSource = self
        }
        productTableView.register(cellType: ProductTableViewCell.self)
        serviceTableView.register(cellType: ServiceTableViewCell.self)
        paymentTableView.register(cellType: PaymentTableViewCell.self)
    }
    private func setupTimeField() {
        timeField.setDatePickerAsInputViewFor(target: self, selector: #selector(dateSelected))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(timeFieldTapped))
        timeField.addGestureRecognizer(tapGesture)
    }
    
    @objc private func timeFieldTapped() {
        timeField.becomeFirstResponder()
    }
    
    //MARK: - Actions
    
    @IBAction func deleteProviderButtonTapped(_ sender: UIButton) {
        deleteClientCart(cartId: cartId!)
    }
    
    
    @IBAction func confirmButtonTapped(_ sender: UIButton) {
        do{
            if deliveryMethodKey == "from_home"{
                let userPhone = try ValidationService.validate(userPhoneNumber: userPhoneField.text)
                let receiveWay = try ValidationService.validate(recieveWay: deliveryMethodKey)
                let address = try ValidationService.validate(receiveAddress: addressField.text)
                let time = try ValidationService.validate(receiveTime: timeField.text)
                
                clientOrdersStore(providerId: proiderId!, cartId: cartId!, paymentMethod: selectedPaymentMethod ?? "wallet", note: notesField.text ?? " ", contactPhone: userPhone, deliveryMethod: receiveWay, userReceiveLat:String(receiveLat ?? 0.0) , userRecieveLng: String(receiveLng ?? 0.0), userReceivingAddress: address, receivingTime: time, financialTotal: financialTotalValue ?? "", financialTax: financialTaxValue ?? " ", financialApp: appPrice ?? " ", financialDeliveryPrice: deliveryPriceValue ?? "", financialFinalTotal: finalTotalPrice ?? "", delegateRate: delegateRate ?? 0, deliveryCost: deliveryCost ?? 0)
            }else if deliveryMethodKey == "from_store"{
                let userPhone = try ValidationService.validate(userPhoneNumber: userPhoneField.text)
                let receiveWay = try ValidationService.validate(recieveWay: deliveryMethodKey)
                let time = try ValidationService.validate(receiveTime: timeField.text)
                
                clientOrdersStore(providerId: proiderId!, cartId: cartId!, paymentMethod: selectedPaymentMethod ?? "wallet", note: notesField.text ?? " ", contactPhone: userPhone, deliveryMethod: receiveWay, userReceiveLat: nil , userRecieveLng: nil, userReceivingAddress: nil, receivingTime: time, financialTotal: financialTotalValue ?? "", financialTax: financialTaxValue ?? " ", financialApp: appPrice ?? " ", financialDeliveryPrice: deliveryPriceValue ?? "", financialFinalTotal: finalTotalPrice ?? "", delegateRate: delegateRate ?? 0, deliveryCost: deliveryCost ?? 0)
            }
            
        }catch{
            show(errorMessage: error.localizedDescription)
        }
    }
    
    @objc func goToMapAction() {
        let vc = GoogleMapVC.create(type: .order)
        vc.delegate = self
        push(vc)
    }

    @objc func dateSelected() {
        if let datePicker = self.timeField.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.locale = Locale(identifier: "en")
            dateFormatter.dateFormat = "yyyy-MM-dd"
            self.timeField.text = dateFormatter.string(from: datePicker.date)
            self.selectdDate = dateFormatter.string(from: datePicker.date)
            timeField.resignFirstResponder()
        }
        self.timeField.setTimePickerAsInputViewFor(target: self, selector: #selector(timeSelected))
        self.timeField.resignFirstResponder()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.timeField.becomeFirstResponder()
        }
    }
    
    
    @objc func timeSelected() {
        if let datePicker = self.timeField.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.locale = Locale(identifier: "en")
            dateFormatter.dateFormat = "HH:mm:ss"
            self.timeField.text = "\(selectdDate ?? "")" + " " + "\(dateFormatter.string(from: datePicker.date))"
        }
        self.timeField.resignFirstResponder()
    }
    
}


//MARK: - TableView

extension CartDetailsVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == productTableView{
            return productModle.count
        }else if tableView == serviceTableView{
            return serviceModle.count
        }else{
            return paymentMethodModel.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == productTableView{
            let cell = productTableView.dequeueReusableCell(with: ProductTableViewCell.self, for: indexPath)
            cell.setData(productModle[indexPath.row])
            cell.quantityStackView.isHidden = true
            cell.onIncreased = {[weak self] in
                guard let self = self else{return}
                guard let item = productModle[indexPath.row].id else {return}
                self.increaseProductQuantity(cartId: cartId!, itemId: item)
            }
            cell.onDcreased = {[weak self] in
                guard let self = self else{return}
                let item = productModle[indexPath.row]
                if item.quantity ?? 1 > 1 {
                    self.decreaseProductQuantity(cartId: cartId!, itemId: item.id ?? 0)
                }
            }
            cell.onDeleted = {[weak self] in
                guard let self = self else{return}
                guard let item = productModle[indexPath.row].id else {return}
                self.deleteClientItem(itemId: item)
            }
            return cell
        }else if tableView == serviceTableView{
            let cell = serviceTableView.dequeueReusableCell(with: ServiceTableViewCell.self, for: indexPath)
            cell.setData(serviceModle[indexPath.row])
            cell.deleteAction = {[weak self ]in
                guard let self = self else {return}
                guard let item = serviceModle[indexPath.row].id else{return}
                self.deleteClientItem(itemId: item)
            }
            return cell
        }else{
            let cell = paymentTableView.dequeueReusableCell(with: PaymentTableViewCell.self, for: indexPath)
            cell.selectionStyle = .blue
            cell.setData(paymentMethodModel[indexPath.row])
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == paymentTableView {
            for i in 0..<paymentMethodModel.count {
                paymentMethodModel[i].isSelected = false
            }
            paymentMethodModel[indexPath.row].isSelected = true
            self.selectedPaymentMethod = paymentMethodModel[indexPath.row].key
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == productTableView{
            return 130
        }else if tableView == serviceTableView{
            return 130
        }else{
            return 70
        }
    }
    
}


//MARK: - DropDownList
extension CartDetailsVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        receivingMethod.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        receivingMethodField.text = receivingMethod[row].name
        deliveryMethodKey = receivingMethod[row].key
        
        return receivingMethod[row].name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if receivingMethod[row].key == "from_store" {
            deliveryMethodKey = "from_store"
            receiveAddressStackView.isHidden = true
            deliveryCost = 0
            deliveryPriceLable.text = "\(deliveryCost!) " + "SAR".localized
        }else {
            deliveryMethodKey = "from_home"
            receiveAddressStackView.isHidden = false
            getCartDetails(id: cartId!)
        }
    }
}


//MARK: - getAddress

extension CartDetailsVC: GetAddressOnMap {
    func getAddressOnMap(address: String, lat: Double, lng: Double) {
        let lat = lat
        let lng = lng
        
        let roundedReceiveLat = ((lat) * 1000).rounded() / 1000
        let roundedReceiveLng = ((lng) * 1000).rounded() / 1000
        
        self.receiveLat = roundedReceiveLat
        self.receiveLng = roundedReceiveLng
        self.addressField.text = address
    }
}

//MARK: - present SuccesView
extension CartDetailsVC : SuccessProtocol{
    func popOut() {
        pop()
    }
    func presentSuccessView(){
        let vc = SuccessVC.create(delegate: self,
                                  title: "Your request has been sent sucefully".localized,
                                  subtitle: "You can follow your order from orders screen".localized,
                                  isModalInPresentation: true
        )
        
        present(vc, animated: true)
    }
}
