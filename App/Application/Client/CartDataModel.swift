//
//  CartModel.swift
//  App
//
//  Created by Abdo Emad on 11/06/2024.
//


import Foundation

struct CartDetailsModel: Codable {
    var cart: Cart?
    var financial: Financial?
}

// MARK: - Cart
struct Cart: Codable {
    var id: Int?
    var provider: Provider?
    var items: Items?
    var countOfItems: Int?

    enum CodingKeys: String, CodingKey {
        case id, provider, items
        case countOfItems = "count_of_items"
    }
}

// MARK: - Items
struct Items: Codable {
    var products, services: [Product]?
}

// MARK: - Product
struct Product: Codable {
    var id, quantity: Int?
    var name: String?
    var price: Double?
    var productId: Int?
    var attachments: [Attachment]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case quantity
        case price
        case name
        case productId = "product_id"
        case attachments
    }
}

// MARK: - Financial
struct Financial: Codable {
    var total, tax, app, deliveryPrice: Double?
    var finalTotal: Double?
    var delegateRate: Double?
    var deliveryCost: Double?

    enum CodingKeys: String, CodingKey {
        case total, tax, app
        case deliveryPrice = "delivery_price"
        case finalTotal = "final_total"
        case delegateRate = "delegate_rate"
        case deliveryCost = "delivery_cost"
        
    }
}


// MARK: - CartModel
struct CartModel: Codable {
    var data: [CartDataModel]?
}

// MARK: - CartDataModel
struct CartDataModel: Codable {
    var id: Int?
    var provider: Provider?
    var countOfItems: Int?

    enum CodingKeys: String, CodingKey {
        case id, provider
        case countOfItems = "count_of_items"
    }
}
