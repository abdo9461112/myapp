//
//  OrdersDetailsVC.swift
//  App
//
//  Created by Abdo Emad on 13/06/2024.
//

import UIKit

class OrdersDetailsVC: BaseViewController {
    
    
    @IBOutlet weak var stepperView: StepperView!
    @IBOutlet weak var productTableview: UITableView!
    @IBOutlet weak var serviceTableView: UITableView!
    
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var tax: UILabel!
    @IBOutlet weak var deliveryPrice: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    
    @IBOutlet weak var receivingAddress: UILabel!
    @IBOutlet weak var timeOfReceipt: UILabel!
    @IBOutlet weak var userPhone: UILabel!
    @IBOutlet weak var notes: UILabel!
    
    @IBOutlet weak var delegatView: UIView!
    @IBOutlet weak var delegateName: UILabel!
    @IBOutlet weak var delegateNumber: UILabel!
    @IBOutlet weak var delegateImage: UIImageView!
    
    @IBOutlet weak var providerView: UIView!
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var providerNumber: UILabel!
    @IBOutlet weak var providerImage: UIImageView!
    
    @IBOutlet weak var waitingLable: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var rateButton: UIButton!
    
    @IBOutlet weak var receiptButton: UIButton!
    @IBOutlet weak var receiveSuccessfullyStack: UIStackView!
    
    @IBOutlet weak var chatWithDelegate: UIButton!
    
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    var orderId:Int?
    
    private var productModel: [Product] = []{
        didSet{
            productTableview.reloadData()
            if productModel.isEmpty{
                productTableview.isHidden = true
            }
        }
    }
    private var serviceModel: [Product] = []{
        didSet{
            serviceTableView.reloadData()
            if serviceModel.isEmpty{
                serviceTableView.isHidden = true
            }
        }
    }
    
    private var fromStoreStepperSteps: [String] = [
        "In progress".localized,
        "Prepared".localized,
        "Finish order".localized,
    ]
    
    
    private var inHouseStepperSteps: [String] = [
        "In progress".localized,
        "Delegate on the way to you".localized,
        "Delivered".localized,
    ]
    private var providerId: Int?
    private var delegateId: Int?
    private var deliveryType: String?
    private var isRatedFlag: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        getOrderDetials(orderId: orderId!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        (self.navigationController as? ColoredNav)?.changeApperance(to: .colored)
    }
    
    private func setUp(){
        addBackButtonWith(title: "Details".localized, color: .white)
        [serviceTableView,productTableview].forEach { tableView in
            tableView?.delegate = self
            tableView?.dataSource = self
        }
        serviceTableView.register(cellType: ServiceTableViewCell.self)
        productTableview.register(cellType: ProductTableViewCell.self)
        stepperView.isHidden = true
        receiptButton.isHidden = true
        waitingLable.isHidden = true
        cancelButton.isHidden = true
        payButton.isHidden = true
        chatButton.isHidden = true
        receiveSuccessfullyStack.isHidden = true
        rateButton.isHidden = true
        delegatView.isHidden = true
        providerView.isHidden = true
    }
    static func create(orderId:Int) -> OrdersDetailsVC{
        let vc = OrdersDetailsVC()
        vc.orderId = orderId
        return vc
    }
    
    private func configureStepperView(stepperSteps: [String]) {
        stepperView.setStepperLinesColor(activeColor: .main, notActiceColor: .lightGray)
        stepperView.setStepperLabelsColor(activeColor: .black, notActiceColor: .black)
        stepperView.setStepperViewImages(nextStateImage: UIImage(named: "unStageStepper"), currentStateImage: UIImage(named: "stageStepper"), finishedStateImage: UIImage(named: "stageStepper"))
        stepperView.setStepperTitlesForItems(titles: stepperSteps)
    }
    
    private func setData(_ data : OrderDetailsModel?){
        if data?.deliveryMethod == "from_store" {
            configureStepperView(stepperSteps: fromStoreStepperSteps)
            receivingAddress.text = "From store".localized
        }else {
            configureStepperView(stepperSteps: inHouseStepperSteps)
            receivingAddress.text = data?.userRecevingAddress
        }
        total.text = "\(data?.total ?? 0) " + "SAR".localized
        tax.text =  "\(data?.taxValue ?? 0) " + "SAR".localized
        deliveryPrice.text = "\(data?.deliveryPrice ?? 0) " + "SAR".localized
        totalPrice.text = "\(data?.finalTotal ?? 0) " + "SAR".localized
        timeOfReceipt.text = data?.receivingTime
        userPhone.text = data?.user.phone
        notes.text = data?.notes
        providerName.text = data?.provider.name
        providerNumber.text = data?.provider.phone
        providerImage.setWith(data?.provider.image)
        delegateImage.setWith(data?.delegate.image)
        delegateName.text = data?.delegate.name
        delegateNumber.text = data?.delegate.phone
        productModel = data?.items.products ?? []
        serviceModel = data?.items.services ?? []
        providerId = data?.provider.id
        delegateId = data?.delegate.id
        isRatedFlag = data?.isRated
    }
    func handleResponseViews(status: Status, isRated: Bool) {
        
        switch (status.client, status.provider, status.delegate) {
        
        case (.wattingAccept, .wattingAccept, _):
            handleWaitingForAcceptanceStatus()
        
        case (.prepareForPay, .accepted, _):
            handlePrepareForPayStatus()
        
        case (.paid, .accepted, _):
            handleAcceptedStatus()
        
        case (.paid, .preparing, _):
            handlePreparedStatus()
        
        case (.paid, .delegateHasArrived, .receivedFromProvider):
            if self.deliveryType == "from_store" {
                handleClientReceived()
            } else {
                handleDelegateReceivedFromProvider()
            }
        
        case (.paid, .delegateHasArrived, .receivedToClient):
            handleDeliveredStatus()
        
        case (.paid, .delegateHasArrived, _):
            handleDelegateHasArrived()
        
        case (.paid, .onMyWay, _):
            handleClientReceived()
        
        case (.receivedFromDelegate,.delegateHasArrived, .receivedToClient):
            handleFinishedStatus()
        
        case (_, .rejected, _):
            handleRejectedStatus()
        
        default:
            break
        }
    }

    
    
    @IBAction func cancelOrderButtTapped(_ sender: UIButton) {
        cancelOrder(orderId: orderId!)
        
    }
    
    @IBAction func chatButtonTapped(_ sender: UIButton) {
    }
    
    @IBAction func payButtonTapped(_ sender: UIButton) {
        guard let id = orderId else {return}
        let vc = PaymentVC.create(orderId: id, totalPrice: totalPrice.text ?? "", offerId: 0, type: .order)
        push(vc)
    }
    
    @IBAction func rateButtonTapped(_ sender: UIButton) {
        let vc = RateVC.create(providerId: providerId ?? 0, delgateId: delegateId ?? 0, orderId: orderId ?? 0)
        push(vc)
    }
    
    @IBAction func chatWithDelegateButTapped(_ sender: UIButton) {
    }
    
    @IBAction func finishOrderButtonTapped(_ sender: UIButton) {
        guard let id = orderId else {return}
        changeStatus(orderId: id, status: "received_from_delegate")
    }
    
    @IBAction func receiptButtonTapped(_ sender: UIButton) {
        clientOrderInvoice(orderId: orderId ?? 0 )
    }
    
}


//MARK: - TableView
extension OrdersDetailsVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == productTableview{
            return productModel.count
        }else{
            return serviceModel.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == productTableview{
            let cell = tableView.dequeueReusableCell(with: ProductTableViewCell.self, for: indexPath)
            cell.setData(productModel[indexPath.row])
            cell.deleteButton.isHidden = true
            cell.countingView.isHidden = true
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(with: ServiceTableViewCell.self, for: indexPath)
            cell.setData(serviceModel[indexPath.row])
            cell.deleteButton.isHidden = true
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}

//MARK: - NetWroking

extension OrdersDetailsVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getOrderDetials(orderId:Int){
        Task{
            showIndicator()
            let endpoint = OrdersEndpoint.getOrderDetials(orderId: orderId)
            do{
                guard let response = try await request(endpoint) else{return}
                hideIndicator()
                if let data = response.data{
                    self.setData(data)
                    self.handleResponseViews(status: data.status, isRated: true)
                }
            }catch{
                print("error calling order details api : \(error.localizedDescription)")
            }
        }
    }
    private func cancelOrder(orderId:Int){
        Task{
            showIndicator()
            let endpoint = OrdersEndpoint.clientOrderCancel(orderId: orderId)
            do{
                guard let response = try await request(endpoint) else {return}
                hideIndicator()
                if response.key == .success{
                    presentSuccessVC()
                }else{
                    show(errorMessage: response.message)
                }
            }catch{
                print("error calling cancel Order api : \(error.localizedDescription)")
            }
        }
    }
    private func changeStatus(orderId: Int, status: String){
        Task{
            let endpoint = OrdersEndpoint.clientOrderStatusChange(orderId: orderId, status: status)
            do{
                guard let _ = try await request(endpoint)else{return}
                getOrderDetials(orderId: orderId)
            }catch{
                
            }
        }
    }
    private func clientOrderInvoice(orderId:Int) {
        Task{
            let endpoint =  OrdersEndpoint.clientOrdersInvoice(orderId: orderId)
            do{
               guard let response = try await request(endpoint)else{return}
                guard let urlString = response.data?.link else {return}
                guard let url = URL(string: urlString) else {return}
                await UIApplication.shared.open(url)
            }catch{
                print("error opening client order invice url : \(error.localizedDescription)")
            }
        }
    }
}

//MARK: - Handle Views -
extension OrdersDetailsVC {
    
    private func handleWaitingForAcceptanceStatus() {
        waitingLable.isHidden = false
        cancelButton.isHidden = false
        waitingLable.text = "Watting for Acceptance".localized
    }
    
    private func handlePrepareForPayStatus() {
        waitingLable.isHidden = false
        payButton.isHidden = false
        waitingLable.text = "Order accepted".localized
    }
    
    private func handleAcceptedStatus() {
        receiptButton.isHidden = false
        providerView.isHidden = false
        waitingLable.isHidden = false
        waitingLable.text = "Wating to be prepared".localized
    }
    
    private func handlePreparedStatus() {
        stepperView.setStepperCurrentIndex(toIndex: 0)
        stepperView.isHidden = false
        chatButton.isHidden = false
        receiptButton.isHidden = false
        providerView.isHidden = false
    }
    
    private func handleDelegateHasArrived() {
        stepperView.setStepperCurrentIndex(toIndex: 0)
        stepperView.isHidden = false
        chatButton.isHidden = false
        receiptButton.isHidden = false
        providerView.isHidden = false
    }
    
    private func handleDelegateReceivedFromProvider() {
        stepperView.setStepperCurrentIndex(toIndex: 1)
        stepperView.isHidden = false
        chatButton.isHidden = false
        receiptButton.isHidden = false
        providerView.isHidden = false
        delegatView.isHidden = false
    }
    
    private func handleClientReceived() {
        stepperView.setStepperCurrentIndex(toIndex: 2)
        stepperView.isHidden = false
        chatButton.isHidden = false
        receiptButton.isHidden = false
        providerView.isHidden = false
        receiveSuccessfullyStack.isHidden = false
        if isRatedFlag == true {
            rateButton.isHidden = true
        }else {
            rateButton.isHidden = false
        }
    }
    
    private func handleDeliveredStatus() {
        stepperView.setStepperCurrentIndex(toIndex: 1)
        stepperView.isHidden = false
        receiptButton.isHidden = false
        providerView.isHidden = false
        delegatView.isHidden = false
        receiveSuccessfullyStack.isHidden = false

    }
    
    private func handleFinishedStatus() {
        stepperView.setStepperCurrentIndex(toIndex: 2)
        waitingLable.isHidden = false
        receiveSuccessfullyStack.isHidden = true
        waitingLable.text = "Order has been received".localized
        if isRatedFlag == true {
            rateButton.isHidden = true
        }else {
            rateButton.isHidden = false
        }
    }
    
    private func handleRejectedStatus() {
        stepperView.setStepperCurrentIndex(toIndex: 2)
        providerView.isHidden = false
        waitingLable.isHidden = false
        waitingLable.text = "This order has been rejected".localized
    }
    

}


extension OrdersDetailsVC: SuccessProtocol{
    func popOut() {
        let vc = UserTabBarController()
        AppHelper.changeWindowRoot(vc:vc)
    }
    
    private func presentSuccessVC(){
        let vc = SuccessVC.create(delegate: self, title: "The order has been successfully cancelled".localized)
        present(vc, animated: true)
    }
}
