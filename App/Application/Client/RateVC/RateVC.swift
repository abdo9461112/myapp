//
//  RateVC.swift
//  App
//
//  Created by Abdo Emad on 22/06/2024.
//

import UIKit
import Cosmos
class RateVC: BaseViewController {

    @IBOutlet weak var providerRate: CosmosView!
    @IBOutlet weak var providerMessage: UITextView!
    @IBOutlet weak var delegateRate: CosmosView!
    @IBOutlet weak var delegateMessage: UITextView!
    @IBOutlet weak var rateDelegaetStackView: UIStackView!
    
    private var providerId: Int?
    private var delgateId: Int?
    private var orderId : Int?
    private let responseHandler: ResponseHandler = DefaultResponseHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }

    private func setUp(){
        if delgateId == 0 {
            rateDelegaetStackView.isHidden = true
        }
        addBackButtonWith(title: "Rate".localized, color: .white)
    }

    static func create(providerId: Int,delgateId: Int,orderId: Int) -> RateVC{
        let vc = RateVC()
        vc.providerId = providerId
        vc.delgateId = delgateId
        vc.orderId = orderId
        return vc
    }
    @IBAction func rateButtonTapped(_ sender: UIButton) {
        addReview(orderId: orderId!, providerId: providerId!, providerValue: providerRate.rating, providerMessage: providerMessage.text, delegateId: delgateId, delegateValue: delegateRate.rating, delegateMessage: delegateMessage.text)
    }
    
}

extension RateVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    private func addReview(orderId:Int,providerId:Int,providerValue: Double, providerMessage: String?, delegateId: Int?,delegateValue: Double?, delegateMessage: String?){
        Task{
            let endpoint = OrdersEndpoint.addReview(orderId: orderId, providerId: providerId, providerValue: providerValue, providerComment: providerMessage, delegateId: delegateId, delegateValue: delegateValue, delegateComment: delegateMessage)
            do{
                guard let response = try await request(endpoint)else{return}
                if response.key == .success{
                    show(successMessage: response.message)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.popToRoot()
                    }
                }
            }catch{
                print("error adding review api request : \(error.localizedDescription)")
            }
        }
    }
}
