//
//  About&TermsVC.swift
//  App
//
//  Created by Abdo Emad on 19/05/2024.
//

import UIKit

class About_TermsVC: BaseViewController {
    enum AboutTermsType{
        case about
        case terms
    }
    
    var type: AboutTermsType = .about
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    private var textViewData = UITextView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchContent()
        configureInitialDesign()
        setupTextView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
    }
    
    static func create(type: AboutTermsType) -> About_TermsVC {
        let vc = About_TermsVC()
        vc.type = type
        return vc
    }
    
    private func configureInitialDesign() {
        let title = (type == .about) ? "About App".localized : "Terms and conditions".localized
        self.addBackButtonWith(title: title,color: .white)
    }
    
    private func fetchContent() {
        if type == .about {
            getAboutData()
        } else {
            getTermsData()
        }
        
    }
    
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<BaseResponse<ResponseData>>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress).data
    }
    
    
    private func setupTextView() {
        textViewData.translatesAutoresizingMaskIntoConstraints = false
        textViewData.isEditable = false
        textViewData.font = UIFont.systemFont(ofSize: 20)
        textViewData.textColor = .new
        textViewData.backgroundColor = .systemBackground
        textViewData.addCornerRadius(radious: 10)
        view.addSubview(textViewData)
        
        NSLayoutConstraint.activate([
            textViewData.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
            textViewData.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            textViewData.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            textViewData.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16)
        ])
    }
}

extension About_TermsVC {
    
    func getTermsData() {
        Task {
            showIndicator()
            do {
                if let htmlString = try await request(SettingsEndpoints.Terms()),
                   let htmlData = htmlString.data(using: .utf8),
                   let attributedString = try? NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) {
                    print("HTMLDATA:\(htmlData)")
                    hideIndicator()
                    textViewData.attributedText = attributedString
                    textViewData.textColor = .new
                    textViewData.font = UIFont.systemFont(ofSize: 14)
                } else {
                    print("Error: Unable to process HTML string")
                }
            } catch {
                print("Error: \(error.localizedDescription)")
            }
        }
        
    }
    
    func getAboutData() {
        Task {
            showIndicator()
            do {
                if let htmlString = try await request(SettingsEndpoints.about()),
                   let htmlData = htmlString.data(using: .utf8),
                   let attributedString = try? NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) {
                    print("HTMLDATA:\(htmlData)")
                    hideIndicator()
                    textViewData.attributedText = attributedString
                    textViewData.textColor = .new
                    textViewData.font = UIFont.systemFont(ofSize: 18)
                    
                } else {
                    print("Error: Unable to process HTML string")
                }
            } catch {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
}
