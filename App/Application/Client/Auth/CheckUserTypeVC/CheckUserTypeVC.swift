//
//  CheckUserTypeVC.swift
//  App
//
//  Created by Abdo Emad on 08/06/2024.
//

import UIKit

enum UserType{
    case client
    case delgate
}
class CheckUserTypeVC: BaseViewController {


    @IBOutlet weak var delgateView: UIView!
    @IBOutlet weak var clientView: UIView!
    
    var userType : UserType = .client
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        handleClientView()
    }
    private func setup(){
        setLeading(title: "Select user".localized)

        clientView.addTapGesture {
            self.handleClientView()
        }
        delgateView.addTapGesture {
            self.handleDelgateView()
        }
    }
    
    @objc func handleClientView(){
        clientView.backgroundColor = .main
        delgateView.backgroundColor = .systemBackground
        userType = .client
    }
    @objc func handleDelgateView(){
        delgateView.backgroundColor = .main
        clientView.backgroundColor = .systemBackground
        userType = .delgate
    }

    @IBAction func confirmButton(_ sender: UIButton) {
        switch userType {
        case .client:
         goToLoginVC()
        case .delgate:
            show(errorMessage: "ain't available yet.")
        }
    }
    
}

extension CheckUserTypeVC{
    private func goToLoginVC(){
        let vc = ClientLoginVC()
        let nav = BaseNavigationController(rootViewController: vc)
        AppHelper.changeWindowRoot(vc: nav)
    }
}
