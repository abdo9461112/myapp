//
//  ClientVerificationCodeVC.swift
//  App
//
//  Created by Abdo Emad on 08/05/2024.
//

import UIKit



class ClientVerificationCodeVC: BaseViewController{
    
    @IBOutlet weak var codeTextField: OTPTextField!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var resendCodeButton: UIButton!
    
    
    var phone : String = ""
    var code = ""
    private var countryCode: String?

    let responseHandler : ResponseHandler = DefaultResponseHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesing()
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        (self.navigationController as? ColoredNav)?.changeApperance(to: .light)
    }
    
    private func setupDesing(){
        codeOTPConfigure()
        addBackButtonWith(title: "Verification Code".localized)
        confirmButton.addCornerRadius(radious: 10)
        codeTextField.didEnterLastDigit = { code in
        self.code = code
        }
    }
    
    private func codeOTPConfigure() {
        codeTextField.configure(with: 4)
        codeTextField.didEnterLastDigit = { [weak self] code in
            guard let self = self else { return }
            self.codeTextField.resignFirstResponder()
        }
    }
    
    static func create(phone: String, countryCode: String) -> ClientVerificationCodeVC{
        let vc  = ClientVerificationCodeVC()
        vc.phone = phone
        vc.countryCode = countryCode
        return vc
    }
    
    @IBAction func confirmButtonTaped(_ sender: Any) {
        
            do{
                let code = try ValidationService.validate(verificationCode: code)
                verfiyCode(phone: phone, code: code, countryCode: countryCode ?? " ", type: "login")

            }catch{
                
                show(errorMessage: error.localizedDescription)
        }
    }
       
    
    
    
    @IBAction func resendCodeButtonTaped(_ sender: Any) {
        resendCode(phone: phone, countryCode: countryCode ?? "", type: "active")
    }
    
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
}


//MARK: - NetWorknig

extension ClientVerificationCodeVC{
    
    func verfiyCode(phone:String,code:String,countryCode: String, type: String){
        Task{
            do{
                let endpoint = AuthEndpoints.verifyCode(phone: phone, code: code, countryCode: countryCode, type: type)
                guard let response = try await self.request(endpoint)else {return}
                    if response.key == .success{
                        self.show(successMessage: response.message)
                        UserDefaults.user = response.data
                        UserDefaults.isLogin = true
                        UserDefaults.accessToken = response.data?.token
                        UserDefaults.isFirstTime = false
                        self.goLocationScreen()
                    }else{
                        self.show(errorMessage: response.message)
                    }
            }catch{
                
                print("Error verfiying code : \(error.localizedDescription)")
            }
          
        }
    }
    func resendCode(phone: String, countryCode: String, type: String) {
        Task {
            let endpoint = AuthEndpoints.resendCode(phone: phone, countryCode: countryCode, type: type)
            do {
                guard let response = try await request(endpoint) else { return }
                show(successMessage: response.message)

            } catch {
                print("Error: \(error.localizedDescription)")
            }
        }
    }

}


extension ClientVerificationCodeVC{
    private func goLocationScreen(){
        let vc = GoogleMapVC.create(type: .login)
        push(vc)
    }
}
