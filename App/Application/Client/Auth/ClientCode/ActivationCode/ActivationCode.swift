//
//  ActivationCode.swift
//  App
//
//  Created by Abdo Emad on 27/05/2024.
//

import UIKit

class ActivationCode: BaseViewController {
    
    @IBOutlet weak var codeTextField: OTPTextField!
    private var phone : String?
    private var code : String = " "
    private var countryCode : String?
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesign()
    }
    
    static func create(phone: String, countrtCode: String) -> ActivationCode{
        let vc = ActivationCode()
        vc.phone = phone
        vc.countryCode = countrtCode
        return vc
    }
    
    private func setupDesign(){
        codeOTPConfigure()
        setLeading(title: "Activation Code".localized)
        codeTextField.didEnterLastDigit = { code in
            self.code = code
        }
        
    }
    
    private func codeOTPConfigure() {
        codeTextField.configure(with: 4)
        codeTextField.didEnterLastDigit = { [weak self] code in
            guard let self = self else { return }
            self.codeTextField.resignFirstResponder()
        }
    }
    
    
    @IBAction func confirmButtonTaped(_ sender: UIButton) {
        do{
            let code = try ValidationService.validate(verificationCode: code)
            
            activetionCode(phone: phone ?? " " , code: code, countryCode: countryCode ?? " ")
            
        }catch{
            
            show(errorMessage: error.localizedDescription)
        }
    }
    @IBAction func resendButtonTaped(_ sender: UIButton) {
        resendCode(phone: phone!, countryCode: countryCode!, type: "active")
    }
    
}


extension ActivationCode{
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func activetionCode(phone : String ,code : String, countryCode : String){
        Task{
            let endpoint = AuthEndpoints.activateAccount(phone: phone, code: code, countryCode: countryCode)
            do{
                guard let response = try await request(endpoint) else {return}
                show(successMessage: response.message)
                if let data = response.data{
                    UserDefaults.accessToken = data.token
                    UserDefaults.user = data
                    UserDefaults.isLogin = true
                    UserDefaults.isFirstTime = false
                    goLocationScreen()
                }else{
                    show(errorMessage: response.message)
                }
                
            }catch{
                
                print("Error Activetion Code:\(error.localizedDescription)")
            }
        }
    }
    
    func resendCode(phone: String, countryCode: String, type: String) {
        Task {
            showIndicator()
            let endpoint = AuthEndpoints.resendCode(phone: phone, countryCode: countryCode, type: type)
            do {
                guard let response = try await request(endpoint) else { return }
                hideIndicator()
                show(successMessage: response.message)
            } catch {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
}


extension ActivationCode{
    private func goLocationScreen(){
        let vc = GoogleMapVC.create(type: .login)
        push(vc)
    }
}
