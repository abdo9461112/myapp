//
//  ClientSignUpVC.swift
//  App
//
//  Created by Abdo Emad on 08/05/2024.
//

import UIKit

class ClientSignUpVC: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userPhoneText: PhoneTextFieldView!
    @IBOutlet weak var userNmaeText: NormalTextFieldView!
    @IBOutlet weak var userEmailText: EmailTextFieldView!
    @IBOutlet weak var userCity: DropDownTextFieldView!
    @IBOutlet weak var signButton: UIButton!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var termsButton: UIButton!
    
    var select =  0
    private var cityId: Int?
    private var phone: String?
    private var imageData: Data?
    private var isSelectedImage: Bool = false
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var cityModel : [City] = [ ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesing()
        getCities()
        
    }
    
    static func create()-> ClientSignUpVC{
        let vc = ClientSignUpVC()
        return vc
    }
    
    private func setupDesing(){
        addBackButtonWith(title: "New Register".localized)
        termsButton.addUnderLineOnButton(Button: termsButton, tittle: "I have read and agree to the terms and conditions".localized,color: .new)
        loginButton.addUnderLineOnButton(Button: loginButton, tittle: "Login".localized)
        signButton.addCornerRadius(radious: 10)
        userImage.layer.cornerRadius = userImage.frame.height / 2
        userCity.delegate = self
        let _ = UITapGestureRecognizer(target: self, action: #selector(getCountriesCode))
        userPhoneText.stakView.addTapGesture {
            self.getCountriesCode()
        }
    }
    
    @objc func getCountriesCode(){
        getCountryApi()
    }
        
    @IBAction func PickedImageTaped(_ sender: Any) {
        ImagePicker().pickImage { image, imageData in
            self.imageData = imageData
            self.userImage.image = image
            self.isSelectedImage = true
        }
    }
    
    
    @IBAction func termsButtonTaped(_ sender: Any) {
        let vc = About_TermsVC.create(type: .terms)
        push(vc)
    }
    
    @IBAction func checkButtonTaped(_ sender: UIButton) {
        sender.isSelected.toggle()
        if sender.isSelected{
            select = 1
            checkButton.setImage(UIImage(systemName: "checkmark.rectangle.portrait.fill"), for: .normal)
        }else{
            select = 0
            checkButton.setImage(nil, for: .normal)
        }
    }
    
    @IBAction func sginButtonTaped(_ sender: Any) {
        do {
            let _ = try ValidationService.validate(selectedPersonalImage: isSelectedImage)
            let email = try EmailValidationService.validate(email: userEmailText.emailTextValue() ?? "")
            let name = try ValidationService.validate(name: userNmaeText.textValue() ?? " ")
            let cityid = try ValidationService.validate(cityId: cityId ?? 0)
            let phone = try userPhoneText.phoneText()
            guard select == 1 else {
                show(errorMessage: "please read and agree on terms & conditions".localized)
                return
            }
            singUp(phone: phone, email: email, image: imageData, name: name, city_id: cityid, countryCode:
                    userPhoneText.countryCodeLabel.text ?? " "  , is_conditions: select)
        }catch{
            
            show(errorMessage: error.localizedDescription)
            
        }
    }
    
    @IBAction func loginButtonTaped(_ sender: Any) {
        pop()
    }
}


extension ClientSignUpVC{
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<BaseResponse<ResponseData>>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> BaseResponse<ResponseData>? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getCities(){
        Task{
            let endpoint = AuthEndpoints.getCities()
            do{
                guard let response = try await self.request(endpoint) else {return}
                cityModel = response.data ?? [ ]
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    
    private func singUp(phone:String,email: String,image:Data?, name:String, city_id:Int, countryCode : String,is_conditions : Int ){
        Task{
            showIndicator()
            let endpoint = AuthEndpoints.signUp(name: name, image: image, phone: phone, email: email, city_id: city_id, countryCode: countryCode, is_conditions: is_conditions)
            do{
                guard let response = try await self.request(endpoint) else{return}
                hideIndicator()
                if response.key == .fail{
                    show(errorMessage: response.message)
                    return
                }else{
                    show(successMessage: response.message)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        let vc = ActivationCode.create(phone: phone, countrtCode: countryCode)
                        let navController = UINavigationController(rootViewController: vc)
                        AppHelper.changeWindowRoot(vc: navController)
                    }
                  
                }
                
            }catch{
                
                print("Error sginning UP : \(error.localizedDescription)")
            }
            
        }
    }
    
    
    private func getCountryApi() {
        Task { 
            let endpoint = AuthEndpoints.getCountryCode()
            do {
                guard let response = try await self.request(endpoint) else { return }
                let vc = CountriesVC(countries: response.data ?? [ ], delegate: self)
                let nav = BaseNavigationController(rootViewController: vc)
                self.present(nav, animated: true)
            } catch {
                print("Error calling countries api : \(error.localizedDescription)")
            }
        }
    }


}



extension ClientSignUpVC: CountryCodeDelegate, DropDownTextFieldViewDelegate{
    func dropDownList(for textFieldView: DropDownTextFieldView) -> [any DropDownItem] {
        return cityModel

    }
    
    func didSelect(item: any DropDownItem, for textFieldView: DropDownTextFieldView) {
        cityId = item.id

    }
    
    
    func didSelectCountry(_ item: CountryCodeModel) {
        userPhoneText.countryCodeLabel.text = item.key
        userPhoneText.flagImage.setWith(item.image)
    }
        
}
