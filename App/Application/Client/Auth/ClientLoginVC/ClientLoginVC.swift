//
//  ClientLoginVC.swift
//  App
//
//  Created by Abdo Emad on 08/05/2024.
//

import UIKit

class ClientLoginVC: BaseViewController {
    
    @IBOutlet weak var textField: PhoneTextFieldView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var visiterButton: UIButton!
    
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    private var countryCodesModel: [CountryCodeModel] =  []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesign()
    }
    
    static func create() -> ClientLoginVC{
        let vc = ClientLoginVC()
        return vc
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
        UserDefaults.accessToken = nil
    }
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    
    private func setupDesign(){
        [loginButton,visiterButton].forEach {
            $0?.addCornerRadius(radious: 10)
        }
        
        self.setLeading(title: "Login".localized)
        signUpButton.addUnderLineOnButton(Button: signUpButton, tittle: "Sign Up".localized)
        
        textField.stakView.addTapGesture {
            self.countryCodesActions()
        }
        
    }
    
    private func countryCodesActions(){
        getCountryApi()
    }
    
    @IBAction func loginButtonTaped(_ sender: Any) {
        do{
            guard let countryCode = textField.countryCodeLabel.text else {return}
            let phone = try PhoneValidationService.validate(phone: textField.phoneTextValue())
            login(phone: phone, countryCode:countryCode)
        }catch{
            show(errorMessage: error.localizedDescription)
        }
    }
    
    @IBAction func signUpButtonTaped(_ sender: Any) {
        goToCreateAccountScreen()
        
    }
    
    @IBAction func VisiterButtonTaped(_ sender: Any) {
        goToHomeScreen()
    }
    
}

//MARK: -  Networking

extension ClientLoginVC {
    private func login(phone: String, countryCode: String) {
        Task {
            let endpoint = AuthEndpoints.login(phone: phone, countryCode: countryCode)
            do {
                guard let response = try await self.request(endpoint) else {return}
                if response.key == .success{
                    show(successMessage: response.message)
                    let vc = ClientVerificationCodeVC.create(phone: phone, countryCode: countryCode)
                    self.push(vc)
                }else if response.key == .needActive{
                    show(errorMessage: response.message)
                    let vc = ActivationCode.create(phone: phone, countrtCode: countryCode)
                    self.push(vc)
                }else{
                    show(errorMessage: response.message)
                }
            } catch {
                print("Error client login: \(error.localizedDescription)")
            }
        }
    }
    
    
    private func getCountryApi(){
        Task{
            let endpoint = AuthEndpoints.getCountryCode()
            do{
                guard let response = try await self.request(endpoint) else {return}
                self.countryCodesModel = response.data ?? [ ]
                let vc = CountriesVC(countries: countryCodesModel, delegate: self)
                let nav = BaseNavigationController(rootViewController: vc)
                self.present(nav, animated: true)
            }catch{
                print("Error calling countries api : \(error.localizedDescription)")
            }
        }
    }
}

extension ClientLoginVC{
    
    func goToHomeScreen(){
        UserDefaults.isFirstTime = true
        let vc = UserTabBarController()
        AppHelper.changeWindowRoot(vc: vc)
    }
    
    func  goToCreateAccountScreen(){
        let vc = ClientSignUpVC()
        push(vc)
    }
    
}

extension ClientLoginVC: CountryCodeDelegate{
    func didSelectCountry(_ item:  CountryCodeModel) {
        textField.flagImage.setWith(item.image)
        textField.countryCodeLabel.text = item.key
    }
    
}
