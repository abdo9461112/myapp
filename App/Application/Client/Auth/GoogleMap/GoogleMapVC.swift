//
//  GoogleMapVC.swift
//  App
//
//  Created by Abdo Emad on 09/06/2024.
//

import UIKit
import SnapKit
import GoogleMaps

protocol GetAddressOnMap {
    func getAddressOnMap(address: String, lat: Double, lng: Double)
}

enum Types {
    case login
    case order
    case home
}

class GoogleMapVC: BaseViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    private var mapView: GMSMapView!
    
    //MARK: - Properties -
    private var selectedLatitude: Double?
    private var selectedLongitude: Double?
    private var marker = GMSMarker()
    private var locator: Locator?
    private var myLocation = false
    private var type: Types = .order
    var delegate: GetAddressOnMap?
    
    //MARK: - LifeCycle Events -
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeMapView()
        setupMapLayout()
        handleInitialState()
        addBackButtonWith(title: "Map".localized)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as? ColoredNav)?.changeApperance(to: .colored)
    }
    
    private func initializeMapView() {
        mapView = GMSMapView.init()
    }
    
    private func setupMapLayout() {
        view.addSubview(mapView)
        view.sendSubviewToBack(mapView)
            mapView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(containerView.snp.top)
        }
    }
    
    //MARK: - Creation -
    static func create(type: Types) -> GoogleMapVC {
        let vc = GoogleMapVC()
        vc.type = type
        return vc
    }
    
    //MARK: - Logic Methods -
    private func handleInitialState() {
        locator = Locator(delegate: self)
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
    }
    
    private func animateMapTo(location: CLLocationCoordinate2D) {
        CATransaction.begin()
        CATransaction.setValue(0.5, forKey: kCATransactionAnimationDuration)
        mapView.animate(with: GMSCameraUpdate.setTarget(location))
        marker.position = location
        CATransaction.commit()
    }
    
    private func reverseGeo(coordinate: CLLocationCoordinate2D) {
        let geoCoder = GMSGeocoder()
        geoCoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let places = response?.results(), let place = places.first {
                self.addressLabel.text = "\(place.country ?? "") \(place.locality ?? "") \(place.thoroughfare ?? "")"
                self.selectedLatitude = coordinate.latitude
                self.selectedLongitude = coordinate.longitude
                self.marker.map = self.mapView
                self.animateMapTo(location: coordinate)
            } else {
                print(error?.localizedDescription ?? "error")
            }
        }
    }
    
    @IBAction func confirmButtonTapped(_ sender: UIButton) {
        guard let lat = selectedLatitude, let long = selectedLongitude, let address = addressLabel.text else {
            show(errorMessage: "Please select your address".localized)
            return
        }
        
        switch type {
        case .login:
            delegate?.getAddressOnMap(address: address, lat: lat, lng: long)
            UserDefaults.locationPicked = address
            navigateToHome()
        case .order, .home:
            delegate?.getAddressOnMap(address: address, lat: lat, lng: long)
            UserDefaults.locationPicked = address
            pop()
        }
    }
}

//MARK: - Extensions -
extension GoogleMapVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        reverseGeo(coordinate: coordinate)
    }
}

extension GoogleMapVC: LocatorDelegate {
    func updateUserLocation(lat: Double, long: Double) {
        reverseGeo(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long))
        mapView.camera = GMSCameraPosition(latitude: lat, longitude: long, zoom: 18)
        locator = nil
    }

    func showLocationAlert(message: String) {
        print(message)
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        guard let coordinate = mapView.myLocation?.coordinate else { return false }
        mapView.camera = GMSCameraPosition(latitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 18)
        myLocation = true
        return true
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if myLocation {
            reverseGeo(coordinate: CLLocationCoordinate2D(latitude: position.target.latitude, longitude: position.target.longitude))
            myLocation = false
        }
    }
}

extension GoogleMapVC {
    private func navigateToHome() {
        let vc = UserTabBarController()
        AppHelper.changeWindowRoot(vc: vc)
    }
}
