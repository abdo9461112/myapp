//
//  MyOrdersVC.swift
//  App
//
//  Created by Abdo Emad on 13/06/2024.
//

enum OrderStatusCases: String{
    case new
    case current
    case expired
    case privateOrder = "private"
    case جديدة
    case حالية
    case منتهية
    case خاصة

}
import UIKit
import SnapKit

class MyOrdersVC: BaseViewController {
    
    @IBOutlet weak var newButton: UIButton!
    @IBOutlet weak var currentButton: UIButton!
    @IBOutlet weak var expiredButton: UIButton!
    @IBOutlet weak var privateButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var buttonsStackView: UIStackView!
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var orderStatus : OrderStatusCases = .new
    private var ordersModel : [OrdersDataModel] = [ ] {
        didSet{
            tableView.reloadData()
            if ordersModel.isEmpty{
                emptyDataLabel.isHidden = false
            }else{
                emptyDataLabel.isHidden = true
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    private let emptyDataLabel: UILabel = {
        let label = UILabel()
        label.text = "No orders".localized
        label.textColor = .main
        label.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        return label
    }()
    
    private func setUp(){
        setLeading(title: "Orders".localized, color: .white)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: OrdersCell.self)
        view.addSubview(emptyDataLabel)
        emptyDataLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        (self.navigationController as? ColoredNav)?.changeApperance(to: .colored)
        if UserDefaults.isLogin{
            switch orderStatus{
            case .new,.جديدة:
                handleNewOrder()
            case .current,.حالية:
                handleCurrentOrder()
            case .expired,.منتهية:
                handleExpiredOrder()
            case .privateOrder,.خاصة:
                handlePrivateOrder()
            }
        }else{
            buttonsStackView.isHidden = true
            showRequiredLoginAlert()
        }
    }
    
    
    @IBAction func buttonStateTapped(_ sender: UIButton) {
        guard   let title = sender.currentTitle,
                let status = OrderStatusCases(rawValue: title) else {return}
        switch status {
        case .new,.جديدة:
            handleNewOrder()
        case .current,.حالية:
            handleCurrentOrder()
        case .expired,.منتهية:
            handleExpiredOrder()
        case .privateOrder,.خاصة:
            handlePrivateOrder()
        }
    }
    
    private func handleNewOrder() {
        orderStatus = .new
        updateButtonStates(selectedButton: newButton)
        getOrdersApi(status: orderStatus.rawValue)
    }
    
    private func handleCurrentOrder() {
        orderStatus = .current
        updateButtonStates(selectedButton: currentButton)
        getOrdersApi(status: orderStatus.rawValue)
    }
    
    private func handleExpiredOrder() {
        orderStatus = .expired
        updateButtonStates(selectedButton: expiredButton)
        getOrdersApi(status: orderStatus.rawValue)
    }
    
    private func handlePrivateOrder() {
        orderStatus = .privateOrder
        updateButtonStates(selectedButton: privateButton)
        getOrdersApi(status: orderStatus.rawValue)
    }
    
    private func updateButtonStates(selectedButton: UIButton) {
        UIView.animate(withDuration: 0.3) {
            let buttons = [self.newButton, self.currentButton, self.expiredButton, self.privateButton]
            for button in buttons {
                if button == selectedButton {
                    button?.tintColor = .white
                    button?.backgroundColor = .main
                } else {
                    button?.tintColor = .black
                    button?.backgroundColor = .white
                }
            }
        }
    }
}

//MARK: - tableVeiw

extension MyOrdersVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        ordersModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: OrdersCell.self, for: indexPath)
        if orderStatus == .privateOrder {
            cell.setData(ordersModel[indexPath.row])
        }else{
            cell.configur(data: ordersModel[indexPath.row])
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = ordersModel[indexPath.row].id else { return }
        switch orderStatus {
        case .new,.جديدة:
            pushOrderDetailsViewController(orderId: id, isPrivate: false)
        case .current, .expired,.حالية,.منتهية:
            if ordersModel[indexPath.row].type == "private"{
                pushOrderDetailsViewController(orderId: id, isPrivate: true)
            }else{
                pushOrderDetailsViewController(orderId: id, isPrivate: false)
            }
        case .privateOrder,.خاصة:
            pushOrderDetailsViewController(orderId: id, isPrivate: true)
        }
    }
    
    private func pushOrderDetailsViewController(orderId: Int, isPrivate: Bool) {
        let viewController: UIViewController
        if isPrivate {
            viewController = SpecialOrderDetailsVC.create(orderId: orderId)
        } else {
            viewController = OrdersDetailsVC.create(orderId: orderId)
        }
        
        viewController.hidesBottomBarWhenPushed = true
        push(viewController)
    }
}



//MARK: - Networking

extension MyOrdersVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    private func getOrdersApi(status: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            guard let self = self else {return}
            self.getOrders(status: status)
        }
    }
    private func getOrders(status: String){
        showIndicator()
        Task{
            let endpoint = OrdersEndpoint.getClientOrders(status: status)
            do{
                print(status)
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                ordersModel = response.data?.data ?? []
            }catch{
                print("error calling orders api request : \(error.localizedDescription)")
            }
        }
    }
}







