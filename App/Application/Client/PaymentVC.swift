//
//  PaymentVC.swift
//  App
//
//  Created by Abdo Emad on 15/06/2024.
//

import UIKit

enum payTypes {
    case offer
    case order
}

class PaymentVC: BaseViewController {

    //MARK: - Properties -
    private var orderId: Int!
    private var totalPrice: String?
    private var paymentMethod: String?
    private var offerId: Int!
    private var type: payTypes = .order
    
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var tableView: UITableView!
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var paymentMethodModel: [PaymentMethodModel] = [] {
        didSet{
            tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        getPaymentMethod()
    }

    static func create(orderId :Int, totalPrice: String, offerId : Int, type: payTypes) -> PaymentVC{
        let vc = PaymentVC()
        vc.orderId = orderId
        vc.offerId = offerId
        vc.totalPrice = totalPrice
        vc.type = type
        return vc
    }
    private func setUp(){
        addBackButtonWith(title: "Payment".localized, color: .white)
        price.text = totalPrice
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: PaymentTableViewCell.self)
    }

    @IBAction func confirmButtonTapped(_ sender: Any) {
        switch type {
        case .offer:
            paySpecialOrder(orderId: orderId, paymentMethod: paymentMethod ?? "wallet", offerId: offerId)
        case .order:
            clientOrdersPayment(orderId: orderId, paymentMethod: paymentMethod ?? "wallet")
        }
    }
}

//MARK: - TableView

extension PaymentVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        paymentMethodModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: PaymentTableViewCell.self, for: indexPath)
        cell.setData(paymentMethodModel[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<paymentMethodModel.count {
            paymentMethodModel[i].isSelected = false
        }
        paymentMethodModel[indexPath.row].isSelected = true
        self.paymentMethod = paymentMethodModel[indexPath.row].key
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      65
    }
    
}
//MARK: - Networking

extension PaymentVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getPaymentMethod(){
        Task{
            let endpoint = CartEndpoints.getPaymentMethod()
            do{
                guard let response = try await request(endpoint)else{return}
                paymentMethodModel = response.data ?? [ ]
            }catch{
                
            }
        }
    }
    private func clientOrdersPayment(orderId: Int, paymentMethod: String){
        Task{
            showIndicator()
            let endpoint = OrdersEndpoint.clientOrdersPayment(orderId: orderId, paymentMethod: paymentMethod)
            do{
                guard let response =  try await request(endpoint) else {return}
                hideIndicator()
                if response.key == .success{
                   presentSuccessVC()
                }else{
                    show(errorMessage: response.message)
                }
            }catch{
                print("error calling client Orders Payment requset : \(error.localizedDescription)")
            }
        }
    }
    private func paySpecialOrder(orderId: Int, paymentMethod: String, offerId: Int){
        Task{
            showIndicator()
            let endpoint = OrdersEndpoint.paySpecialOrder(orderId: orderId, paymentMethod: paymentMethod, offerId: offerId)
            do{
                guard let response =  try await request(endpoint) else {return}
                hideIndicator()
                if response.key == .success{
                    show(successMessage: response.message)
                    presentSuccessVC()
                }else{
                    show(errorMessage: response.message)
                }
            }catch{
                print("error calling SpecialOrder Payment requset : \(error.localizedDescription)")
            }
        }
    }
}
//MARK: - Present SuccessView

extension PaymentVC: SuccessProtocol{
    func popOut() {
        let vc = UserTabBarController()
        AppHelper.changeWindowRoot(vc: vc)
    }
    
    private func presentSuccessVC(){
        let vc = SuccessVC.create(delegate: self,
                                  title: "Your order has been paid successfully".localized,
                                  subtitle: "You can follow your order from orders screen".localized,
                                  isModalInPresentation: true
        )
        present(vc, animated: true)
    }
    
}
