//
//  UIKit+UIFont.swift
//  App
//
//  Created by MGAbouarab on 29/12/2023.
//

import UIKit

private func switchFont(form font: UIFont?, to familyName: String) -> UIFont? {
    
    guard let font = font else {return nil}
    let currentFontName = font.fontName.lowercased()
    var fontName = familyName
    
    if currentFontName.range(of: "black") != nil {
        fontName += "-Bold"
    } else if currentFontName.range(of: "bold") != nil {
        fontName += "-Bold"
    } else if currentFontName.range(of: "semibold") != nil {
        fontName += "-SemiBold"
    } else if currentFontName.range(of: "medium") != nil {
        fontName += "-Medium"
    } else if currentFontName.range(of: "regular") != nil {
        fontName += "-Regular"
    }else {
        fontName += "-SemiBold"
    }
    
    return UIFont(name: fontName, size: font.pointSize)
}
private func switchFont(for attributedText: NSAttributedString?, to familyName: String) -> NSAttributedString? {
    
    guard let attributedText = attributedText, attributedText.length > 0 else {return nil}
    var attributes = attributedText.attributes(at: 0, longestEffectiveRange: nil, in: NSMakeRange(0, attributedText.length))
    
    guard let currentFont = (attributes[.font] as? UIFont) else {return attributedText}
    
    let currentFontName = currentFont.fontName.lowercased()
    
    var fontName = familyName
    
    if currentFontName.range(of: "black") != nil {
        fontName += "-Bold"
    } else if currentFontName.range(of: "bold") != nil {
        fontName += "-Bold"
    } else if currentFontName.range(of: "semibold") != nil {
        fontName += "-SemiBold"
    } else if currentFontName.range(of: "medium") != nil {
        fontName += "-Medium"
    } else if currentFontName.range(of: "regular") != nil {
        fontName += "-Regular"
    }else {
        fontName += "-SemiBold"
    }
    
    attributes[.font] = UIFont(name: fontName, size: currentFont.pointSize)
    return NSAttributedString(string: attributedText.string, attributes: attributes)
}


extension UILabel {
    @objc var substituteFontName : String {
        get {
            return self.font.fontName
        }
        set {
            self.font = switchFont(form: self.font, to: newValue)
        }
    }
    @objc var substituteFontNameOfAttribute : String {
        get {
            guard let attributedText = self.attributedText, attributedText.length > 0 else {return ""}
            let attributes = attributedText.attributes(at: 0, longestEffectiveRange: nil, in: NSMakeRange(0, attributedText.length))
            return (attributes[.font] as? UIFont)?.fontName ?? ""
        }
        set {
            self.attributedText = switchFont(for: self.attributedText, to: newValue)
        }
    }
}
extension UITextView {
    @objc var substituteFontName : String {
        get {
            return self.font?.fontName ?? "";
        }
        set {
            self.font = switchFont(form: self.font, to: newValue)
        }
    }
    @objc var substituteFontNameOfAttribute : String {
        get {
            guard let attributedText = self.attributedText, attributedText.length > 0 else {return ""}
            let attributes = attributedText.attributes(at: 0, longestEffectiveRange: nil, in: NSMakeRange(0, attributedText.length))
            return (attributes[.font] as? UIFont)?.fontName ?? ""
        }
        set {
            self.attributedText = switchFont(for: self.attributedText, to: newValue)
        }
    }
}
extension UITextField {
    @objc var substituteFontName : String {
        get {
            return self.font?.fontName ?? "";
        }
        set {
            self.font = switchFont(form: self.font, to: newValue)
        }
    }
    @objc var substituteFontNameOfAttribute : String {
        get {
            guard let attributedText = self.attributedText, attributedText.length > 0 else {return ""}
            let attributes = attributedText.attributes(at: 0, longestEffectiveRange: nil, in: NSMakeRange(0, attributedText.length))
            return (attributes[.font] as? UIFont)?.fontName ?? ""
        }
        set {
            self.attributedText = switchFont(for: self.attributedText, to: newValue)
        }
    }
}

extension UIFont {
    static func overrideSystemFont() {
        UILabel.appearance().substituteFontName = FontNames.family
        UITextView.appearance().substituteFontName = FontNames.family
        UITextField.appearance().substituteFontName = FontNames.family
        
        UITextView.appearance().substituteFontNameOfAttribute = FontNames.family
        UILabel.appearance().substituteFontNameOfAttribute = FontNames.family
        UITextField.appearance().substituteFontNameOfAttribute = FontNames.family
    }
}


