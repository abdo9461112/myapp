//
//  UIFonts.swift
//  App
//
//  Created by MGAbouarab on 29/12/2023.
//

import UIKit

extension UIFont {
    static func printAllFonts() {
        for family in UIFont.familyNames {
            let names = UIFont.fontNames(forFamilyName: family)
            print("Family: \(family) Font names: \(names.json() ?? "\(names)")")
        }
    }
}

extension UIFontDescriptor.AttributeName {
    static let nsctFontUIUsage = UIFontDescriptor.AttributeName(rawValue: "NSCTFontUIUsageAttribute")
}

public extension UIFont {
    
    enum FontNames: String {
        /// change the family name with what you want and this will change all previous font with the new one
        static let family = "URWDINArabic"
        
        case black = "Black"
        case bold = "Bold"
        case semiBold = "Demi"
        case medium = "Medium"
        case regular = "Regular"
        case light = "Light"
        
        var name: String {
            return FontNames.family + "-" + rawValue
        }
        
    }
    
    ///  All Fonts in app
    static func appBlack(size: CGFloat) -> UIFont {
        let name = FontNames.black.name
        guard let font = UIFont(name: name, size: size) else {
            print("🔤 Font Error:: Can't find Font with name \(name), we will provide the system font.")
            return .systemFont(ofSize: size, weight: .bold)
        }
        return font
    }
    static func appBold(size: CGFloat) -> UIFont {
        let name = FontNames.bold.name
        guard let font = UIFont(name: name, size: size) else {
            print("🔤 Font Error:: Can't find Font with name \(name), we will provide the system font.")
            return .systemFont(ofSize: size, weight: .bold)
        }
        return font
    }
    static func appSemiBold(size: CGFloat) -> UIFont {
        let name = FontNames.semiBold.name
        guard let font = UIFont(name: name, size: size) else {
            print("🔤 Font Error:: Can't find Font with name \(name), we will provide the system font.")
            return .systemFont(ofSize: size, weight: .light)
        }
        return font
    }
    static func appMedium(size: CGFloat) -> UIFont {
        let name = FontNames.medium.name
        guard let font = UIFont(name: name, size: size) else {
            print("🔤 Font Error:: Can't find Font with name \(name), we will provide the system font.")
            return .systemFont(ofSize: size, weight: .medium)
        }
        return font
    }
    static func appRegular(size: CGFloat) -> UIFont {
        let name = FontNames.regular.name
        guard let font = UIFont(name: name, size: size) else {
            print("🔤 Font Error:: Can't find Font with name \(name), we will provide the system font.")
            return .systemFont(ofSize: size, weight: .regular)
        }
        return font
    }
    static func appLight(size: CGFloat) -> UIFont {
        let name = FontNames.light.name
        guard let font = UIFont(name: name, size: size) else {
            print("🔤 Font Error:: Can't find Font with name \(name), we will provide the system font.")
            return .systemFont(ofSize: size, weight: .regular)
        }
        return font
    }
    
}


public extension UIFont {
    //MARK:- Objc Methods
    @objc class func myMediumFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: FontNames.medium.name, size: size)!
    }
    @objc class func myRegularFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: FontNames.regular.name, size: size)!
    }
    @objc class func myBoldFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: FontNames.bold.name, size: size)!
    }
    @objc convenience init(myCoder aDecoder: NSCoder) {
        guard
            let fontDescriptor = aDecoder.decodeObject(forKey: "UIFontDescriptor") as? UIFontDescriptor,
            let fontAttribute = fontDescriptor.fontAttributes[.nsctFontUIUsage] as? String else {
            self.init(myCoder: aDecoder)
            return
        }
        var fontName = ""
        switch fontAttribute {
        case "CTFontLightUsage":
            fontName = FontNames.light.name
        case "CTFontRegularUsage":
            fontName = FontNames.regular.name
        case "CTFontEmphasizedUsage", "CTFontBoldUsage":
            fontName = FontNames.bold.name
        case "CTFontHeavyUsage":
            fontName = FontNames.black.name
        case "CTFontMediumUsage":
            fontName = FontNames.medium.name
        default:
            fontName = FontNames.medium.name
        }
        self.init(name: fontName, size: fontDescriptor.pointSize)!
    }
    
    //MARK: - Initialisation
    class func overrideInitialize() {
        guard self == UIFont.self else { return }
        if let systemFontMethod = class_getClassMethod(self, #selector(systemFont(ofSize:))), let mySystemFontMethod = class_getClassMethod(self, #selector(myRegularFont(ofSize:))) {
            method_exchangeImplementations(systemFontMethod, mySystemFontMethod)
        }
        if let boldSystemFontMethod = class_getClassMethod(self, #selector(boldSystemFont(ofSize:))), let myBoldSystemFontMethod = class_getClassMethod(self, #selector(myBoldFont(ofSize:))) {
            method_exchangeImplementations(boldSystemFontMethod, myBoldSystemFontMethod)
        }
        if let italicSystemFontMethod = class_getClassMethod(self, #selector(italicSystemFont(ofSize:))), let myItalicSystemFontMethod = class_getClassMethod(self, #selector(myMediumFont(ofSize:))) {
            method_exchangeImplementations(italicSystemFontMethod, myItalicSystemFontMethod)
        }
        if let initCoderMethod = class_getInstanceMethod(self, #selector(UIFontDescriptor.init(coder:))), let myInitCoderMethod = class_getInstanceMethod(self, #selector(UIFont.init(myCoder:))) {
            method_exchangeImplementations(initCoderMethod, myInitCoderMethod)
        }
        // Trick to get over the lack of UIFont.init(coder:))
    }
}

private extension Collection {

    /// Convert self to JSON String.
    /// Returns: the pretty printed JSON string or an empty string if any error occur.
    func json() -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted])
            return String(data: jsonData, encoding: .utf8)
        } catch {
            print("json serialization error: \(error)")
            return nil
        }
    }
}

