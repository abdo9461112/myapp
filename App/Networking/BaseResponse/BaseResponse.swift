//
//  BaseResponse.swift
//  App
//
//  Created by MGAbouarab on 22/12/2023.
//

import Foundation

struct BaseResponse<T: Decodable>: Decodable {
    var key: ServerResponseKey
    var message: String
    var data: T?
    
    enum CodingKeys: String, CodingKey {
        case key
        case message = "msg"
        case data
    }
    
}

struct Paginate: Codable {
    var totalItems, countItems, perPage, totalPages: Int?
    var currentPage: Int?
    var nextPageURL, pervPageURL: String?
    
    enum CodingKeys: String, CodingKey {
        case totalItems = "total_items"
        case countItems = "count_items"
        case perPage = "per_page"
        case totalPages = "total_pages"
        case currentPage = "current_page"
        case nextPageURL = "next_page_url"
        case pervPageURL = "perv_page_url"
    }
}
