//
//  Typealias.swift
//  App
//
//  Created by MGAbouarab on 21/12/2023.
//

import Foundation
import Alamofire

typealias RequestConvertible = URLRequestConvertible
typealias URLRequestEncoding = URLEncoding
typealias JSONRequestEncoding = JSONEncoding
typealias RequestMethod = HTTPMethod
typealias APIParameters = [String: Any?]
