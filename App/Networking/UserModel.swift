//
//  UserModel.swift
//  App
//
//  Created by Abdo Emad on 08/06/2024.
//

import Foundation

struct UserModel: Codable{
    
    var id: Int?
    var name: String?
    var countryCode: String?
    var phone: String?
    var image: String?
    var email: String?
    var token: String?
    var city: City?
    var walletBalance: Double?
    var accountType: String?
    var isNotify: Bool?
    var isActive: Bool?
    var locationText: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case id
        case email
        case countryCode = "country_code"
        case phone
        case image
        case token
        case city
        case walletBalance = "wallet_balance"
        case accountType = "account_type"
        case isNotify  = "is_notify"
        case isActive = "is_actvie"
    }
}



// MARK: - City
struct City: Codable, DropDownItem {
    var id: Int
    var name: String
}
