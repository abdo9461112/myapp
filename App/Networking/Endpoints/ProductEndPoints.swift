//
//  ProductEndPoints.swift
//  App
//
//  Created by Abdo Emad on 10/06/2024.
//

import Foundation

struct ProductEndPoints {
     init() {}
}

extension ProductEndPoints {
    static
    func getProviderProducts(id:Int, filter:String?) -> Endpoint<BaseResponse<ProviderProductsModel>>{
        return.init(method: .post,
                    path: "client/provider/products",
                    body: [
                        "provider_id" : id,
                        "filter[name]":filter
                    ]
        )
    }
    
    static
    func addProductToFav(productId: Int) -> Endpoint<BaseResponse<FavouritesActionModel>>{
        return .init(method: .post,
                     path: "client/products/favorite",
                     body: [
                        "product_id":productId
                     ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    
   
    static
    func clientProduct(id:Int) -> Endpoint<BaseResponse<ProviderProductDetailsModel>>{
        return .init(method: .post,
                     path: "client/product",
                     body: [
                    "product_id":id
                     ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
}
 
