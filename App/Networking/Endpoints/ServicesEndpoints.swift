//
//  ServicesEndpoints.swift
//  App
//
//  Created by Abdo Emad on 11/06/2024.
//

import Foundation

struct ServicesEndpoints {
     init() {}
}

extension ServicesEndpoints {
    
    static
    func getProviderServices(id:Int, filter:String?) -> Endpoint<BaseResponse<ProviderProductsModel>>{
        return.init(method: .post,
                    path: "client/provider/services",
                    body: [
                        "provider_id" : id,
                        "filter":filter
                    ]
        )
    }
    static
    func addServiceToFav(serviceId: Int) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return .init(method: .post,
                     path: "client/services/favorite",
                     body: [
                        "service_id":serviceId
                     ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    
    static
    func clientService(serviceId:Int) -> Endpoint<BaseResponse<ServiceDetailsModel>>{
        return .init(method: .post,
                     path: "client/service",
                     body: [
                        "service_id":serviceId
                     ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
}
 


