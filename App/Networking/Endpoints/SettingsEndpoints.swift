//
//  SettingsEndpoints.swift
//  App
//
//  Created by Abdo Emad on 1/06/2024.
//

import Foundation
import UIKit

struct SettingsEndpoints {
    init() {}
}

extension SettingsEndpoints {
    static
    func 
    intro() -> Endpoint<BaseResponse<[OnboardingModel]>> {
        return .init(
            path: "client/splash"
        )
    }
    
    
    static
    func Terms() -> Endpoint<BaseResponse<String>>{
        return .init(
            method:.get,
            path: "conditions"
        )
    }
    static
    func getProfile() -> Endpoint<BaseResponse<UserModel>>{
        return .init(
            method: .post,
            path: "client/profile",
            headerType: .authorized(token: UserDefaults.accessToken ?? "")
        )
    }
    
    static 
    func about() -> Endpoint<BaseResponse<String>>{
        return .init(method: .get,
                     path: "about")
    }
    static 
    func getQuestions() -> Endpoint<BaseResponse<[FaqsModel]>>{
        return .init(method: .get,
                     path: "questions")
    }
    
    static 
    func contactUs(name: String, phone: String, subject: String, messgae: String, country_code: String) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return .init(method:.post,
                     path: "contact-us",
                     body: [
                        "name": name,
                        "phone": phone,
                        "subject": subject,
                        "message":messgae,
                        "country_code": country_code
                     ]
        )
        
    }
    
    static 
    func updateProfile(name: String, email: String, image: Data? , cityId:Int) -> Endpoint<BaseResponse<UserModel>>{
        var uploadData = [UploadData]()
        if let image = image {
            uploadData.append(UploadData(key: "image", data: image, mimeType: .jpg))
        }
        
        
        var body: [String: Any]  =
        [
            "name": name,
            "email":email,
            "city_id":cityId
        ]
       
        
        return .init(method: .post,
                     path: "client/profile/update",
                     body:body,
                     headerType: .authorized(token: UserDefaults.accessToken ?? ""),
                     uploads: uploadData
        )
    }
    
    static 
    func delete()-> Endpoint<BaseResponse<EmptyResponseData>>{
        return .init(method: .post,
                     path: "client/profile/delete",
                     headerType: .authorized(token: UserDefaults.accessToken ?? "")
        )
    }
    static
    func changeNotification() -> Endpoint<BaseResponse<EmptyResponseData>>{
        return .init(method: .post,
                     path: "notifications/change",
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    static
    func changeLang(lang:String) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return.init(method: .post,
                    path: "language/change",
                    body: [
                        "lang": lang,
                    ],
                    headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    
    static
    func getClientFavorites(type: String) -> Endpoint<BaseResponse<ProviderProductsModel>>{
        return .init(method: .post,
                     path: "client/favorites",
                     body:
                        [
                        "type": type
                        ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    static
    func getWalletBalance() -> Endpoint<BaseResponse<WalletBalanceData>>{
        return .init(method: .post,
                     path: "wallet/balance",
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
}


