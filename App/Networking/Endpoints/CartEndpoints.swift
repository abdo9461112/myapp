//
//  ProductEndPoints.swift
//  App
//
//  Created by Abdo Emad on 9/06/2024.
//

import Foundation

struct CartEndpoints {
    init() {}
}

extension CartEndpoints {
    
    static
    func clientCartCheckQuantity(id:Int) -> Endpoint<BaseResponse<CheckQuantityModel>>{
        return.init(method: .post,
                    path: "client/cart/check-quantity",
                    body: [
                        "product_id":id
                    ],
                    headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    static
    func addProdcutToCart(id:Int,quantity:Int?) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return.init(method: .post,
                    path: "client/cart/product/add",
                    body: [
                        "product_id":id,
                        "quantity":quantity
                    ],
                    headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    static
    func addServiceToCart(id:Int) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return.init(method: .post,
                    path: "client/cart/service/reservation",
                    body: [
                        "service_id":id,
                    ],
                    headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    
    static
    func getClientCart() -> Endpoint<BaseResponse<CartModel>>{
        return.init(method: .post,
                    path: "client/carts",
                    headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    static
    func deleteClientCart(cartId:Int) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return.init(method: .post,
                    path: "client/cart/delete",
                    body:
                        [
                            "cart_id": cartId
                        ],
                    headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    static
    func getClientCartDetails(cartId:Int) -> Endpoint<BaseResponse<CartDetailsModel>>{
        return.init(method: .post,
                    path: "client/cart/info",
                    body:
                        [
                            "cart_id": cartId
                        ],
                    headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    static
    func increaseProductQuantity(cartId:Int,itemId:Int) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return.init(method: .post,
                    path: "client/cart/quantity/increase",
                    body:
                        [
                            "cart_id":cartId,
                            "item_id":itemId
                        ],
                    headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    static
    func decreaseProductQuantity(cartId:Int,itemId:Int) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return.init(method: .post,
                    path: "client/cart/quantity/decrease",
                    body:
                        [
                            "cart_id":cartId,
                            "item_id":itemId
                        ],
                    headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    static
    func deleteClientItem(itemId:Int) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return.init(method: .post,
                    path: "client/cart/items/deletet",
                    body:
                        [
                            "item_id": itemId
                        ],
                    headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    static
    func getPaymentMethod() -> Endpoint<BaseResponse<[PaymentMethodModel]>>{
        return .init(method: .get,
                     path: "payment-methods")
    }
    static
    func clientOrdersStore(providerId: Int, cartId: Int, paymentMethod: String, note: String, contactPhone: String, deliveryMethod: String, userReceiveLat: String?, userRecieveLng: String?, userReceivingAddress: String?, receivingTime: String, financialTotal: String, financialTax: String, financialApp: String, financialDeliveryPrice: String, financialFinalTotal: String, delegateRate: Double, deliveryCost: Double) -> Endpoint<BaseResponse<EmptyResponseData>>{
        
        let body: [String: Any] = [
                "provider_id": providerId,
                "cart_id": cartId,
                "payment_method": paymentMethod,
                "notes": note ,
                "contact_phone": contactPhone,
                "delivery_method": deliveryMethod,
                "user_receving": [
                    "lat": userReceiveLat ?? "",
                    "lng": userRecieveLng ?? "",
                    "address": userReceivingAddress ?? ""
                ],
                "receiving_time": receivingTime,
                "financial": [
                    "total": financialTotal,
                    "tax": financialTax,
                    "app": financialApp,
                    "delivery_price": financialDeliveryPrice,
                    "final_total": financialFinalTotal,
                    "delegate_rate": delegateRate,
                    "delivery_cost": deliveryCost
                ]
            ]
        return .init(method: .post,
                     path: "client/orders/store",
                     body:body,
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
        
    }
}

