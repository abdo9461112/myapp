//
//  HomeEndPoints.swift
//  App
//
//  Created by Abdo Emad on 5/06/2024.
//


import Foundation

struct HomeEndPoints {
     init() {}
}

extension HomeEndPoints {
    
    static
    func updateLocation(lat: Double?, long: Double?, address: String?) -> Endpoint<BaseResponse<EmptyResponseData>> {
        return .init(
            method: .post,
            path: "location/update",
            body: [
                "lat": lat,
                "long": long,
                "address": address
            ]
        )
    }
    
    static
    func getProvidersCategories(category_id: Int?, name: String?, bestSell: Int?, distance: Int?, new: Int?, rate: String?)-> Endpoint<BaseResponse<ProviderDataModel>>{
        return .init(method:.post,
                     path: "client/category/providers",
                     body:
                        ["category_id" : category_id,
                         "name": name,
                         "best_sell": bestSell,
                         "distance": distance,
                         "new": new,
                         "rate":rate
                        ]
        )
    }
    
    static
    func clientSearch(category_id: Int?, name: String?, bestSell: Int?, distance: Int?, new: Int?, rate: String?)-> Endpoint<BaseResponse<ProviderDataModel>>{
        return .init(method:.post,
                     path: "client/search",
                     body:
                        ["category_id" : category_id,
                         "name": name,
                         "best_sell": bestSell,
                         "distance": distance,
                         "new": new,
                         "rate":rate
                        ]
        )
    }
    
    
    static
    func getHomeSliders() -> Endpoint<BaseResponse<HomeModel>>{
        return .init(method: .post,
                     path: "client/home")
    }
    
    
    static
    func getCategories() -> Endpoint<BaseResponse<[CategoriesModel]>>{
        return .init(
            path: "categories")
        
    }
    static
    func getProvider(id: Int) -> Endpoint<BaseResponse<ProviderDetailsModel>>{
        return .init(method: .post,
                     path: "client/provider",
                     body: [
                        "provider_id" : id
                     ]
        )
    }
    static
    func getNotifications() -> Endpoint<BaseResponse<NotificationModel>>{
        return .init(path: "notifications",
                     headerType: .authorized(token: UserDefaults.accessToken ?? "")
        )
    }
    static
    func deleteNotifications() -> Endpoint<BaseResponse<EmptyResponseData>>{
        return .init(method: .post
                     ,path: "notifications/delete",
                     headerType: .authorized(token: UserDefaults.accessToken ?? "")
        )
    }
    static
    func deleteOneNotifications(id:String) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return .init(method: .post
                     ,path: "notification/delete",
                     body: 
                        [
                            "id":id
                        ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? "")
        )
    }
}
