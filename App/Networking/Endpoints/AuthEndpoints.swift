//
//  AuthEndpoints.swift
//  App
//
//  Created by Abdo Emad on 1/06/2024.
//

import Foundation

struct AuthEndpoints {
    init() {}
    
    
}
struct EmptyResponseData: Decodable {}

extension AuthEndpoints {
    
    static
    func login(phone: String, countryCode: String) -> Endpoint<BaseResponse<UserModel>> {
        return .init(
            method: .post,
            path: "client/login",
            body: [
                "user_name": "phone",
                "country_code": countryCode,
                "phone": phone,
                "device_id": "UserDefaults.pushNotificationToken",
                "device_type": "ios",
                "type":"login"
            ]
        )
    }
    static
    func verifyCode(phone: String, code: String , countryCode: String , type: String) -> Endpoint<BaseResponse<UserModel>> {
        return.init(
            method: .post,
            path: "client/verify/code",
            body: [
                "user_name": UserDefaults.user?.name ?? " ",
                "country_code":countryCode,
                "phone": phone,
                "device_id": "UserDefaults.pushNotificationToken",
                "device_type": "ios",
                "type": type,
                "code": code
            ]
        )
    }
    
    static
    func verifyPhone(phone: String, code: String , countryCode: String , type: String) -> Endpoint<BaseResponse<UserModel>> {
        return.init(
            method: .post,
            path: "client/verify/code",
            body: [
                "user_name": UserDefaults.user?.name ?? " ",
                "country_code":countryCode,
                "phone": phone,
                "device_id": "UserDefaults.pushNotificationToken",
                "device_type": "ios",
                "type": type,
                "code": code
            ],
            headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
        
    }
    static
    func resendCode(phone: String, countryCode: String, type : String) -> Endpoint<BaseResponse<EmptyResponseData>> {
        return Endpoint(
            method: .post,
            path: "client/code/resend",
            body: [
                "phone": phone,
                "country_code": countryCode,
                "type" : type,
                "device_id":  UserDefaults.pushNotificationToken ?? "no device id for firebase for this device and this is an ios device",
                "device_type": "ios"
            ]
        )
    }
    
    static
    func signUp(name: String, image: Data?, phone: String, email: String, city_id: Int, countryCode: String, is_conditions: Int) -> Endpoint<BaseResponse<UserModel>> {
        
        func uploadImageMultipart() -> [UploadData] {
            var data: [UploadData] = []
            if let imageData = image {
                data.append(UploadData(key: "image", data: imageData, mimeType: .jpg, fileName: "image.jpg"))
            }
            return data
        }
        
        var body: [String: Any] = [
            "country_code": countryCode,
            "phone": phone,
            "name": name,
            "email": email,
            "city_id": city_id,
            "is_conditions": is_conditions
        ]
        
        // Append image data to the body if it exists
        if let imageData = image {
            body["image"] = imageData.base64EncodedString()
        }
        
        return Endpoint(
            method: .post,
            path: "client/register",
            body: body,
            uploads: uploadImageMultipart()
        )
    }
    
    static
    func activateAccount(phone: String,code : String, countryCode: String) -> Endpoint<BaseResponse<UserModel>>{
        return .init(
            method: .post,
            path: "client/active/account",
            body:  [
                "code": code,
                "country_code": countryCode,
                "phone": phone,
                "device_id": "UserDefaults.pushNotificationToken",
                "device_type": "ios",
                "type":"active"
            ]
            
        )
        
    }
    
    static
    func getCities()-> Endpoint<BaseResponse<[City]>> {
        return.init(
            method: .get,
            path: "cities")
    }
    
    static
    func getCountryCode() -> Endpoint<BaseResponse<[CountryCodeModel]>>{
        return.init(path: "countries")
    }
    
    static
    func clientLogout() -> Endpoint<BaseResponse<EmptyResponseData>>{
        return.init(
            method: .post,
            path: "client/logout",
            body:
                [
                    "device_id":  UserDefaults.pushNotificationToken ?? "no device id for firebase for this device and this is an ios device",
                    "device_type": "ios"
                ],
            headerType: .authorized(token: UserDefaults.accessToken ?? "")
        )
        
    }
    static
    func clientRequestChangePhone() -> Endpoint<BaseResponse<EmptyResponseData>>{
        return .init(method: .post,
                     path: "client/request/change/phone",
                     headerType:.authorized(token: UserDefaults.accessToken ?? "")
        )
    }
    static
    func clientRegisterPhone(countryCode: String , newPhone: String) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return.init(method: .post,
                    path: "client/register/phone",
                    body: [
                        "country_code": countryCode,
                        "phone": newPhone,
                        "device_id":  UserDefaults.pushNotificationToken ?? "no device id for firebase for this device and this is an ios device",
                        "device_type": "ios"
                    ],
                    headerType: .authorized(token: UserDefaults.accessToken ?? " ")
                    
        )
    }
}


