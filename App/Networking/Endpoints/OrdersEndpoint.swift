//
//  OrdersEndpoint.swift
//  App
//
//  Created by Abdo Emad on 13/06/2024.
//

import Foundation
struct OrdersEndpoint{
    init(){ }
}

extension OrdersEndpoint{
    static
    func clientOrderCancel(orderId:Int) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return .init(method: .post,
                     path: "client/orders/cancel",
                     body:
                        [
                            "order_id":orderId,
                        ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? "")
        )
    }
    static
    func getClientOrders(status: String) -> Endpoint<BaseResponse<OrdersModel>>{
        return .init(method: .post,
                     path: "client/orders",
                     body:
                        [
                            "status":status,
                        ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? "")
        )
    }
    static
    func storePrivateOrder(name:String,receivingTime:String,address:String,price:String,notes:String,categoryId:Int) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return .init(method: .post,
                     path: "client/orders/private/store",
                     body:
                        [
                            "name":name,
                            "receiving_time":receivingTime,
                            "address":address,
                            "total":price,
                            "notes":notes,
                            "category_id":categoryId
                        ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    static
    func getOrderDetials(orderId: Int) -> Endpoint<BaseResponse<OrderDetailsModel>> {
        return .init(method: .post,
                     path: "client/orders/show",
                     body:
                        [
                            "order_id":orderId
                        ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
                     
        )
    }
    static
    func clientOrderStatusChange(orderId: Int,status:String) -> Endpoint<BaseResponse<EmptyResponseData>> {
        return .init(method: .post,
                     path: "client/orders/status/change",
                     body:
                        [
                            "order_id":orderId,
                            "status":status
                        ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
                     
        )
    }
    
    static
    func getOffers(orderId: Int) -> Endpoint<BaseResponse<OfferPricesModel>> {
        return .init(method: .post,
                     path: "client/orders/offers",
                     body:
                        [
                            "order_id":orderId
                        ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
                     
        )
    }
    static
    func paySpecialOrder(orderId:Int,paymentMethod:String,offerId:Int) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return .init(method:.post,
                     path:"client/orders/payment",
                     body:
                        [
                            "order_id":orderId,
                            "payment_method":paymentMethod,
                            "offer_id":offerId
                        ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
   static
    func clientOrdersPayment(orderId:Int,paymentMethod:String) -> Endpoint<BaseResponse<EmptyResponseData>>{
        return .init(method:.post,
                     path:"client/orders/payment",
                     body:
                        [
                            "order_id":orderId,
                            "payment_method":paymentMethod,
                        ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    static
    func clientOrdersInvoice(orderId:Int) -> Endpoint<BaseResponse<InvoiceModel>>{
        return .init(method:.post,
                     path:"client/orders/invoice",
                     body:
                        [
                            "order_id":orderId,
                        ],
                     headerType: .authorized(token: UserDefaults.accessToken ?? " ")
        )
    }
    
    static
    func addReview(orderId: Int, providerId: Int, providerValue: Double, providerComment: String?, delegateId: Int?, delegateValue: Double?, delegateComment: String?) -> Endpoint<BaseResponse<EmptyResponseData>> {
        var body: [String: Any] = [:]
                
                let providerRating : Rateing = .init(value: "\(providerValue)", comment: providerComment, id: providerId)
                body["provider"] = try? providerRating.asDictionary()
                
                if let delegateId = delegateId, let delegateValue = delegateValue {
                    let delegateRating : Rateing = .init(value: "\(delegateValue)", comment: delegateComment, id: delegateId)
                    body["delegate"] = try? delegateRating.asDictionary()

                }else{
                    let providerRating : Rateing = .init(value: "\(providerValue)", comment: providerComment, id: providerId)
                    body["provider"] = try? providerRating.asDictionary()
                }
                body["order_id"] =  orderId

            
            return .init(
                method: .post,
                path: "client/orders/reviews/store",
                body: body,
                headerType: .authorized(token: UserDefaults.accessToken ?? " ")
            )
        }
    }


struct Rateing: Codable {
    let value: String
    let comment: String?
    let id: Int
    
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
        guard let dictionary = json as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}
