//
//  NetworkExecuter.swift
//  App
//
//  Created by MGAbouarab on 22/12/2023.
//

import Foundation

protocol NetworkExecuter {
    func execute(_ request: any Requestable, progress progressHandler: ((_ progress: Int)-> Void)?) async throws -> Data
}


