//
//  AppConfiguration.swift
//  App
//
//  Created by MGAbouarab on 16/01/2024.
//

import Foundation

struct AppConfiguration {
    
    private init() {}
    
    static var allowSpecialOrder: Bool = false
    
}
