//
//  AppDelegate.swift
//  App
//
//  Created by MGAbouarab on 20/12/2023.
//

import UIKit
import GoogleMaps
import GooglePlaces
import FirebaseCore
import FirebaseMessaging
import IQKeyboardManagerSwift
import Network

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    //MARK: - Properties -
    let reachability: Reachability = Reachability()
    let gcmMessageIDKey = "gcm.message_id"
    let googleMapKey = "AIzaSyBPGGgV-dTkfNUN5e6HAOOrH84lZDcvbPE"

    //MARK: - Initializer -
    override init() {
        super.init()
        UIFont.overrideInitialize()
        UIFont.overrideSystemFont()
    }
    
    //MARK: - Application Lifecycle -
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //MARK: - Networking -
        self.startObserveNetworkChange()
        
        //MARK: - Keyboard -
        self.handleKeyboard()
        
        //MARK: - Google Map Key -
        self.handleGoogleMap()
        

        Language.handleViewDirection()
        //MARK: - FCM -
        let monitor = NWPathMonitor()
        let queue = DispatchQueue(label: "NetworkMonitor")
        monitor.start(queue: queue)
        
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                DispatchQueue.main.async {
                    self.handleFCMFor(application)
                    queue.suspend()
                }
            } else {
                print("No network connection")
            }
        }
        
        return true
    }

    // MARK: - UISceneSession Lifecycle -
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {}

}

extension AppDelegate {
    
    //MARK: - FCM -
    private func handleFCMFor(_ application: UIApplication) {
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
    }
    
    //MARK: - Networking -
    private func startObserveNetworkChange() {
        reachability.listen { [weak self] status in
            switch status {
            case .unknown:
                break
            case .notReachable:
                ConnectionAlertManager.shared.showConnectionLost()
            case .reachable(_):
                guard self?.reachability.lastStatus == .notReachable else {return}
                ConnectionAlertManager.shared.showConnectionRestored()
            }
        }
    }
    
    //MARK: - Google Maps -
    private func handleGoogleMap() {
        GMSServices.provideAPIKey(self.googleMapKey)
    }
    
    //MARK: - Keyboard -
    private func handleKeyboard() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarConfiguration.tintColor = .main
        IQKeyboardManager.shared.resignOnTouchOutside = true
//        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(BaseViewController.self)
    }
    
    //MARK: - FackData -
    private func setFakeData() {
        #if DEBUG
        UserDefaults.isLogin = false
        UserDefaults.accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiMTIzNGUyMzJlNTE0ZmRiNmI5YmY1YThkYjZjMzE1ZTM1YWY5NWYwMmQ0YWMwYzMzNWFhMTdhN2VkNmU0OGRjY2NiN2MyZDdjODE0NDM2Y2QiLCJpYXQiOjE3MDUwNjYwOTEuMzc4NDc3MDk2NTU3NjE3MTg3NSwibmJmIjoxNzA1MDY2MDkxLjM3ODQ3OTAwMzkwNjI1LCJleHAiOjE3MzY2ODg0OTEuMzc3MTk5ODg4MjI5MzcwMTE3MTg3NSwic3ViIjoiNzc0MjkiLCJzY29wZXMiOltdfQ.qhX6XuqK0yLNyNORNYWhIqxVIoSaSunPGRHEMs_iMur9O5otfUCAVVYWiXMcM166rNZe6sY4gtwyXTgJZsOKVGaaCuhxutJWuCdJhRzmfjCrMHAIT-s805F3CLh2d0BkYWHzpgrhkmh7nYVLvC1oTcG5gJ3rUQKYYHBuS3U5a2KQ2S7O893olnsBtZwvXsnSmQn5q26lnzgEPAqCtcEcx9XOa--PtSceUFqKU0T3pD9XRWdoQuigjt8R9CZ06_54UllBu6ZV-O1tuj4Cky33WapOfkeGvSD0FFaj-wKpPfTiAVXf5SnYF9eqGmaEyZLyYMexevfCsxu3etD7-ELxaSO_Sbi9LR30xU-ITclELZ94Zf-nz2LVGshUDDHGUyiQ-YA0RgWHP-zovWZdxZAPXKIq_cFEip1Mv3G6ue7Pii1MOJ59maqmDLpEI67Pfk9TLPRI6os-c_zxErvffQDxhE8skTqB0jzny1DNmg9p-VqUHTND5ZfSe2t07xvoFZDRW56syu93BmT0ZGi8nB4cl9_Wd_SugI-mxpZYFIJIac3_TepGpqnXWxvfpLTOz6_fsQRyLCdu3Rn5rREEDJPvP4rIRQgoLMxE1Wbhbqw4V8OEviE2nJvPxdxQt-CWVGtTp75iH9YDo-MTq4FAiS_DrL2J0HMF2hUHSpt57JKR9eQ"
        #endif
    }
    
}
