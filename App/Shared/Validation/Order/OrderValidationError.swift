//
//  OrderValidationError.swift
//  App
//
//  Created by MGAbouarab on 11/01/2024.
//

import Foundation

enum OrderValidationError: Error {
    case emptyOrderDetails
    case emptyOrderPaymentType
    case emptyOrderStartLocation
    case emptyOrderEndLocation
    case emptyOrderDurationTime
}

extension OrderValidationError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .emptyOrderDetails:
            return "OrderValidationError_emptyOrderDetails".localized
        case .emptyOrderPaymentType:
            return "OrderValidationError_emptyOrderPaymentType".localized
        case .emptyOrderStartLocation:
            return "OrderValidationError_emptyOrderStartLocation".localized
        case .emptyOrderEndLocation:
            return "OrderValidationError_emptyOrderEndLocation".localized
        case .emptyOrderDurationTime:
            return "OrderValidationError_emptyOrderDurationTime".localized
        }
    }
}
