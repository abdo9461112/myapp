//
//  TokenValidationErrors.swift
//  App
//
//  Created by MGAbouarab on 12/01/2024.
//

import Foundation

enum TokenValidationErrors: Error {
    case unAuthorized
}
