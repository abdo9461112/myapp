//
//  CGColor.swift
//  App
//
//  Created by MGAbouarab on 27/12/2023.
//

import UIKit

extension CGColor {
    
    static var mainCGColor: CGColor {
        return UIColor.main.cgColor
    }
//    static var mainWithAlpha: CGColor {
//        let x = UIColor.mainWithAlpha
//        return x.cgColor
//    }
//    
//    static var secondary: CGColor {
//        let x = UIColor.secondary
//        return x.cgColor
//    }
//    static var secondaryWithAlpha: CGColor {
//        let x = UIColor.secondaryWithAlpha
//        return x.cgColor
//    }
    
}
