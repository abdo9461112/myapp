//
//  UIImageView.swift
//  App
//
//  Created by MGAbouarab on 29/12/2023.
//

import UIKit.UIImageView
import Kingfisher

extension UIImageView {
    func setWith(_ stringURL: String?) {
        self.kf.indicatorType = .activity
        let placeholder = UIImage(named: "AppIcon")
        guard let stringURL, let url = URL(string: stringURL) else {
            self.image = UIImage(named: "AppIcon")
            return
        }
        self.kf.setImage(
            with: url,
            placeholder: placeholder,
            options: [
                .onFailureImage(placeholder),
                .transition(.fade(0.4))
            ]
        )
    }
}
