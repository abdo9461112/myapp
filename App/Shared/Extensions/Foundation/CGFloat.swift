//
//  CGFloat.swift
//  App
//
//  Created by MGAbouarab on 27/12/2023.
//

import Foundation

extension CGFloat {
    static let buttonCorner: CGFloat = 16
    static let cardCorner: CGFloat = 16
    static let smallCardCorner: CGFloat = 8
    static let sheetCorner: CGFloat = 30
    static let defaultBorderWidth: CGFloat = 1
}
