//
//  String.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import Foundation

extension String {
    func trimWhiteSpace() -> String{
        let newValue = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return newValue
    }
    func isValidEmail() -> Bool {
        let testEmail = NSPredicate(format:"SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")
        return testEmail.evaluate(with: self)
    }
    func isValidPhoneNumber(pattern: String) -> Bool {
        return self.range(of: pattern ,options: .regularExpression) != nil
    }
}
