//
//  Double.swift
//  App
//
//  Created by MGAbouarab on 26/01/2024.
//

import Foundation

extension Double {
    var toPrice: String {
        return String(format: "%.2f", self).replacingOccurrences(of: ".00", with: "") + " " + "App_Currency".localized
    }
}
