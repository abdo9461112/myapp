//
//  NearProvidersView.swift
//  App
//
//  Created by MGAbouarab on 06/01/2024.
//

import UIKit

class NearProvidersView: UIView {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var addressLabel: UILabel!
    @IBOutlet weak private var addressView: UIView!
    @IBOutlet weak private var categoryLabel: UILabel!
    @IBOutlet weak private var rateView: SmallDetailsWithImageView!
    @IBOutlet weak private var distanceView: SmallDetailsWithImageView!
    @IBOutlet weak private var statusView: UIView!
    @IBOutlet weak private var statusLabel: UILabel!
    
    //MARK: - Initializer -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "NearProvidersView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.categoryLabel.isHidden = true
        self.addressView.isHidden = true
    }
    
    //MARK: - Data -
    func setStatus(isOpen: Bool) {
        self.statusView.backgroundColor = isOpen ? .systemGreen : .systemRed
        self.statusLabel.text = isOpen ? "Open_Status".localized : "Closed_Status".localized
    }
    func set(image imageURL: String, forProviderName providerName: String?) {
        self.imageView.setWith(imageURL)
        self.nameLabel.text = providerName
    }
    func set(address: String) {
        self.addressView.isHidden = false
        self.addressLabel.text = address
    }
    func set(categoryName: String) {
        self.categoryLabel.isHidden = false
        self.categoryLabel.text = categoryName
    }
    func set(rating: String, distance: String) {
        self.rateView.set(details: rating)
        self.distanceView.set(details: distance)
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
}

