//
//  DropDownTextField.swift
//  App
//
//  Created by Abdo Emad on 10/06/2024.
//

protocol DropDownTextFieldDelegate {
    func dropDownList(for textField: UITextField) -> [DropDownItem]
    func didSelect(item: DropDownItem, for textField: UITextField)
    
}

import UIKit
class DropDownTextField: UITextField {
    
    //MARK: - Properties -
    private lazy var picker = UIPickerView()
    private var items: [DropDownItem] = []
    private var selectedItem: DropDownItem?
    var dropDownDelegate: DropDownTextFieldDelegate?
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) || action == #selector(UIResponderStandardEditActions.cut(_:)) || action == #selector(UIResponderStandardEditActions.delete(_:)) || action == #selector(UIResponderStandardEditActions.select(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    //MARK: - Lifecycle -
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupDesign()
    }
    
    private func setupDesign() {
        self.picker.delegate = self
        self.inputView = picker
        self.inputView?.clipsToBounds = true
        tintColor = .new
        self.addDoneButtonOnKeyboard()
    }
    
    
    private func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        doneToolbar.tintColor = .main
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done".localized, style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        guard let selectedItem = self.selectedItem else {
            if let item = self.items.first {
                self.text = item.name
                self.dropDownDelegate?.didSelect(item: item, for: self)
            }
            self.resignFirstResponder()
            return
        }
        self.text = selectedItem.name
        self.dropDownDelegate?.didSelect(item: selectedItem, for: self)
        self.resignFirstResponder()
    }
    
}
extension DropDownTextField: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        self.items = self.dropDownDelegate?.dropDownList(for: self) ?? []
        return self.items.count
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return NSAttributedString(string: self.items[row].name, attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard row < self.items.count else {return}
        self.selectedItem = self.items[row]
//        self.doneButtonAction()
    }
}

