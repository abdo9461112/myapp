//
//  PhoneNumberTextFieldView+Localization.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import Foundation

extension String {
    var phoneNumberLocalizable: String {
        return NSLocalizedString(self, tableName: "PhoneNumberLocalizable", bundle: Bundle.main, value: "", comment: "")
    }
}

