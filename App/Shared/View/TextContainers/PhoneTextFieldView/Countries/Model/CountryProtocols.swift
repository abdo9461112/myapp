//
//  CountryProtocols.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import Foundation

protocol CountryCodeItem {
    var id: Int? {get}
    var name: String {get}
    var flag: String? {get}
    var emoji: String? {get}
    var countryCode: String? {get}
}
protocol CountryCodeDelegate {
    func didSelectCountry(_ item: CountryCodeModel)
}


