//
//  CountryModel.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import Foundation


struct CountriesModel: Codable {
    let countries: [Country]?
}

// MARK: - Country
struct Country: Codable {
    let id: Int
    let name: String
    let currency, currencyCode, currencyCodeAr: String?
    let currencyCodeEn, example, iso2, iso3: String?
    let callingCode: String?
    let flag: String?

    enum CodingKeys: String, CodingKey {
        case id, name, currency
        case currencyCode = "currency_code"
        case currencyCodeAr = "currency_code_ar"
        case currencyCodeEn = "currency_code_en"
        case example, iso2, iso3
        case callingCode = "calling_code"
        case flag
    }
}

struct CountryCodeModel: Codable {
    var id: Int?
    var name: String
    var key: String?
    var image: String?
}
