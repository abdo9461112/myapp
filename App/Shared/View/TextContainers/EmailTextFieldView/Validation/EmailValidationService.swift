//
//  EmailValidationService.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import Foundation

struct EmailValidationService {
    
    static func validate(email: String?) throws -> String {
        guard let email = email, !email.trimWhiteSpace().isEmpty else {
            throw EmailValidationError.empty
        }
        guard email.isValidEmail() else{
            throw EmailValidationError.wrong
        }
        return email
    }
    
}

