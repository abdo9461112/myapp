//
//  EmailValidationError.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import Foundation

enum EmailValidationError: Error {
    case empty
    case wrong
}

extension EmailValidationError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .empty:
            return "Please enter email field.".emailLocalizable
        case .wrong:
            return "Please enter correct email address.".emailLocalizable
        }
    }
}

