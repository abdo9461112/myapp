//
//  DropDownTextFieldViewProtocols.swift
//  App
//
//  Created by MGAbouarab on 10/01/2024.
//

import Foundation

protocol DropDownTextFieldViewDelegate: AnyObject {
    func dropDownList(for textFieldView: DropDownTextFieldView) -> [DropDownItem]
    func didSelect(item: DropDownItem, for textFieldView: DropDownTextFieldView)
}
protocol DropDownTextFieldViewListDelegate: AnyObject {
    func didSelect(item: DropDownItem)
}

