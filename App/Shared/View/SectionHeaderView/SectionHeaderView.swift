//
//  SectionHeaderView.swift
//  App
//
//  Created by MGAbouarab on 20/01/2024.
//

import UIKit

class SectionHeaderView: UIView {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var titleLabel: UILabel!
    
    //MARK: - Initializer -
    convenience init(title: String, font: UIFont = .appSemiBold(size: 14)) {
        self.init()
        self.titleLabel.text = title.capitalized
        self.titleLabel.font = font
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "SectionHeaderView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        
    }
    
    //MARK: - Data -
    func set(title: String, font: UIFont = .appSemiBold(size: 14)) {
        self.titleLabel.text = title.capitalized
        self.titleLabel.font = font
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}
