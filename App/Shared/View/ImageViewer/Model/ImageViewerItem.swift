//
//  ImageViewerItem.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import Foundation

struct ImageViewerItem {
    let urlImage: String?
    let dataImage: Data?
    
    init(urlImage: String? = nil, dataImage: Data? = nil) {
        self.urlImage = urlImage
        self.dataImage = dataImage
    }
}
