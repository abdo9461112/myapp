//
//  ImageViewerViewController.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import UIKit

class ImageViewerViewController: UIViewController {

    //MARK: - IBOutlets -
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Properties -
    private var viewTranslation = CGPoint(x: 0, y: 0)
    private let items: [ImageViewerItem]
    private let selectedIndex: IndexPath
    
    //MARK: - Initializer -
    init(items: [ImageViewerItem], selectedIndex: IndexPath = IndexPath(row: 0, section: 0)) {
        self.items = items
        self.selectedIndex = selectedIndex
        super.init(nibName: "ImageViewerViewController", bundle: nil)
        self.modalPresentationStyle = .overFullScreen
        self.modalTransitionStyle = .crossDissolve
    }
    required init?(coder: NSCoder) {
        self.selectedIndex = IndexPath(row: 0, section: 0)
        self.items = []
        super.init(coder: coder)
    }
    
    //MARK: - Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCollectionView()
        self.setupDesign()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.collectionView.scrollToItem(at: selectedIndex, at: .centeredHorizontally, animated: false)
        UIView.animate(withDuration: 0.2) {
            self.collectionView.alpha = 1
        }
    }
    
    //MARK: - Design Methods -
    private func setupDesign() {
        self.view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
        self.collectionView.alpha = 0
        self.view.backgroundColor = .black
    }
    
    //MARK: - Actions -
    @objc func handleDismiss(sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .changed:
            viewTranslation = sender.translation(in: view)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.collectionView.transform = CGAffineTransform(translationX: 0, y: self.viewTranslation.y)
                self.view.backgroundColor = .black.withAlphaComponent(0.5)
                
            })
        case .ended:
            if viewTranslation.y < 200 && viewTranslation.y > -200 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.collectionView.transform = .identity
                    self.view.backgroundColor = .black
                })
            } else {
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut) {
                    self.collectionView.alpha = 0
                    self.view.backgroundColor = .black.withAlphaComponent(0)
                } completion: { (_) in
                    self.dismiss(animated: true, completion: nil)
                }
            }
        default:
            break
        }
    }
    @IBAction func closeButtonPressed() {
        self.dismiss(animated: true)
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}

//MARK: - UICollectionView -
extension ImageViewerViewController {
    private func setupCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(cellType: ImageViewerCell.self)
    }
}
extension ImageViewerViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: ImageViewerCell.self, for: indexPath)
        cell.setupCell(image: self.items[indexPath.row])
        return cell
    }
}
extension ImageViewerViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: self.collectionView.bounds.height)
    }
}
