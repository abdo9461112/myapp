//
//  ImageViewerCell.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import UIKit

class ImageViewerCell: UICollectionViewCell {

    //MARK: - IBOutlets -
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    //MARK: - Lifecycle -
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupScrollView()
        self.addZoomTap()
    }
    
    //MARK: - Design -
    private func addZoomTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageViewTapped))
        tap.numberOfTapsRequired = 2
        self.imageView.isUserInteractionEnabled = true
        self.imageView.addGestureRecognizer(tap)
    }
    
    //MARK: - Data -
    func setupCell(image: ImageViewerItem, enableZoom: Bool = true) {
        self.imageView.contentMode = enableZoom ? .scaleAspectFit : .scaleAspectFill
        self.scrollView.isUserInteractionEnabled = enableZoom
        if let url = image.urlImage {
            self.imageView.setWith(url)
        } else if let data = image.dataImage {
            self.imageView.image = UIImage(data: data)
        }
    }
    
    //MARK: - Actions -
    @objc func imageViewTapped() {
        let isDefaultZoom = self.scrollView.zoomScale == 1
        UIView.animate(withDuration: 0.4) {
            self.scrollView.zoomScale = isDefaultZoom ? 2 : 1
        }
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }

}

extension ImageViewerCell: UIScrollViewDelegate {
    private func setupScrollView() {
        self.scrollView.delegate = self
        self.scrollView.maximumZoomScale = 2
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
}
