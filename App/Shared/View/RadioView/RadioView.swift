//
//  RadioView.swift
//  App
//
//  Created by MGAbouarab on 08/02/2024.
//

import UIKit

class RadioView: UIView {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var imageView: UIImageView!
    
    //MARK: - Properties -
    @IBInspectable
    var name: String? {
        didSet {
            self.nameLabel.xibLocKey = name
        }
    }
    
    //MARK: - Initializer -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "RadioView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.setSelection(false)
    }
    func setSelection(_ isSelected: Bool) {
        self.nameLabel.textColor = isSelected ? .label : .secondaryLabel
        self.imageView.image = isSelected ? .init(systemName: "circle.inset.filled") : .init(systemName: "circle")
        self.imageView.tintColor = isSelected ? .main : .secondaryLabel
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
}
