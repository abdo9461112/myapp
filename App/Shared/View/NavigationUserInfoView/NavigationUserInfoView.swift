//
//  NavigationUserInfoView.swift
//  App
//
//  Created by MGAbouarab on 20/01/2024.
//

import UIKit

class NavigationUserInfoView: UIView {
    
    enum Status {
        case authorized(image: String?)
        case unauthorized
    }
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak var viewTapped: UIView!
    
    //MARK: - Initializer -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "NavigationUserInfoView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        
    }
    
    //MARK: - Data -
    func set(status: NavigationUserInfoView.Status) {
        switch status {
        case .authorized(let image):
            self.imageView.contentMode = .scaleAspectFill
            self.imageView.setWith(image)
        case .unauthorized:
            self.imageView.contentMode = .scaleAspectFit
            self.imageView.image = UIImage(named: "AppIcon")
            self.viewTapped.isHidden = true
        }
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}
