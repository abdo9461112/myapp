//
//  BaseNavigationController.swift
//  App
//
//  Created by MGAbouarab on 30/12/2023.
//

import UIKit


/// This Class to make it easy to change UINavigationBar Appearance
/// Grouping the appearance colors to be easy to use with `setupAppearance` method.
/// Make `convenience init` to make it easy to add default `AppearanceTheme` when creating the new object.
/// `setupAppearance` Method is used to set the appearance with specific `AppearanceTheme`.
///
class BaseNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    var appearanceBackgroundColor: UIColor { .white }
    var appearanceTintColor: UIColor { .main }
    
    //MARK: - Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupGesture()
        self.handleAppearance()
        
    }
    
    //MARK: - Design -
    private func handleAppearance() {
        let appearance = UINavigationBarAppearance()
        appearance.titleTextAttributes = [.foregroundColor: UIColor.main, .font: UIFont.boldSystemFont(ofSize: 17)]
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        navigationBar.tintColor = .main
        navigationController?.navigationBar.isHidden = true
//        appearance.backgroundColor = .white
    }
    private func setupGesture() {
        interactivePopGestureRecognizer?.delegate = self
        self.view.semanticContentAttribute = Language.isRTL() ? .forceRightToLeft : .forceLeftToRight
    }
    
}


class ColoredNav: BaseNavigationController {
    
    enum ApperanceType {
        case light
        case colored
        
        var appearanceBackgroundColor: UIColor {
            switch self {
            case .light:
                UIColor.white
            case .colored:
                    .main
            }
        }
        var appearanceTintColor: UIColor {
            switch self {
            case .light:
                    .main
            case .colored:
                UIColor.white
            }
        }
    }
    
    //MARK: - Properties -
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override var appearanceBackgroundColor: UIColor { .main}
    override var appearanceTintColor: UIColor { UIColor.white }
    
    func changeApperance(to apperanceType: ApperanceType) {
        
        UIView.animate(withDuration: 0.5) {
            
            //        let appearance = UINavigationBarAppearance()
            self.navigationBar.standardAppearance.titleTextAttributes = [.foregroundColor: apperanceType.appearanceTintColor, .font: UIFont.boldSystemFont(ofSize: 17)]
            self.navigationBar.tintColor = apperanceType.appearanceTintColor
            self.navigationBar.standardAppearance.backgroundColor = apperanceType.appearanceBackgroundColor
            //        navigationBar.standardAppearance = appearance
            //        navigationBar.scrollEdgeAppearance = appearance
            
            
            self.navigationBar.scrollEdgeAppearance?.titleTextAttributes = [.foregroundColor: apperanceType.appearanceTintColor, .font: UIFont.boldSystemFont(ofSize: 17)]
            self.navigationBar.tintColor = apperanceType.appearanceTintColor
            self.navigationBar.scrollEdgeAppearance?.backgroundColor = apperanceType.appearanceBackgroundColor
            
        }
    }
    
    
}

    
/// Find underLine in UINavigationBar and show/hide it
extension BaseNavigationController {
    func hideHairline() {
        if let hairline = findHairlineImageViewUnder(navigationBar) {
            hairline.isHidden = true
        }
    }
    func restoreHairline() {
        if let hairline = findHairlineImageViewUnder(navigationBar) {
            hairline.isHidden = false
        }
    }
    private func findHairlineImageViewUnder(_ view: UIView) -> UIImageView? {
        if view is UIImageView && view.bounds.size.height <= 1.0 {
            return view as? UIImageView
        }
        for subview in view.subviews {
            if let imageView = self.findHairlineImageViewUnder(subview) {
                return imageView
            }
        }
        return nil
    }
}

/// To enable swipe to pop
extension BaseNavigationController: UIGestureRecognizerDelegate {
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        /// to solve an issue which make the app freeze when the user swipe on the root viewController and push new viewController
        viewControllers.count > 1
    }
}



