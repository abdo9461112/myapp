//
//  ProviderTableViewCell.swift
//  App
//
//  Created by MGAbouarab on 06/01/2024.
//

import UIKit

class ProviderTableViewCell: UITableViewCell {

    //MARK: - IBOutlets -
    @IBOutlet weak private var providerContainerView: UIView!
    @IBOutlet weak private var providerView: NearProvidersView!
    
    //MARK: - Lifecycle -
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupInitialDesign()
    }
    
    private func setupInitialDesign() {
        self.selectionStyle = .none
        self.providerContainerView.layer.cornerRadius = .cardCorner
        self.providerContainerView.clipsToBounds = true
    }
    
    //MARK: - Data -
    func set(image: String, name: String) {
        self.providerView.set(image: image, forProviderName: name)
    }
    func set(address: String) {
        self.providerView.set(address: address)
    }
    func setStatus(isOpen: Bool) {
        self.providerView.setStatus(isOpen: isOpen)
    }
    func set(rating: String, distance: String) {
        self.providerView.set(rating: rating, distance: distance)
    }

    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}
