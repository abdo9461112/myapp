//
//  MainButton.swift
//  App
//
//  Created by MGAbouarab on 26/12/2023.
//

import UIKit

class MainButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupInitialDesign()
        self.addPulsateAnimationWhenTapped()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupInitialDesign()
        self.addPulsateAnimationWhenTapped()
    }
    
    fileprivate func setupInitialDesign() {
        self.layer.cornerRadius = .buttonCorner
        self.clipsToBounds = true
        self.backgroundColor = .main
        self.setTitleColor(.white, for: .normal)
    }
    
    private func addPulsateAnimationWhenTapped() {
        self.addTarget(self, action: #selector(self.flash), for: .touchUpInside)
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
}

class RedButton: MainButton {
    override func setupInitialDesign() {
        super.setupInitialDesign()
        self.backgroundColor = .systemRed
    }
}

class GrayButton: MainButton {
    override func setupInitialDesign() {
        super.setupInitialDesign()
        self.backgroundColor = .systemGray4
        self.setTitleColor(.label, for: .normal)
    }
}
