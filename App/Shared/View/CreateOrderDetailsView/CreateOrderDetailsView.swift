//
//  CreateOrderDetailsView.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import UIKit

class CreateOrderDetailsView: UIView {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var textView: AppTextView!
    @IBOutlet weak private var imagesPicker: PickImageView!
    
    //MARK: - Properties -
    var notes: String? {
        self.textView.textValue()
    }
    var selectedImagesData: [Data] {
        self.imagesPicker.images
    }
    
    //MARK: - Initializer -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CreateOrderDetailsView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.layer.cornerRadius = .cardCorner
        self.clipsToBounds = true
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}
