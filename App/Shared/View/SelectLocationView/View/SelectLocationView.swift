////
////  SelectLocationView.swift
////  App
////
////  Created by MGAbouarab on 10/01/2024.
////
//
//import UIKit
//
//class SelectLocationView: TextFieldView {
//    
//    //MARK: - IBOutlet -
//    @IBOutlet weak private var titleLabel: UILabel!
//    @IBOutlet weak private var textField: UITextField!
//    @IBOutlet weak private var containerView: UIView!
//    @IBOutlet weak private var leadingImageView: UIImageView!
//    @IBOutlet weak private var trailingImageView: UIImageView!
//    
//    
//    //MARK: - Properties -
//    @IBInspectable var leadingImage: UIImage? {
//        didSet {
//            self.leadingImageView.image = leadingImage?.withRenderingMode(.alwaysTemplate)
//        }
//    }
//    @IBInspectable var trailingImage: UIImage? {
//        didSet {
//            self.trailingImageView.image = trailingImage?.withRenderingMode(.alwaysTemplate)
//        }
//    }
//    @IBInspectable var titleLocalizedKey: String? {
//        didSet {
//            self.titleLabel.xibLocKey = titleLocalizedKey
//        }
//    }
//    @IBInspectable var placeholderLocalizedKey: String? {
//        didSet {
//            self.textField.xibPlaceholderLocKey = placeholderLocalizedKey
//        }
//    }
//    private var selectedLocation: Location? {
//        didSet {
//            self.set(text: self.selectedLocation?.address)
//        }
//    }
//    weak var delegate: SelectLocationViewDelegate?
//    
//    //MARK: - Initializer -
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        self.xibSetUp()
//        self.setupInitialDesign()
//    }
//    required init?(coder: NSCoder) {
//        super.init(coder: coder)
//        self.xibSetUp()
//        self.setupInitialDesign()
//    }
//    
//    private func xibSetUp() {
//        let view = loadViewFromNib()
//        view.frame = self.bounds
//        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        addSubview(view)
//    }
//    private func loadViewFromNib() -> UIView {
//        
//        let bundle = Bundle(for: type(of: self))
//        let nib = UINib(nibName: "SelectLocationView", bundle: bundle)
//        return nib.instantiate(withOwner: self, options: nil).first as! UIView
//        
//    }
//    
//    //MARK: - Design -
//    private func setupInitialDesign() {
//        self.setupContainerView()
//        self.leadingImageView.isHidden = true
//        self.textField.isUserInteractionEnabled = false
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.viewDidTapped))
//        self.addGestureRecognizer(tap)
//    }
//    private func setupContainerView() {
//        self.containerView.layer.cornerRadius = containerViewCornerRadius
//        self.containerView.clipsToBounds = true
//        self.setInactiveState()
//    }
//    private func setActiveState() {
//        UIView.animate(withDuration: 0.2) {
//            self.containerView.addActiveBorder()
//            self.tintColor = self.activeTintColor
//        }
//    }
//    private func setInactiveState() {
//        UIView.animate(withDuration: 0.2) {
//            self.containerView.removeBorder()
//            self.tintColor = self.inActiveTintColor
//        }
//    }
//    
//    //MARK: - Encapsulation -
//    private func set(text: String?) {
//        guard let text, !text.trimWhiteSpace().isEmpty else {
//            self.textField.text = nil
//            self.setInactiveState()
//            return
//        }
//        self.textField.text = text
//        self.setActiveState()
//    }
//    func locationValue() -> Location? {
//        self.selectedLocation
//    }
//    
//    //MARK: - Actions -
//    @objc private func viewDidTapped() {
//        switch UserDefaults.isLogin {
//        case true:
//            #warning("Make the client go to addresses viewController to select the address, or make him decide if to go to address or pick from map")
//            break
//        case false:
//            let vc = PickLocationViewController(delegate: self, location: self.selectedLocation)
//            SceneDelegate.current?.window?.topViewController()?.navigationController?.pushViewController(vc, animated: true)
//        }
//    }
//    
//    
//    //MARK: - Deinit -
//    deinit {
//        print("\(self.className) is deinit, No memory leak found")
//    }
//    
//}
//
//extension SelectLocationView: PickLocationDelegate {
//    func didPick(location: Location) {
//        self.selectedLocation = location
//        self.delegate?.didPickLocation(location, for: self)
//    }
//}
