//
//  OrderPriceSummaryView.swift
//  App
//
//  Created by MGAbouarab on 11/01/2024.
//

import UIKit

class OrderPriceSummaryView: UIView {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var tableView: UITableView!
    
    //MARK: - Properties -
    private var items: [KeyValueModel] = []
    
    
    //MARK: - Initializer -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "OrderPriceSummaryView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.layer.cornerRadius = .cardCorner
        self.clipsToBounds = true
        self.setupTableView()
    }
    
    //MARK: - Data -
    func set(items: [KeyValueModel]) {
        defer {
            self.tableView.reloadData()
        }
        self.items = items
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
}

//MARK: - TableView -
extension OrderPriceSummaryView {
    private func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.register(cellType: KeyValueTableViewCell.self)
    }
}
extension OrderPriceSummaryView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: KeyValueTableViewCell.self, for: indexPath)
        let item = self.items[indexPath.row]
        cell.configure(item.configuration)
        cell.set(key: item.key, value: item.value)
        return cell
    }
}
