//
//  SmallDetailsWithImageView.swift
//  App
//
//  Created by MGAbouarab on 06/01/2024.
//

import UIKit

class SmallDetailsWithImageView: UIView {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak private var nameLabel: UILabel!
    
    //MARK: - Properties -
    @IBInspectable
    var image: UIImage? {
        didSet {
            self.imageView.image = self.image
        }
    }
    
    //MARK: - Initializer -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "SmallDetailsWithImageView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.layer.cornerRadius = 4
        self.clipsToBounds = true
    }
    
    //MARK: - Data -
    func set(image: UIImage?, forDetails name: String) {
        self.imageView.image = image
        self.nameLabel.text = name
    }
    func set(details name: String) {
        self.nameLabel.text = name
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
}

