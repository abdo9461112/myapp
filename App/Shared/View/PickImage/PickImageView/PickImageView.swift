//
//  PickImageView.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import UIKit

class PickImageView: UIView {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var pickImageView: UIView!
    @IBOutlet weak private var collectionView: DynamicHightCollectionView!
    
    //MARK: - Properties -
    private(set) var images: [Data] = [] {
        didSet {
            self.collectionView.isHidden = images.isEmpty
            self.collectionView.reloadData()
        }
    }
    
    //MARK: - Initializer -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "PickImageView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    //MARK: - Lifecycle -
    override func layoutSubviews() {
        super.layoutSubviews()
        self.pickImageView.addDashedBorder()
    }
    
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.pickImageView.layer.cornerRadius = .cardCorner
        self.clipsToBounds = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.pickImageViewTapped))
        self.pickImageView.addGestureRecognizer(tap)
        self.setupCollectionView()
    }
    
    //MARK: - Actions -
    @objc private func pickImageViewTapped() {
        ImagePicker().pickImage { [weak self] (image, imageData) in
            guard let self = self, let data = imageData else {return}
            self.images.append(data)
        }
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}


//MARK: - UICollectionView -
extension PickImageView {
    private func setupCollectionView() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(cellType: PickImageCollectionViewCell.self)
    }
}
extension PickImageView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: PickImageCollectionViewCell.self, for: indexPath)
        cell.onDeleteAction = { [weak self] in
            self?.images.remove(at: indexPath.item)
        }
        cell.onPresentAction = { [weak self] in
            guard let self = self else {return}
            let vc = ImageViewerViewController(items: self.images.map {.init(dataImage: $0)}, selectedIndex: indexPath)
            SceneDelegate.current?.window?.topViewController()?.present(vc, animated: true)
        }
        cell.set(imageData: self.images[indexPath.row])
        return cell
    }
    
    
}
extension PickImageView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let length = (collectionView.bounds.width-20)/3
        return .init(width: length, height: length)
    }
}
    

