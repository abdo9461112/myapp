//
//  PickImageCollectionViewCell.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import UIKit

class PickImageCollectionViewCell: UICollectionViewCell {

    //MARK: - IBOutlets -
    @IBOutlet weak private var imageView: UIImageView!
    
    //MARK: - Properties -
    var onDeleteAction: (()->())?
    var onPresentAction: (()->())?
    
    //MARK: - Lifecycle -
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupInitialDesign()
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        
        self.layer.cornerRadius = .smallCardCorner
        self.clipsToBounds = true
        
        self.imageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped))
        self.imageView.addGestureRecognizer(tap)
    }
    
    //MARK: - Data -
    func set(imageData data: Data) {
        self.imageView.image = .init(data: data)
    }
    
    //MARK: - Actions -
    @objc private func imageTapped() {
        self.onPresentAction?()
    }
    @IBAction private func deleteButtonPressed() {
        self.onDeleteAction?()
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }

}
