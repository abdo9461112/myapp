////
////  OrderReceiveDetailsView.swift
////  App
////
////  Created by MGAbouarab on 10/01/2024.
////
//
//import UIKit
//
//class OrderReceiveDetailsView: UIView {
//    
//    struct Time: DropDownItem {
//        let id: Int
//        let name: String
//    }
//    
//    //MARK: - IBOutlets -
//    @IBOutlet weak private var startLocationView: SelectLocationView!
//    @IBOutlet weak private var endLocationView: SelectLocationView!
//    @IBOutlet weak private var timeView: DropDownTextFieldView!
//    
//    //MARK: - Properties -
//    private(set) var startLocation: Location? {
//        didSet {
//            guard let location = self.startLocation else {return}
//            self.onSelectStartLocation?(location)
//            guard let endLocation = self.endLocation else {return}
//            self.onSelectStartAndEndLocations?(location, endLocation)
//        }
//    }
//    private(set) var endLocation: Location? {
//        didSet {
//            guard let location = self.endLocation else {return}
//            self.onSelectEndLocation?(location)
//            guard let startLocation = self.startLocation else {return}
//            self.onSelectStartAndEndLocations?(startLocation, location)
//        }
//    }
//    private(set) var times: [Time] = [
//        .init(id: 1, name: "One_Hour".localized),
//        .init(id: 2, name: "Two_Hour".localized),
//        .init(id: 3, name: "Three_Hour".localized),
//        .init(id: 4, name: "Four_Hour".localized),
//        .init(id: 5, name: "Five_Hour".localized),
//        .init(id: 6, name: "Six_Hour".localized),
//        .init(id: 7, name: "Seven_Hour".localized),
//        .init(id: 8, name: "Eight_Hour".localized),
//        .init(id: 9, name: "Nine_Hour".localized),
//        .init(id: 10, name: "Ten_Hour".localized),
//    ]
//    private(set) var selectedTime: Time?
//    
//    var onSelectStartLocation: ((_ location: Location)->())?
//    var onSelectEndLocation: ((_ location: Location)->())?
//    var onSelectStartAndEndLocations: ((_ startLocation: Location, _ endLocation: Location)->())?
//    
//    //MARK: - Initializer -
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        self.xibSetUp()
//        self.setupInitialDesign()
//    }
//    required init?(coder: NSCoder) {
//        super.init(coder: coder)
//        self.xibSetUp()
//        self.setupInitialDesign()
//    }
//    
//    private func xibSetUp() {
//        let view = loadViewFromNib()
//        view.frame = self.bounds
//        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        addSubview(view)
//    }
//    private func loadViewFromNib() -> UIView {
//        
//        let bundle = Bundle(for: type(of: self))
//        let nib = UINib(nibName: "OrderReceiveDetailsView", bundle: bundle)
//        return nib.instantiate(withOwner: self, options: nil).first as! UIView
//        
//    }
//    
//    //MARK: - Design -
//    private func setupInitialDesign() {
//        self.layer.cornerRadius = .cardCorner
//        self.clipsToBounds = true
//        self.timeView.delegate = self
//        self.startLocationView.delegate = self
//        self.endLocationView.delegate = self
//    }
//    func startLocation(isHidden: Bool) {
//        self.startLocationView.isHidden = isHidden
//    }
//    
//    //MARK: - Data -
//    
//    //MARK: - Deinit -
//    deinit {
//        print("\(self.className) is deinit, No memory leak found")
//    }
//    
//}
//
//extension OrderReceiveDetailsView: DropDownTextFieldViewDelegate {
//    
//    func dropDownList(for textFieldView: DropDownTextFieldView) -> [DropDownItem] {
//        return self.times
//    }
//    func didSelect(item: DropDownItem, for textFieldView: DropDownTextFieldView) {
//        self.selectedTime = item as? Time
//    }
//    
//}
//
//extension OrderReceiveDetailsView: SelectLocationViewDelegate {
//    func didPickLocation(_ location: Location, for selectLocationView: SelectLocationView) {
//        switch selectLocationView {
//        case self.startLocationView:
//            self.startLocation = location
//        case self.endLocationView:
//            self.endLocation = location
//        default:
//            fatalError("Please handle this case")
//        }
//    }
//}
