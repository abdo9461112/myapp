//
//  CounterView.swift
//  App
//
//  Created by MGAbouarab on 27/01/2024.
//

import UIKit

class CounterView: UIView {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var textField: UITextField!
    
    //MARK: - Properties -
    var onIncrease: (()->())?
    var onDecrease: (()->())?
    var onChangeAmount: ((String?)->())?
    
    //MARK: - Initializer -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CounterView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.layer.cornerRadius = .cardCorner
        self.clipsToBounds = true
        self.textField.delegate = self
    }
    
    //MARK: - Data -
    func set(count: String) {
        self.textField.text = count
    }
    func set(numbersFont font: UIFont) {
        self.textField.font = font
    }
    
    //MARK: - Actions -
    @IBAction private func increaseButtonPressed() {
        self.onIncrease?()
    }
    @IBAction private func decreaseButtonPressed() {
        self.onDecrease?()
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}

extension CounterView: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.onChangeAmount?(textField.text)
    }
}
