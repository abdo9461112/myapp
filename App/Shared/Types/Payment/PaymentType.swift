//
//  PaymentType.swift
//  App
//
//  Created by MGAbouarab on 09/01/2024.
//

import Foundation

struct PaymentType: Codable {
    
    let id: Int
    let key: String
    let name: String
    let description: String
    let image: String
    var isSelected: Bool
    
    enum CodingKeys: String, CodingKey {
        case id
        case key
        case name
        case description = "desc"
        case image
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.key = try container.decodeIfPresent(String.self, forKey: .key) ?? ""
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.description = try container.decodeIfPresent(String.self, forKey: .description) ?? ""
        self.image = try container.decodeIfPresent(String.self, forKey: .image) ?? ""
        self.isSelected = false
    }
    
    init(id: Int, key: String, name: String, description: String, image: String, isSelected: Bool) {
        self.id = id
        self.key = key
        self.name = name
        self.description = description
        self.image = image
        self.isSelected = isSelected
    }
    
    
    
}

