//
//  SuccessOrderModel.swift
//  App
//
//  Created by MGAbouarab on 12/01/2024.
//

import Foundation

struct SuccessOrderModel: Decodable {
    
    let orderId: Int
    
    enum CodingKeys: String, CodingKey {
        case orderId = "order_id"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.orderId = try container.decodeIfPresent(Int.self, forKey: .orderId) ?? 0
    }
    
    init(orderId: Int) {
        self.orderId = orderId
    }
    
}
