//
//  Numerical.swift
//  App
//
//  Created by MGAbouarab on 25/12/2023.
//

import Foundation

enum Numerical: Decodable {
    
    enum Error: Swift.Error {
        case couldNotFindNumerical
    }
    
    case string(String)
    case double(Double)
    
    init(from decoder: Decoder) throws {
        if let double = try? decoder.singleValueContainer().decode(Double.self) {
            self = .double(double)
            return
        }
        if let string = try? decoder.singleValueContainer().decode(String.self) {
            self = .string(string)
            return
        }
        throw Error.couldNotFindNumerical
    }
    
}
extension Numerical {
    var stringValue: String {
        switch self {
        case .string(let value):
            return value
        case .double(let value):
            return String(format: "0.2f", value)
        }
    }
    var doubleValue: Double {
        switch self {
        case .string(let value):
            return Double(value) ?? 0
        case .double(let value):
            return value
        }
    }
    var intValue: Int {
        switch self {
        case .string(let value):
            return Int(value) ?? 0
        case .double(let value):
            return Int(value)
        }
    }
    var floatValue: Float {
        switch self {
        case .string(let value):
            return Float(value) ?? 0
        case .double(let value):
            return Float(value)
        }
    }
}
