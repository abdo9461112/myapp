//
//  OrderPriceModel.swift
//  App
//
//  Created by MGAbouarab on 11/01/2024.
//

import Foundation

struct OrderPriceModel: Decodable {
    
    let addedValue: String
    let appPercentage: String
    let couponMessage: String
    let deliveryPrice: String
    let discount: String
    let hasCoupon: Bool
    let price: String
    let totalPrice: String
    
    enum CodingKeys: String, CodingKey {
        case addedValue = "added_value"
        case appPercentage = "app_percentage"
        case couponMessage = "coupon_msg"
        case deliveryPrice = "delivery_price"
        case discount = "discount"
        case hasCoupon = "has_coupon"
        case price = "price"
        case totalPrice = "total_price"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.addedValue = try container.decodeIfPresent(String.self, forKey: .addedValue) ?? ""
        self.appPercentage = try container.decodeIfPresent(String.self, forKey: .appPercentage) ?? ""
        self.couponMessage = try container.decodeIfPresent(String.self, forKey: .couponMessage) ?? ""
        self.deliveryPrice = try container.decodeIfPresent(String.self, forKey: .deliveryPrice) ?? ""
        self.discount = try container.decodeIfPresent(String.self, forKey: .discount) ?? ""
        self.hasCoupon = try container.decodeIfPresent(Bool.self, forKey: .hasCoupon) ?? false
        self.price = try container.decodeIfPresent(String.self, forKey: .price) ?? ""
        self.totalPrice = try container.decodeIfPresent(String.self, forKey: .totalPrice) ?? ""
    }
    
    init(addedValue: String, appPercentage: String, couponMessage: String, deliveryPrice: String, discount: String, hasCoupon: Bool, price: String, totalPrice: String) {
        self.addedValue = addedValue
        self.appPercentage = appPercentage
        self.couponMessage = couponMessage
        self.deliveryPrice = deliveryPrice
        self.discount = discount
        self.hasCoupon = hasCoupon
        self.price = price
        self.totalPrice = totalPrice
    }
}
