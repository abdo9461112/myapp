//
//  PaginationModel.swift
//  App
//
//  Created by MGAbouarab on 06/01/2024.
//

import Foundation

struct PaginationModel: Decodable {
    
    private let currentPage: Int
    private let totalPages: Int
    
    var isLastPage: Bool {
        currentPage >= totalPages
    }
    
    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case totalPages = "total_pages"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.currentPage = try container.decodeIfPresent(Int.self, forKey: .currentPage) ?? 1
        self.totalPages = try container.decodeIfPresent(Int.self, forKey: .totalPages) ?? 1
    }
    
    init() {
        self.currentPage = 1
        self.totalPages = 1
    }
    
    
}
