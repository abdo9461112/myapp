//
//  KeyValueModel.swift
//  App
//
//  Created by MGAbouarab on 11/01/2024.
//

import Foundation

struct KeyValueModel {
    let key: String
    let value: String
    let configuration: KeyValueTableViewCell.Configuration
    init(key: String, value: String, configuration: KeyValueTableViewCell.Configuration = .default) {
        self.key = key
        self.value = value
        self.configuration = configuration
    }
}
